export * from './userActions'
export * from './api'

export const STRINGS = {
    added_to_cart:'Added to cart',
    register_success:'Successfully registered!',
    max_limit_reached:'Maximim limit reached!'
}