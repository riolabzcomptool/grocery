/* eslint-disable no-underscore-dangle */
import React from 'react'
import { withRouter } from 'react-router'


const RecentBlog = (props) => {
  const { name, slug } = props
  const url = `${slug}`;
  console.log("8888",props);
    return ( 
    <div className="recent-posts">
      <h5 className="recent-posts-head-wrapper">
        <a href={url}  className="recent-posts-head">
          {name}
        </a>
      </h5>
      
    </div>
  
    );

}

export default withRouter(RecentBlog)
