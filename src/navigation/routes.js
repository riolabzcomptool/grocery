import React from 'react';

const loadable = loader => React.lazy(loader);

const routes = [
  {
    path: '/products',
    component: loadable(() => import('modules/product/list')),
    exact: true,
  },
  {
    path: '/products/:type/:id',
    component: loadable(() => import('modules/product/list')),
    exact: true,
  },
  {
    path: '/products/:type',
    component: loadable(() => import('modules/product/list')),
    exact: true,
  },
  {
    path: '/product/:slug',
    component: loadable(() => import('modules/product/details')),
    exact: true,
  },
  {
    path: '/home',
    component: loadable(() => import('modules/home')),
    exact: true,
  },
  // {
  //   path: '/user/login',
  //   component: loadable(() => import('modules/user/login')),
  //   exact: true,
  //   headerState: {
  //     title: 'Login',
  //     bread: [
  //       {
  //         url: '/home',
  //         text: 'Home',
  //       },
  //       {
  //         text: 'Login',
  //       },
  //     ],
  //   },
  // },
  // {
  //   path: '/user/register',
  //   component: loadable(() => import('modules/user/register')),
  //   exact: true,
  //   headerState: {
  //     title: 'Register',
  //     bread: [
  //       {
  //         url: '/home',
  //         text: 'Home',
  //       },
  //       {
  //         text: 'Register',
  //       },
  //     ],
  //   },
  // },
  // {
  //   path: '/forgot-password',
  //   component: loadable(() => import('modules/user/forgot-password')),
  //   exact: true,
  //   headerState: {
  //     title: 'Forgot Password',
  //     bread: [
  //       {
  //         url: '/home',
  //         text: 'Home',
  //       },
  //       {
  //         text: 'Forgot Password',
  //       },
  //     ],
  //   },
  // },
  // {
  //   path: '/reset-password/:resetPasswordToken',
  //   component: loadable(() => import('modules/user/reset-password')),
  //   exact: true,
  //   headerState: {
  //     title: 'Reset Password',
  //     bread: [
  //       {
  //         url: '/home',
  //         text: 'Home',
  //       },
  //       {
  //         text: 'Reset Password',
  //       },
  //     ],
  //   },
  // },
  // {
  //   path: '/user/user-profile',
  //   component: loadable(() => import('modules/user/profile')),
  //   exact: true,
  //   headerState: {
  //     title: 'User Profile',
  //     bread: [
  //       {
  //         url: '/home',
  //         text: 'Home',
  //       },
  //       {
  //         text: 'User Profile',
  //       },
  //     ],
  //   },
  // },
  {
    path: '/cart',
    component: loadable(() => import('modules/cart')),
    exact: true,
    headerState: {
      title: 'Cart',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Cart',
        },
      ],
    },
  },
  // {
  //   path: '/orders',
  //   component: loadable(() => import('modules/user/orders/list')),
  //   exact: true,
  //   headerState: {
  //     title: 'My Orders',
  //     bread: [
  //       {
  //         url: '/home',
  //         text: 'Home',
  //       },
  //       {
  //         text: 'My Orders',
  //       },
  //     ],
  //   },
  // },

  {
    path: '/checkout',
    component: loadable(() => import('modules/checkout')),
    exact: true,
    headerState: {
      title: 'Checkout',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Checkout',
        },
      ],
    },
  },
  {
    path: '/wishlist',
    component: loadable(() => import('modules/wishlist')),
    exact: true,
    headerState: {
      title: 'Wishlist',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Wishlist',
        },
      ],
    },
  },

  {
    path: '/info/:slug',
    component: loadable(() => import('modules/information-2')),
    exact: true,
  },
  {
    path: '/order-placed',
    component: loadable(() => import('modules/order-placed')),
    exact: true,
  },
  // {
  //   path: '/information-1',
  //   component: loadable(() => import('modules/information-1')),
  //   exact: true,
  // },
  // {
  //   path: '/information-2',
  //   component: loadable(() => import('modules/information-2')),
  //   exact: true,
  // },
];

export const isRouteWithHeader = url => {
  //
  console.log('in findRoute', url);
  const route = routes.find(item => item.path === url);
  if (route) {
    if (route.headerState) {
      return true;
    }
  }
  return false;
};

export const getHeaderState = url => {
  console.log('in getHeaderState');
  const route = routes.find(item => item.path === url);
  console.log(route);
  if (route && route.headerState) {
    console.log(route.headerState);
    return route.headerState;
  }
  return null;
};

export default routes;
