import React from 'react';
import appleStoreImg from 'assets/images/app/app-storw.png'
import playStoreImg from 'assets/images/app/play-store.png'

const BrandsApp = () => {
  return (
    <section className="">
      <div className="container">
        <div className="row">
          {/* <div className="col-xl-4 logo no-border">
            <h2 className="title"> brands</h2>
            <div className="logo-3 border-logo slick-initialized slick-slider">
              <button
                className="slick-prev slick-arrow"
                aria-label="Previous"
                type="button"
                style=""
              >
                Previous
              </button>
              <div className="slick-list draggable">
                <div
                  className="slick-track"
                  style="opacity: 1; width: 5402px; transform: translate3d(-2190px, 0px, 0px);"
                >
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="-3"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/15.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="-2"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/16.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="-1"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/17.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="0"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/1.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="1"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/2.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="2"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/3.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="3"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/4.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="4"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/5.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="5"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/6.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="6"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/7.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="7"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/8.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="8"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/9.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="9"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/10.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="10"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/11.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="11"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/12.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-current slick-active"
                    data-slick-index="12"
                    aria-hidden="false"
                    style="width: 146px;"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/13.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-active"
                    data-slick-index="13"
                    aria-hidden="false"
                    style="width: 146px;"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/14.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-active"
                    data-slick-index="14"
                    aria-hidden="false"
                    style="width: 146px;"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/15.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="15"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/16.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide"
                    data-slick-index="16"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/17.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="17"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/1.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="18"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/2.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="19"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/3.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="20"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/4.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="21"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/5.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="22"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/6.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="23"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/7.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="24"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/8.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="25"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/9.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="26"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/10.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="27"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/11.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="28"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/12.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="29"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/13.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="30"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/14.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="31"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/15.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="32"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/16.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                  <div
                    className="slick-slide slick-cloned"
                    data-slick-index="33"
                    aria-hidden="true"
                    style="width: 146px;"
                    tabindex="-1"
                  >
                    <div>
                      <div className="logo-img" style="width: 100%; display: inline-block;">
                        <img src="../assets/images/logo/17.png" className=" img-fluid" alt="" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button className="slick-next slick-arrow" aria-label="Next" type="button" style="">
                Next
              </button>
            </div>
          </div> */}
          <div className="col-xl-6 offset-xl-1">
            <div className="app-section">
              <div className="app-content">
                <div>
                  <h5>download the big market app</h5>
                  <div className="app-buttons">
                    <a href="#">
                      <img src={appleStoreImg} className=" img-fluid" alt="" />
                    </a>
                    <a href="#">
                      <img src={playStoreImg} className=" img-fluid" alt="" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default BrandsApp;
