import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './styles.css';
import styled from 'styled-components';
import 'assets/styles/colors.scss';
import userImg from 'assets/images/kickill-user.svg'


export const StyledMenuItem = styled.div`
  /* color: #116071; */
  
  padding: 3px 5px;
  font-size: 0.9em;
  color: #6e6e6e;
  font-weight: 500;
  transition: ease-in 0.4s;
  &:hover {
    background: #ded8d880;
  }
`;

export const StyledIcon = styled.i`
  color: #398d9f;
  margin-right: 1em;
`;

const StyledDropdownMenu = styled.div`
  transform: translate3d(-60px, 32px, 0px) !important;
  box-shadow: rgba(0, 0, 0, 0.45) 3px 3px 8px 0px;
`;

const logoutStyle = {
  cursor:"pointer"
}

class ProfileMenu extends Component {
  state = {
    isToggle: false
  }

  handleToggle = () => {
    this.setState(prev => {
      return ({
        ...prev, isToggle: !prev.isToggle
      })
    })
  }

  render() {
    const { img, menu } = this.props;
    const { profileMenuData } = menu;
    const { dispatch } = this.props;



    // const profileMenuFiltered = profileMenuData.filter(item => item.url !== '/logout')

    // return (
    //   <div className="profile-menu">
    //     {profileMenuData.map(item => (
    //       <div className="menu-grid">
    //         <div className="menuitem__icon">
    //           <i className={item.icon} />
    //         </div>
    //         <div className="menuitem__title">{item.title}</div>
    //       </div>
    //     ))}
    //   </div>
    // );

    return (
      // <div className="btn-group">
      <div className="dropdown">
        <button
          type="button"
          className="btn primary-bg-color login-btn h-100"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
          id="dropdownMenuButton"
        // onClick={this.handleToggle}
        >
          <img src={userImg} alt="" className="lazy top-icons" />
        </button>
        <StyledDropdownMenu className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          {profileMenuData.map(item => {
            if (item.divider) return <div key={Math.random()} className="dropdown-divider" />;
            if (item.url) {
              return (
                <Link to={item.url} key={item.key}>
                  {' '}
                  <StyledMenuItem role="presentation" aria-hidden="true" className="dropdown-item">
                    <StyledIcon className={item.icon} />

                    {item.title}
                  </StyledMenuItem>
                </Link>
              );
            }
            if (item.action) {
              return (
                <StyledMenuItem
                  className="dropdown-item"
                  key={item.key}
                  role="button"
                  onClick={() => dispatch({ type: item.action })}
                  aria-hidden="true"
                  style={logoutStyle}
                >
                  <StyledIcon className={item.icon} />

                  {item.title}
                </StyledMenuItem>
              );
            }
            return '';
            // console.log(item.key);
            // return (
            //   <Link key={item.key} className="dropdown-item" to={`${item.url}`}>
            //     {item.icon && (
            //       <span key={item.key}>
            //         <i key={item.key} className={item.icon} aria-hidden="true" />
            //       </span>
            //     )}{' '}
            //     <span>{item.title}</span>
            //   </Link>
            // );
          })}
        </StyledDropdownMenu>
      </div>
    );
  }
}

export default connect(({ menu }) => ({ menu }))(ProfileMenu);
