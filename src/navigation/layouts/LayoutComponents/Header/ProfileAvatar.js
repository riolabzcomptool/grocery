import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {withRouter} from 'react-router'
import { connect } from 'react-redux'
import ProfileMenu from './ProfileMenu'

const mapStateToProps = ({ user, menu }) => ({
  isLogged : user.isLogged,
  menu
})

class ProfileAvatar extends Component {
  render() {
    const { isLogged, menu } = this.props
    console.log(isLogged)
    console.log(menu)
    console.log(this.props)

    if (isLogged) return <ProfileMenu />

    return (
      <Link to="/user/login" className="btn primary-bg-color login-btn">
        Login
        {/* <img src="/resources/images/kickill-user.svg" alt="" className="lazy top-icons" /> */}
      </Link>
    )
  }
}

export default withRouter(connect(mapStateToProps)(ProfileAvatar));
