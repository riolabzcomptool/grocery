import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
// import classNames from 'classnames';
import WOW from 'wowjs'; // main
// import {WOW} from 'wowjs/dist/wow';
// import NavBar from './NavBar';
import { connect } from 'react-redux';
// import Button from 'components/Button';
import qs from 'qs';
import PhoneVerify from 'components/PhoneVerify';
import wishlistImg from 'assets/images/icon/l-2/wishlist.png';
import userImg from 'assets/images/icon/l-2/user.png';
import cartImg from 'assets/images/icon/l-2/cart.png';
import settingsImg from 'assets/images/icon/settings.png';
import { menuActions } from 'redux/actions';
import WishlistSide from 'components/WishlistSide';
import MyAccountSide from 'components/MyAccountSide';
import CartSide from 'components/CartSide';
import BreadCrumbWrapper from 'components/BreadCrumbWrapper';
import { userActions } from '_constants';
import CategoriesNavigation from './CategoriesNavigation';
// import Navigation from './Navigation';
// import CartIcon from './CartIcon';
// import ProfileAvatar from './ProfileAvatar';
// import HeaderWrapper from './HeaderWrapper';

class Header extends PureComponent {
  constructor(props) {
    super(props);
    props.history.listen(() => {
      if (props.history.location.pathname !== '/products') this.setState({ searchText: '' });
    });
  }

  state = {
    isMobileNav: false,
    openCount: 0,
    searchText: '',
  };
  // componentDidMount() {
  //   new WOW.WOW({
  //     live: false,
  //   }).init()
  // }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      const wow = new WOW.WOW({
        live: false,
      });
      wow.init();
    }
    const { searchText: currentText } = this.state;
    const { history } = this.props;
    console.log(
      'joi',
      currentText === '' && history.location.search && history.location.pathname === '/products',
    );
    if (
      currentText === '' &&
      history.location.search &&
      history.location.pathname === '/products'
    ) {
      this.updateSearchText();
    }
  }

  componentDidUpdate(prevProps) {
    const { location: currentLocation } = this.props;
    console.log('KOL', prevProps.location, currentLocation);
    if (prevProps.location.search !== currentLocation.search) {
      this.updateSearchText();
    }
  }

  componentWillUnmount() {
    const { history } = this.props;
    if (history && history.unlinsten) history.unlinsten();
  }

  updateSearchText = () => {
    const { history } = this.props;
    console.log('KOL updating');
    const searchQuery = qs.parse(history.location.search, { ignoreQueryPrefix: true });
    if (searchQuery.search) this.setState({ searchText: searchQuery.search });
  };

  // setToggleMenu = () => {
  //   this.setState(prev => ({
  //     ...prev,
  //     isToggleMenu: !prev.isToggleMenu,
  //   }));
  // };

  openVerifyModal = () => {
    this.setState(prev => ({
      ...prev,
      openCount: prev.openCount + 1,
    }));
  };

  handleSubmitSearch = e => {
    const { history } = this.props;
    if (e) e.preventDefault();
    const { searchText } = this.state;
    if (searchText !== '') history.push(`/products?search=${searchText.trimRight()}`);
  };

  handleChange = e => {
    e.persist();
    const { value } = e.target;
    this.setState({ searchText: value });
  };

  openWishlist = e => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch({
      type: menuActions.TOGGLE_WISHLIST,
    });
  };

  openAccount = e => {
    e.preventDefault();
    const { dispatch, user } = this.props;
    console.log('joiki', user);
    if (!user.isLogged)
      dispatch({
        type: menuActions.TOGGLE_ACCOUNT_SIDE,
      });
  };

  openCart = e => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch({
      type: menuActions.TOGGLE_CART_SIDE,
    });
  };

  handleLogout = e => {
    e.preventDefault();
    const { dispatch, isAccountSide } = this.props;
    dispatch({
      type: userActions.LOGOUT,
    });
    if (isAccountSide)
      dispatch({
        type: menuActions.TOGGLE_ACCOUNT_SIDE,
      });
  };

  render() {
    const { user, wishlistCount, cartCount } = this.props;
    const { openCount, searchText } = this.state;
    return (
      <>
        <WishlistSide />
        <MyAccountSide />
        <CartSide />
        <PhoneVerify openCount={openCount} />
        <header className="header-2">
          <div className="mobile-fix-header" />
          <div className="container">
            <div className="row header-content">
              <div className="col-12">
                <div className="content-header">
                  <div className="left-section">
                    <div className="header-top">
                      <div className="brand-logo">
                        <Link to="/">
                          <img src="/resources/images/popolr.png" className="" alt="" />
                        </Link>
                      </div>
                      <CategoriesNavigation />
                    </div>
                    <div className="header-bottom">
                      <div className="search-bar">
                        <form onSubmit={this.handleSubmitSearch}>
                          <input
                            onChange={this.handleChange}
                            value={searchText}
                            className="search__input"
                            type="text"
                            placeholder="Search a product"
                          />
                          <div className="search-icon ">
                            <input type="submit" />
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div className="right-section">
                    <div className="upper-section">
                      <ul>
                        <li>
                          <a href="#">Incentives on shipping</a>
                        </li>
                        <li>
                          <a href="#">free shipping</a>
                        </li>
                        <li>
                          <a href="#">Fast delivery</a>
                        </li>
                      </ul>
                    </div>
                    <div className="lower-section">
                      <div className="nav-icon">
                        <ul>
                          <li className="onhover-div setting-icon">
                            <div>
                              <img src={settingsImg} className=" img-fluid setting-img" alt="" />
                              <i className="fa fa-sliders mobile-icon" />
                            </div>
                          </li>
                          <li className="onhover-div pr-0 search-3">
                            <div role="presentation" onClick="openSearch()">
                              <i className="fa fa-search mobile-icon-search mobile-icon" />
                            </div>
                            <div id="search-overlay" className="search-overlay">
                              <div>
                                <span
                                  role="presentation"
                                  className="closebtn"
                                  onClick="closeSearch()"
                                  title="Close Overlay"
                                >
                                  ×
                                </span>
                                <div className="overlay-content">
                                  <div className="container">
                                    <div className="row">
                                      <div className="col-xl-12">
                                        <form>
                                          <div className="form-group">
                                            <input
                                              type="text"
                                              className="form-control"
                                              id="exampleInputPassword1"
                                              placeholder="Search a Product"
                                            />
                                          </div>
                                          <button type="submit" className="btn btn-primary">
                                            <i className="fa fa-search" />
                                          </button>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                          <li
                            className="onhover-div wishlist-icon"
                            // onClick={this.openWishlist}
                          >
                            <>
                              <a onClick={this.openWishlist} href="#">
                                <img src={wishlistImg} alt="" className="wishlist-img" />
                                <i className="fa fa-heart mobile-icon" />
                                <div className="wishlist icon-detail">
                                  <h6 className="up-cls">
                                    <span>
                                      {wishlistCount}&nbsp;{wishlistCount === 1 ? 'item' : 'items'}
                                    </span>
                                  </h6>
                                  <h6>wish list</h6>
                                </div>
                              </a>
                            </>
                          </li>

                          {!user.isLogged && (
                            <li
                              className="onhover-div user-icon"
                              // onClick={this.openAccount}
                            >
                              <a href="#" onClick={this.openAccount}>
                                <img src={userImg} alt="" className="user-img" />
                                <i className="fa fa-user mobile-icon" />
                                <div className="wishlist icon-detail">
                                  {user.isLogged && (
                                    <h6 className="up-cls">
                                      <span>my account</span>
                                    </h6>
                                  )}
                                  <h6>{!user.isLogged ? 'login/sign up' : user.firstName}</h6>
                                </div>
                              </a>
                            </li>
                          )}
                          {user.isLogged && (
                            <li className="dropdown onhover-div user-icon">
                              <button
                                // className="dropdown-toggle"
                                type="button"
                                id="dropdownMenuButton"
                                // onClick={this.toggleDropdown}
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                              >
                                <img src={userImg} alt="" className="user-img" />
                                <i className="fa fa-user mobile-icon" />
                                <div className="wishlist icon-detail">
                                  <h6 className="up-cls">
                                    <span>my account</span>
                                  </h6>
                                  <h6>{user.firstName}</h6>
                                </div>
                              </button>
                              {/* <div className="border-bottom" /> */}
                              <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <li className="dropdown-item border-bottom no-hover">
                                  <strong>Hi,&nbsp;{user.firstName}</strong>
                                </li>
                                <li className="dropdown-item">Go to dashboard</li>
                                <li className="dropdown-item">
                                  <a href="#" onClick={this.handleLogout}>
                                    Logout
                                  </a>
                                </li>
                              </div>
                            </li>
                          )}

                          <li
                            className="onhover-div cart-icon"
                            // onClick={this.openCart}
                          >
                            <a onClick={this.openCart} href="#">
                              <img src={cartImg} alt="" className="cart-image" />
                              <i className="fa fa-shopping-cart mobile-icon" />
                              <div className="cart  icon-detail">
                                <h6 className="up-cls">
                                  <span>
                                    {cartCount}&nbsp;{cartCount === 1 ? 'item' : 'items'}
                                  </span>
                                </h6>
                                <h6>my cart</h6>
                              </div>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <BreadCrumbWrapper />
      </>
    );
  }
}

export default withRouter(
  connect(({ user, menu, cart, wishlist }) => ({
    user,
    isAccountSide: menu.isAccountSide,
    cartCount: cart.products.length,
    wishlistCount: wishlist.products.length,
  }))(Header),
);
