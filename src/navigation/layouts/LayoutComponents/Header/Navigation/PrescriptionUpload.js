import React from 'react';
// import './index.scss'

const PrescriptionUpload = () => {
  return (
    <div className="priscription-upload">
      <div className="upload-btn-wrapper">
        <button type="button" className="btn">
          Upload Prescription{' '}
          <span>
            <img className="" src="/resources/images/kickill-upload.svg" alt="" />
          </span>
        </button>
        <input type="file" name="myfile" />
      </div>
    </div>
  );
};

export default PrescriptionUpload;
