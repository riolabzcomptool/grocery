import React, { useState, useRef, useEffect } from 'react';
import { generateCategoryTree } from '_utils';
import classnames from 'classnames';
// import styled from 'styled-components';
import { useCategories } from 'hooks/api';
import { Link } from 'react-router-dom';
import findIndex from 'lodash/findIndex';

// const StyledDiv = styled.div`
//   margin-bottom: 20px;
//   display: grid;
//   grid-template-columns: 1fr 1fr 1fr;
//   grid-auto-flow: columns;
//   &:last-child {
//     margin-bottom: 0px;
//   }
//   &:grid-column(1) {
//     background-color: red;
//   }


//   /* column-count: 4; 
//   @media (max-width: 991px) {
//     column-count: 2;
//   } */
//   @media (max-width: 991px) {
//     display: block;
//   }
// `;

const NavBar = ({ hasSideMenu }) => {
  const [nodes, setNodes] = useState([]);
  const colItemsCount = useRef();
  colItemsCount.current = 0;

  const handleCaret = e => {
    console.log('HELLO');
    const { dataset } = e.currentTarget;
    const { nodeid: selectedNodeId } = dataset;
    console.log(selectedNodeId);
    const index = findIndex(nodes, i => i.id === selectedNodeId);
    console.log('index', index);
    // if (index >= 0) nodes[index].isOpen = !(nodes[index].isOpen && true);
    // if (index >= 0) nodes[index].isOpen = !(nodes[index].isOpen ? nodes[index].isOpen : false);

    setNodes(prev => {
      if (index >= 0) {
        // if (prev[index].children && prev[index].children.length > 0)
          prev[index].isOpen = !(prev[index].isOpen ? prev[index].isOpen : false);
      }
      return [...prev];
    });
  };

  const { data } = useCategories();

  useEffect(() => console.log(nodes), [nodes]);

  useEffect(() => {
    if (data && data.data) setNodes(generateCategoryTree(data.data));
  }, [data]);

  return (
    <nav className={classnames({ 'has-side-menu': hasSideMenu })}>
      {/** container not necessary */}
      <div className="container">
        <ul className="menu-main">
          {nodes
            .filter(i => i.parent === 0)
            .map(i => {
              console.log(i.isOpen);
              return (
                <li key={i.id} className="menu-level-one">
                  <Link className="main-heading" to={`/products/category/${i.slug}`}>
                    {i.title}
                  </Link>
                  <span
                    data-nodeid={i.id}
                    className="toggle-caret"
                    tabIndex={0}
                    role="button"
                    onKeyDown={handleCaret}
                    onClick={handleCaret}
                  >
                    {i.isOpen && <i className="fas fa-chevron-up" />}
                    {!i.isOpen && <i className="fas fa-chevron-down" />}
                  </span>
                  {i.children && (
                    <div className={classnames('menu-sub', { active: i.isOpen })}>
                      {i.children.map(sub1 => {
                        return (
                          <div className="menu-sub-item" key={sub1.id}>
                            <div>
                              <Link to={`/products/category/${sub1.slug}`}>
                                <h3 className="category-heading">{sub1.title}</h3>
                              </Link>
                              {sub1.children && (
                                <ul>
                                  {sub1.children.map(sub2 => (
                                    <ul key={sub2.id}>
                                      <li>
                                        <Link to={`/products/category/${sub2.slug}`}>
                                          {sub2.title}
                                        </Link>
                                      </li>
                                    </ul>
                                  ))}
                                </ul>
                              )}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  )}
                </li>
              );
            })}
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
