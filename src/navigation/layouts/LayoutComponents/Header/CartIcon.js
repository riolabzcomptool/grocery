import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class CartIcon extends PureComponent {
  state = {
    // isToggleMenu: false,
  };

  render() {
    console.log('CartIcon', this.props);
    const { cart } = this.props;
    return (
      <Link to="/cart" className="btn primary-bg-color cart-btn">
        <img src="/resources/images/kickill-cart.svg" alt="" className="top-icons" />
        {`(${cart.products.length}) Items`}
      </Link>
    );
  }
}

export default connect(({ cart }) => ({ cart }))(CartIcon);
