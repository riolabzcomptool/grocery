import React, { useCallback, useState } from 'react';
import classNames from 'classnames';
import { useCategories } from 'hooks/api';
import { generateCategoryTree } from '_utils';

const CategoriesNavigation = () => {
  const [isMobileNav, setMobileNav] = useState(false);

  const handleToggleNav = useCallback(e => {
    e.preventDefault();
    setMobileNav(prev => !prev);
  }, []);

  const handleCategoryClick = useCallback((e) => {
      e.preventDefault()
  },[])

  const { data } = useCategories();

  let categoryTree = [];
  if (data?.data) categoryTree = generateCategoryTree(data.data);

  if (data?.data)
    return (
      <nav id="main-nav">
        <a href="#" onClick={handleToggleNav} className="toggle-nav">
          <i className="fa fa-list-ul" />
        </a>
        <ul
          id="main-menu"
          className={classNames('sm pixelstrap sm-horizontal', {
            'right-0': isMobileNav,
          })}
        >
          <li>
            <div
              role="button"
              onKeyPress={handleToggleNav}
              tabIndex={0}
              onClick={handleToggleNav}
              className="mobile-back text-right"
            >
              Back
              <i className="fa fa-angle-right pl-2" aria-hidden={isMobileNav} />
            </div>
          </li>
          {categoryTree.map(i => (
            <li>
              <a onClick={handleCategoryClick} href="#">{i.title}</a>
            </li>
          ))}
        </ul>
      </nav>
    );
  return null;
};

export default CategoriesNavigation;
