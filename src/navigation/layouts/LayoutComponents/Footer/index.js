/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
// import { getInformations } from 'services/informations';
// import { getBrands } from 'services/brands';
import { getCategories } from 'services/category';
import logo from 'assets/images/popolrlogo2.png';
import visa from 'assets/images/icon/visa.png';
import mastercard from 'assets/images/icon/mastercard.png';
import paypal from 'assets/images/icon/paypal.png';
import americanExp from 'assets/images/icon/american-express.png';
import discover from 'assets/images/icon/discover.png';
import { getInformations } from 'services/informations';
import data from './data.json';
import FooterMenu from './FooterMenu';
import BrandsSlider from './BrandsSlider';

class Footer extends Component {
  state = {
    footerMenuData: data.data,
    // brands: [],
    informations: [],
    categories: [],
    // quickLinks: [],
  };

  componentDidMount() {
    this.fetchList();
  }

  fetchList = async () => {
    // console.log('fetching brands');
    // const brands = await getBrands({ fields: ['name', 'slug'] });
    const informations = await getInformations();
    const categories = await getCategories({ fields: ['name', 'slug'], parent: null });
    // console.log(brands);
    // if (brands)
    //   this.setState({
    //     brands,
    //   });
    if (informations)
      this.setState({
        informations,
      });
    if (categories)
      this.setState({
        categories,
      });
  };

  render() {
    let {
      footerMenuData, // brands, // quickLinks,
      informations,
      categories,
    } = this.state;

    // brands = brands.map(i => ({
    //   title: i.name,
    //   link: `/products/brand/${i.slug}`,
    //   key: i.slug,
    //   slug: i.slug,
    //   id: i._id,
    // }));
    informations = informations.map(i => ({
      title: i.name,
      link: `/info/${i.slug}`,
      key: i.slug,
      // footerStatus: i.footerStatus,
    }));
    categories = categories.map(i => ({
      title: i.name,
      link: `/products/category/${i.slug}`,
      slug: i.slug,
      id: i._id,
      key: i.slug,
    }));

    // quickLinks = [
    //   { title: 'Blogs', link: '/blogs', key: 'blogs' },
    //   { title: 'Contact Us', key: 'contact-us', link: '/contact-us' },
    // ];

    footerMenuData = [
      // { title: 'Quick Links', key: 'quick-links', children: quickLinks },
      { title: 'Info Links', key: 'informations', children: informations },
      // ...footerMenuData,
      { title: 'Categories', key: 'categories', children: categories },
      // { title: 'Brands', key: 'brands', children: brands },
    ];

    const footerMenu = footerMenuData.map(item => (
      <FooterMenu key={item.key || item._id} title={item.title} list={item.children} />
    ));

    return (
      <>
        <BrandsSlider />
        <footer className="footer-3">
          <div className="footer-section">
            <div className="container">
              <div className="row border-cls section-b-space section-t-space">
                <div className="col-xl-4 col-lg-12 about-section">
                  <div className="footer-title footer-mobile-title">
                    <h4>about</h4>
                  </div>
                  <div className="footer-content">
                    <div className="footer-logo">
                      <img src={logo} className="" alt="" />
                    </div>
                    <p>
                      It is a long established fact that a reader will be distracted by the readable
                      content of a page.
                    </p>
                    <div className="footer-social">
                      <ul>
                        <li>
                          <a aria-label="facebook" href="#">
                            <i className="fa fa-facebook" aria-hidden="true" />
                          </a>
                        </li>
                        <li>
                          <a aria-label="google plus" href="#">
                            <i className="fa fa-google-plus" aria-hidden="true" />
                          </a>
                        </li>
                        <li>
                          <a aria-label="twitter" href="#">
                            <i className="fa fa-twitter" aria-hidden="true" />
                          </a>
                        </li>
                        <li>
                          <a aria-label="instagram" href="#">
                            <i className="fa fa-instagram" aria-hidden="true" />
                          </a>
                        </li>
                        <li>
                          <a aria-label="rss" href="#">
                            <i className="fa fa-rss" aria-hidden="true" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-xl-8 col-lg-12">
                  <div className="row height-cls">
                    {footerMenu}
                    <div className="col-lg-4 footer-link">
                      <div>
                        <div className="footer-title">
                          <h4>contact us</h4>
                        </div>
                        <div className="footer-content">
                          <ul className="contact-list">
                            <li>
                              <i className="fa fa-map-marker" />
                              Popolr Bangalore
                            </li>
                            <li>
                              <i className="fa fa-phone" />
                              Call Us: 123-456-7898
                            </li>
                            <li>
                              <i className="fa fa-envelope-o" />
                              Email Us: Support@popolr.com
                            </li>
                            <li>
                              <i className="fa fa-fax" />
                              Fax: 123456
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="sub-footer">
            <div className="container">
              <div className="row">
                <div className="col-xl-6 col-md-6 col-sm-12">
                  <div className="footer-end">
                    <p>
                      <i className="fa fa-copyright" aria-hidden="true" /> 2020 copyright Riolabz
                    </p>
                  </div>
                </div>
                <div className="col-xl-6 col-md-6 col-sm-12">
                  <div className="payment-card-bottom">
                    <ul>
                      <li>
                        <a aria-label="Visa" href="#">
                          <img src={visa} alt="" />
                        </a>
                      </li>
                      <li>
                        <a aria-label="Mastercard" href="#">
                          <img src={mastercard} alt="" />
                        </a>
                      </li>
                      <li>
                        <a aria-label="Paypal" href="#">
                          <img src={paypal} alt="" />
                        </a>
                      </li>
                      <li>
                        <a aria-label="American express" href="#">
                          <img src={americanExp} alt="" />
                        </a>
                      </li>
                      <li>
                        <a aria-label="Discover" href="#">
                          <img src={discover} alt="" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;
