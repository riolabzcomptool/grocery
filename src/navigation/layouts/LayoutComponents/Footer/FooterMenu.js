import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const FooterMenu = ({ title, list }) => {
  // console.log(list);
  return (
    <div className="col-lg-4 footer-link">
      <div>
        <div className="footer-title">
          <h4>{title}</h4>
        </div>
        <div className="footer-content">
          <ul>
            {list.map(item => {
              const { footerStatus = true } = item;
              return (
                <li key={item.key}>
                  {footerStatus ? (
                    <Link
                      to={{
                        pathname: item.link || '/',
                        state: {
                          title: item.title,
                          slug: item.slug,
                          id: item.id,
                        },
                      }}
                    >
                      {item.title}
                    </Link>
                  ) : (
                      item.title
                    )}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </div>

  );
};

FooterMenu.propTypes = {
  title: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
};

export default FooterMenu;
