import React, { useCallback } from 'react';
import appStoreImg from 'assets/images/app/app-storw.png';
import playStoreImg from 'assets/images/app/play-store.png';
import Slider from 'react-slick';
import { useBrands } from 'hooks/api';

const sliderSettings = {
  arrows: true,
  slidesToShow: 3,
  infinite: true,
  speed: 500,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      },
    },
  ],
};

const BrandsSlider = () => {
  const onClickAnchor = useCallback(e => e.preventDefault(), []);

  const { data: res } = useBrands();

  if (res?.data)
    return (
      <section className="">
        <div className="container">
          <div className="row">
            <div className="col-xl-4 logo no-border">
              <h2 className="title"> brands</h2>
              <div className="logo-3 border-logo">
                <Slider {...sliderSettings}>
                  {res.data.map(i => (
                    <div className="logo-img">
                      <img
                        src={`/${i.image?.[0]?.thumbnail}`}
                        className=" img-fluid"
                        alt={i.title}
                      />
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
            <div className="col-xl-6 offset-xl-1">
              <div className="app-section">
                <div className="app-content">
                  <div>
                    <h5>download the big market app</h5>
                    <div className="app-buttons">
                      <button type="button" onClick={onClickAnchor}>
                        <img src={appStoreImg} className=" img-fluid" alt="" />
                      </button>
                      <button type="button" onClick={onClickAnchor}>
                        <img src={playStoreImg} className=" img-fluid" alt="" />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );

  return null;
};

export default BrandsSlider;
