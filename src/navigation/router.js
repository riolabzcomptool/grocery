import React, { Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Loader from 'components/Loader';
import { ConnectedRouter } from 'connected-react-router';
import PageSection from 'components/PageSection';
import Placeholder from 'components/Placeholder';
import IndexLayout from './layouts';
import routes from './routes';

// const NotFoundPage = () => {
//   console.log('in not found')
//   return 'Not found'
// }

// const Loader = () => {
//   console.log('in loader')
//   return 'Loading'
// }

class RouterJ extends React.Component {
  componentDidUpdate() {
    console.log('router componentDidUpdate', this.props);
  }

  render() {
    const { history } = this.props;
    return (
      <Suspense
        fallback={
          <PageSection>
            <Loader />
          </PageSection>
        }
      >
        <ConnectedRouter history={history}>
          <IndexLayout>
            <Switch>
              <Route exact path="/" render={() => <Redirect to="/home" />} />
              {routes.map(route => {
                return (
                  <Route
                    path={route.path}
                    component={route.component}
                    key={route.path}
                    exact={route.exact}
                  />
                );
              })}
              <Route
                component={() => (
                  <Placeholder
                    title="PAGE NOT FOUND!"
                    subtitle="The page you are looking for does not exist"
                    buttonText="Go back to home"
                    to="/"
                  />
                )}
              />
            </Switch>
          </IndexLayout>
        </ConnectedRouter>
      </Suspense>
    );
  }
}

export default RouterJ;
