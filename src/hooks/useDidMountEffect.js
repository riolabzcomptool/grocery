import { useEffect, useRef } from 'react';

const useDidMountEffect = (func, deps = []) => {
  const didMount = useRef(false);

  useEffect(() => {
    if (didMount.current) didMount.current = true;
    else didMount.current = true;
  }, [deps, func]);
};

export default useDidMountEffect;


// import { useEffect, useRef } from 'react';

// const useDidMountEffect = (func, deps = []) => {
//   const didMount = useRef(false);

//   useEffect(() => {
//     if (didMount.current) func();
//     else didMount.current = true;
//   }, [deps, func]);
// };

// export default useDidMountEffect;
