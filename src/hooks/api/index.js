import { useQuery, useMutation } from 'react-query';
import qs from 'qs';
import cleanDeep from 'clean-deep';
import isEmpty from 'lodash/isEmpty';

// import fetch from '_utils/fetch';
import callApi from '_utils/callApi';
import { CATALOG_API, BACKEND_API } from '_constants/api';

export const useProducts = (options = {}) => {
  // console.log(options);
  const url = CATALOG_API.getProducts;
  let queryStr = '?status=active&sort[priorityOrder]=1';
  Object.entries(options).forEach(([key, val]) => {
    queryStr += `&${key}=${val}`;
  });
  const key = `${url}${queryStr}`;
  // console.log('useProducts', options, queryStr);
  return useQuery(key, () => callApi(key));
};

export const useProductsTest = (options = {}, manual = false) => {
  // const url = CATALOG_API.getProducts;
  const url = CATALOG_API.getProductsQuery;
  let sortQuery = {
    priorityOrder: -1,
  };
  if (!isEmpty(options.sort)) {
    // sortQuery = {...sortQuery, ...options.sort}
    sortQuery = { ...options.sort };
  }
  console.log('useProductsTest sortQuery', sortQuery);
  const queryOptions = {
    ...options,
    status: 'active',
    sort: { ...sortQuery },
  };
  const queryStr = qs.stringify(cleanDeep(queryOptions));

  const key = `${url}?${queryStr}`;
  console.log('useProducts', options, queryStr);
  return useQuery(key, () => callApi(key), { manual });
};

export const useBrands = (options = {}, manual = false) => {
  console.log(options, manual);
  const optionsQuery = qs.stringify(options);
  const url = `${CATALOG_API.getBrands}?status=active&sort=priorityOrder&fields[]=name&fields[]=slug&fields[]=image&${optionsQuery}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useGetInformation = (slug, manual = false) => {
  const url = `${CATALOG_API.getInformations}/${slug}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useSimilarProducts = (organics = []) => {
  let queryStr = '?status=active&sortField=priorityOrder&sortOrder=descend&';
  organics.forEach((val, i) => {
    if (i === organics.length - 1) queryStr += `organic[]=${val}`;
    else queryStr += `organic[]=${val}&`;
    // console.log(queryStr);
  });

  // localhost:3004/api/catalog/v1/products?organic[]=5e44e70a40c1707895934dfa&organic[]=5e46a2f18b3898df11d67d36&status=active&sortField=priorityOrder&sortOrder=ascend
  const url = `${CATALOG_API.getProducts}${queryStr}`;
  return useQuery(url, () => callApi(url));
};

export const useBlogs = () => {
  const url = `${CATALOG_API.getBlogs}?status=active&sort=priorityOrder&feature=active`;
  return useQuery(url, () => callApi(url));
};

export const useCategories = (options = {}, manual = false) => {
  const optionsQuery = qs.stringify(options);
  const url = `${CATALOG_API.getCategories}?status=active&sort[priorityOrder]=1${
    Object.entries(optionsQuery).length > 0 ? `&${optionsQuery}` : ''
  }`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useAddresses = (_, manual = false) => {
  const url = `${CATALOG_API.getAddresses}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const usePrescriptions = (_, manual = false) => {
  const url = `${CATALOG_API.getPrescriptions}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useProduct = (id, manual = false) => {
  const url = `${CATALOG_API.getProducts}/${id}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useProductReviews = (id, manual = false) => {
  const url = `${CATALOG_API.getProductReviews}/${id}?status=active`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useUserOrders = (id, manual = false) => {
  const url = `${CATALOG_API.getUserOrders}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useOrder = (id, manual = false) => {
  const url = `${CATALOG_API.getUserOrders}/${id}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useUserReviews = (id, manual = false) => {
  const url = `${CATALOG_API.getUserReviews}`;
  return useQuery(url, () => callApi(url), { manual });
};

// id or slug
export const useGetBrand = (id, manual = false) => {
  const url = `${CATALOG_API.getBrands}/${id}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useGetCategory = (id, manual = false) => {
  const url = `${CATALOG_API.getCategories}/${id}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useGetCoupon = (id, manual = false) => {
  const url = `${BACKEND_API.getCoupen}/${id}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useVariants = (productId, manual = false) => {
  const url = `${CATALOG_API.getVariants}/${productId}?status=active`;
  return useQuery(url, () => callApi(url), { manual, retry: false });
};

export const useFeatureBanners = () => {
  const url = `${CATALOG_API.getBanners}?status=active&feature=active`;
  return useQuery(url, () => callApi(url));
};

export const useFeatureBlogs = () => {
  const url = `${CATALOG_API.getBlogs}?status=active&feature=active`;
  return useQuery(url, () => callApi(url));
};

export const usePaymentMethods = (manual = false) => {
  const url = `${BACKEND_API.paymentMethod}`;
  return useQuery(url, () => callApi(url, { manual }));
};

export const useGetCouponMutation = () => {
  const url = `${BACKEND_API.getCoupen}`;
  return useMutation(({ coupenCode }) => callApi(`${url}/${coupenCode}`));
};
