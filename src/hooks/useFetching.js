import { useState, useEffect } from 'react'
import callApi from '_utils/callApi'

const useFetching = (url, options = {}) => {
  // let headers = {}
  // if (typeof options.headers !== 'undefined') {
  //   headers = { ...options.headers }
  // }
  // headers = { ...headers, Authorization: `Bearer ${localStorage.getItem('token')}` }
  // options.headers = headers
  // console.log('in useFetching')
  const [loading, setLoading] = useState(false)
  const [response, setResponse] = useState(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    const fetchData = async () => {
      try {
        // console.log('starting fetch')
        // console.log(url, options)
        setLoading(true)
        const res = await callApi(url, options)
        // console.log('res', res)
        // const json = await res.json()
        setLoading(false)
        setResponse(res)
        // console.log('json', json)
        // setResponse(json)
      } catch (err) {
        setLoading(false)
        console.log('useFetching error', err)
        // console.log(err.message)
        setError({ message: err.message || 'Error fetching data', status: err.status })
      }
    }
    // console.log('will fetch data')
    fetchData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [url])

  return [{ response, loading, error }]
}

export default useFetching


// import { useState, useEffect } from 'react';
// import callApi from '_utils/callApi';

// /**
//  *
//  * @param {string} url
//  * @param {object} options api options
//  * @param {booleam} manual if need to fetch initially
//  */
// const useFetching = (url, options = {}, manual = true) => {
//   // let headers = {}
//   // if (typeof options.headers !== 'undefined') {
//   //   headers = { ...options.headers }
//   // }
//   // headers = { ...headers, Authorization: `Bearer ${localStorage.getItem('token')}` }
//   // options.headers = headers
//   // console.log('in useFetching')
//   const [loading, setLoading] = useState(false);
//   const [response, setResponse] = useState(null);
//   const [error, setError] = useState(null);
//   const [refetchFlag, setRefetchFlag] = useState(false);

//   const fetchData = async () => {
//     try {
//       // console.log('starting fetch')
//       // console.log(url, options)
//       setLoading(true);
//       const res = await callApi(url, options);
//       // console.log('res', res)
//       // const json = await res.json()
//       setLoading(false);
//       setResponse(res);
//       // console.log('json', json)
//       // setResponse(json)
//     } catch (err) {
//       setLoading(false);
//       console.log('useFetching error', err);
//       // console.log(err.message)
//       setError({ message: err.message || 'Error fetching data', status: err.status });
//     }
//   };

//   useEffect(() => {
//     // console.log('will fetch data')
//     if (manual) {
//       fetchData();
//     }
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, [url, refetchFlag]);

//   const refetch = () => {
//     setRefetchFlag(prev => !prev);
//   };

//   return [{ response, loading, error }, refetch];
// };

// export default useFetching;

