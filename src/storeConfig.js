import { routerMiddleware } from 'connected-react-router';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { logger } from 'redux-logger';
import {
  // createHashHistory,
  createBrowserHistory,
} from 'history';
import reducers from 'redux/reducers';
// import sagas from 'redux/sagas';

const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const routeMiddleware = routerMiddleware(history);
const middlewares = [sagaMiddleware, routeMiddleware];

if (process.env.NODE_ENV === 'development' && true) {
  
  middlewares.push(logger);
}

const store = createStore(reducers(history), compose(applyMiddleware(...middlewares)));
// sagaMiddleware.run(sagas);


export { history, store, sagaMiddleware };
