import React from 'react';
import Skeleton from 'react-loading-skeleton';
import classNames from 'classnames';

const InfoBanner = ({ title, subtitle, image, loading }) => {
  if (loading)
    return (
      <div
        className={classNames(
          'med-info',
          'wow',
          // { 'fadeInLeft': title && subtitle && image }
        )}
      >
        <div className="med-info-img">
          <Skeleton width={66} height={66} />
        </div>
        <div className="med-info-content">
          <h4>
            <Skeleton />
          </h4>
          <h5>
            <Skeleton />
          </h5>
        </div>
      </div>
    );
  return (
    <div
      className={classNames(
        'med-info',
        'wow',
        // { 'fadeInLeft': title && subtitle && image }
      )}
    >
      <div className="med-info-img">
        {(image && <img className="lazy" src={image} alt="" />) || <Skeleton />}
      </div>
      <div className="med-info-content">
        <h4>{title || <Skeleton />}</h4>
        <h5>{subtitle || <Skeleton />}</h5>
      </div>
    </div>
  );
};

export default InfoBanner;
