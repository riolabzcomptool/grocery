import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import { connect } from 'react-redux';
import Product from 'components/Product';
import { homeActions } from 'redux/actions';

class PopularHealthProducts extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: homeActions.GET_POPULAR_PRODUCTS_REQUEST,
    });
  }

  render() {
    const {
      loading,
      error,
      products, // title
    } = this.props;
    console.log(loading, error, products);
    return (
      // <section className="product-listing-wrapper wow fadeInUp">
      <>
        {loading && products.length <= 0 && (
          <div className="products-page-list-wrapper-loading">
            <Product key="1" />
            <Product key="2" />
            <Product key="3" />
          </div>
        )}
        {error && <div> Error loading popular products</div>}
        <div className="container">
          <div className="section-heading">
            <h2>
              <span className="head">Popular Health Products</span>{' '}
              <span className="more-span">
                <a href="/#"" className="view-all-btn">
                  View All
                </a>
              </span>
            </h2>
          </div>
          <OwlCarousel
            nav
            responsive={{
              0: { items: 2, loop: true },
              500: { items: 3 },
              664: { items: 4 },
              1000: { items: 5 },
            }}
            dots
            lazyLoad={false}
            lazyContent={false}
            className="owl-theme owl-carousel"
            margin={10}
          >
            {products.map(product => (
              <Product
                slug={product.slug}
                item
                key={product.key}
                title={product.name}
                description={product.description}
                price={product.price}
                img={product.image}
              />
            ))}
          </OwlCarousel>
        </div>
      </>
    );
  }
}

const mapStateToProps = ({ home }) => {
  const { popularProducts, popularProdsLoading, popularProductsError } = home;
  return {
    products: popularProducts,
    loading: popularProdsLoading,
    error: popularProductsError,
  };
};

export default connect(mapStateToProps)(PopularHealthProducts);
