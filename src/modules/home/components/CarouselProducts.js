import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

/* <script
src="https://code.jquery.com/jquery-3.3.1.min.js"
async
defer
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"
></script> */

const CarouselProducts = ({ title, children, viewAllLink }) => {
  console.log('%c view all link', 'color:white;background-color:pink');
  console.log(viewAllLink);
  return (
    <div className="container">
      <div className="section-heading">
        <h2>
          {title && <span className="head">{title}</span>}
          <span className="more-span">
            <Link className="view-all-btn" to={{ pathname: viewAllLink, state: { title } }}>
              View All
            </Link>
          </span>
        </h2>
      </div>

      <OwlCarousel
        nav
        responsive={{
          0: { items: 2, loop: true },
          500: { items: 3 },
          664: { items: 4 },
          1000: { items: 5 },
        }}
        dots
        lazyLoad
        lazyContent={false}
        className="owl-theme owl-carousel"
        margin={10}
      >
        {children}
      </OwlCarousel>
    </div>
  );
};

CarouselProducts.propTypes = {
  title: PropTypes.string,
  children: PropTypes.array.isRequired,
};

CarouselProducts.defaultProps = {
  title: '',
};

export default CarouselProducts;
