/* eslint-disable no-underscore-dangle */
import React, { useRef } from 'react';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';
// import Skeleton from 'react-loading-skeleton';
import { useProducts, useFeatureBanners } from 'hooks/api';
import HomeBanner from 'components/HomeBanner';
import Product from 'components/Product';

// import leftImg from 'assets/images/left.png';
// import rightImg from 'assets/images/right.png';
import PhoneVerify from 'components/PhoneVerify';
import TabsNew, { TabsNewPanel } from 'components/TabsNew';
import Categories from 'components/Categories';
// import Portal from 'components/Portal';
// import ProductDetailModal from 'components/ProductDetailModal';
// import { homeActions } from './actions';

const mapStateToProps = ({ home }) => {
  const {
    ads,
    adsLoading,
    adsError,
    infoBanner,
    infoLoading,
    infoBannerError,
    // popularProducts,
    // popularProdsLoading,
  } = home;
  console.log(ads, adsLoading, adsError);
  return {
    ads,
    adsError,
    adsLoading,
    infoBanner,
    infoLoading,
    infoBannerError,
    // popularProducts,
    // popularProdsLoading,
  };
};
const cates = [
  {
    id: '5e4a3ed8815d3e7565b4fa26',
    slug: 'fitness-and-development',
  },
  {
    id: '5e4a3e1f815d3e7565b4fa1d',
    slug: 'winter-section',
  },
];

const Home = () => {
  const shdFetch = useRef(true);
  console.log('shdFetch', shdFetch);
  const sample = ['1', '2', '3', '4', '5', '6', '7', '8'];
  // useEffect(() => {
  //   if (shdFetch.current) {
  //     dispatch({
  //       type: homeActions.TEST_SAGA,
  //     });
  //     dispatch({
  //       type: homeActions.GET_ADS,
  //     });
  //     dispatch({
  //       type: homeActions.GET_INFO_BANNER_REQUEST,
  //     });
  //     dispatch({
  //       type: homeActions.GET_POPULAR_PRODUCTS,
  //     });
  //   }
  //   return () => {
  //     shdFetch.current = false;
  //   };
  // }, [dispatch]);

  const { data: cat1ProdRes, isFetching: cat1Loading } = useProducts({
    category: cates[0].id,
    featured: true,
    limit: 10,
    fields: ['attributes', 'textDescription', 'pricing', 'images', 'slug', 'name'],
  });
  const { data: cat2ProdRes, isFetching: cat2Loading } = useProducts({
    category: cates[1].id,
    featured: true,
    limit: 10,
    fields: ['attributes', 'textDescription', 'pricing', 'images', 'slug', 'name'],
  });
  const { data: featRes, isFetching: featProdsLoading } = useProducts({
    featured: true,
    limit: 10,
    fields: ['attributes', 'textDescription', 'pricing', 'images', 'slug', 'name'],
  });
  // const { data: blogsRes, isFetching: blogsLoading } = useFeatureBlogs();
  const { data: bannerRes } = useFeatureBanners();
  // let blogDetails = [];
  let bannerDetails = [];
  let featuredProducts = [];
  let cat1Prods = [];
  let cat2Prods = [];

  console.log('mmma featProdsLoading', featProdsLoading);

  // if (blogsRes && blogsRes.data) blogDetails = blogsRes.data;
  if (bannerRes && bannerRes.data) bannerDetails = bannerRes.data;
  if (featRes && featRes.data) featuredProducts = featRes.data;
  if (cat1ProdRes && cat1ProdRes.data) cat1Prods = cat1ProdRes.data;
  if (cat2ProdRes && cat2ProdRes.data) cat2Prods = cat2ProdRes.data;

  return (
    <>
      <PhoneVerify />
      <HomeBanner banner={bannerDetails} />
      <Categories />
      <TabsNew>
        <TabsNewPanel label="Featured" id="featured">
          {featProdsLoading &&
            sample.map(i => (
              <div className="col-lg-2 col-sm-4 col-6 p-0">
                <Product key={i} loading />
              </div>
            ))}
          {!featProdsLoading &&
            featuredProducts &&
            featuredProducts.map(product => (
              <div className="col-lg-2 col-sm-4 col-6 p-0">
                <Product
                  slug={product.slug}
                  key={product._id}
                  id={product._id}
                  title={product.name}
                  description={product.textDescription}
                  listPrice={product.pricing?.listPrice}
                  salePrice={product.pricing?.salePrice}
                  img={product.images?.[0]?.thumbnail}
                />
              </div>
            ))}
        </TabsNewPanel>
        <TabsNewPanel label="Fitness & Development" id="fitness-development">
          {cat2Loading &&
            sample.map(i => (
              <div className="col-lg-2 col-sm-4 col-6 p-0">
                <Product key={i} loading />
              </div>
            ))}
          {!cat2Loading &&
            cat2Prods &&
            cat2Prods.map(product => (
              <div className="col-lg-2 col-sm-4 col-6 p-0">
                <Product
                  item
                  key={product._id}
                  slug={product.slug}
                  title={product.name}
                  description={product.textDescription}
                  listPrice={product.pricing?.listPrice}
                  salePrice={product.pricing?.salePrice}
                  img={product.images?.[0]?.thumbnail}
                />
              </div>
            ))}
        </TabsNewPanel>
        <TabsNewPanel label="Winter Section" id="winter-sectin">
          {cat1Loading &&
            sample.map(i => (
              <div className="col-lg-2 col-sm-4 col-6 p-0">
                <Product key={i} loading />
              </div>
            ))}

          {!cat1Loading &&
            cat1Prods &&
            cat1Prods.map(product => (
              <div className="col-lg-2 col-sm-4 col-6 p-0">
                <Product
                  item
                  key={product._id}
                  slug={product.slug}
                  title={product.name}
                  description={product.textDescription}
                  listPrice={product.pricing?.listPrice}
                  salePrice={product.pricing?.salePrice}
                  img={product.images?.[0]?.thumbnail}
                />
              </div>
            ))}
        </TabsNewPanel>
      </TabsNew>
      {/* <section className=" tab-layout1 section-b-space">
        <div className="theme-tab">
          <ul className="tabs border-top-0">
            <li className="current">
              <a href="tab-1">Vegetables</a>
            </li>
            <li className="">
              <a href="tab-2">Snacks</a>
            </li>
            <li className="">
              <a href="tab-3">Food grains</a>
            </li>
            <li className="">
              <a href="tab-4">bakery</a>
            </li>
          </ul>
          <div className="tab-content-cls ratio_square">
            <div id="tab-1" className="tab-content active default" style={styles.block}>
              <div className="container shadow-cls">
                <div className="drop-shadow">
                  <div className="left-shadow">
                    <img src={leftImg} alt="" className=" img-fluid" />
                  </div>
                  <div className="right-shadow">
                    <img src={rightImg} alt="" className=" img-fluid" />
                  </div>
                </div>
                <div className="row border-row1 m-0">
                  {featProdsLoading && (
                    <>
                      <Product key="1" loading />
                      <Product key="2" loading />
                      <Product key="3" loading />
                      <Product key="4" loading />
                      <Product key="5" loading />
                      <Product key="6" loading />
                      <Product key="7" loading />
                    </>
                  )}
                  {!featProdsLoading &&
                    featuredProducts &&
                    featuredProducts.map(product => (
                      <div className="col-lg-2 col-sm-4 col-6 p-0">
                        <Product
                          slug={product.slug}
                          key={product._id}
                          id={product._id}
                          title={product.name}
                          description={product.textDescription}
                          price={product.pricing.salePrice}
                          img={product.images[0].thumbnail}
                        />
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
          <div className="tab-content-cls ratio_square">
            <div id="tab-2" className="tab-content active default">
              <div className="container shadow-cls">
                <div className="drop-shadow">
                  <div className="left-shadow">
                    <img src={leftImg} alt="" className=" img-fluid" />
                  </div>
                  <div className="right-shadow">
                    <img src={rightImg} alt="" className=" img-fluid" />
                  </div>
                </div>
                <div className="row border-row1 m-0">
                  {cat2Loading && (
                    <div className="col-lg-2 col-sm-4 col-6 p-0">
                      <Product key="1" loading />
                      <Product key="2" loading />
                      <Product key="3" loading />
                      <Product key="4" loading />
                      <Product key="5" loading />
                      <Product key="6" loading />
                      <Product key="7" loading />
                    </div>
                  )}
                  {!cat2Loading &&
                    cat2Prods &&
                    cat2Prods.map(product => (
                      <Product
                        item
                        key={product._id}
                        slug={product.slug}
                        title={product.name}
                        description={product.textDescription}
                        price={product.pricing.salePrice}
                        img={product.images[0].thumbnail}
                      />
                    ))}
                </div>
              </div>
            </div>
          </div>
          <div className="tab-content-cls ratio_square">
            <div id="tab-3" className="tab-content active default">
              <div className="container shadow-cls">
                <div className="drop-shadow">
                  <div className="left-shadow">
                    <img src={leftImg} alt="" className=" img-fluid" />
                  </div>
                  <div className="right-shadow">
                    <img src={rightImg} alt="" className=" img-fluid" />
                  </div>
                </div>
                <div className="row border-row1 m-0">
                  {cat1Loading && (
                    <div className="col-lg-2 col-sm-4 col-6 p-0">
                      <Product key="1" loading />
                      <Product key="2" loading />
                      <Product key="3" loading />
                      <Product key="4" loading />
                      <Product key="5" loading />
                      <Product key="6" loading />
                      <Product key="7" loading />
                    </div>
                  )}

                  {!cat1Loading &&
                    cat1Prods &&
                    cat1Prods.map(product => (
                      <Product
                        item
                        key={product._id}
                        slug={product.slug}
                        title={product.name}
                        description={product.textDescription}
                        price={product.pricing.salePrice}
                        img={product.images[0].thumbnail}
                      />
                    ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> */}

      {/* <section className="bg-grey-lite">
        <div className="container">
          <div className="med-info-wrapper">
            {/* {infoLoading && infoBanner.length <= 0 && (
            {infoLoading && infoBanner.length === 0 && (
              <>
                <InfoBanner loading />
                <InfoBanner loading />
              </>
            )}
            
            {infoBanner.length > 0 &&
              !infoLoading &&
              infoBanner.map(item => (
                <InfoBanner
                  key={item.id}
                  title={item.title}
                  subtitle={item.subtitle}
                  image={item.image}
                />
              ))}
          </div>
        </div>
      </section> */}
      {/* 
      <section className="product-listing-wrapper wow fadeInUp">
        {cat1Loading && (
          <>
            <div className="section-heading">
              <h2>
                <span className="head">
                  <Skeleton />
                </span>
              </h2>
            </div>
            <div className="products-page-list-wrapper-loading">
              <Product key="1" loading />
              <Product key="2" loading />
              <Product key="3" loading />
              <Product key="4" loading />
              <Product key="5" loading />
              <Product key="6" loading />
              <Product key="7" loading />
            </div>
          </>
        )}
        {!cat1Loading && cat1Prods && (
          <Carousel
            title="Fitness & Development"
            viewAllLink={`/products/category/${cates[0].slug}`}
          >
            {cat1Prods.map(product => (
              <Product
                item
                key={product._id}
                slug={product.slug}
                title={product.name}
                description={product.textDescription}
                price={product.pricing.salePrice}
                img={product.images[0].thumbnail}
              />
            ))}
          </Carousel>
        )}
      </section> */}

      {/* <Product title={title} description={description} price={price} img={img} /> */}

      {/* <section className="product-listing-wrapper bg-grey-lite skin-care wow fadeInUp">
        {cat2Loading && (
          <>
            <div className="section-heading">
              <h2>
                <span className="head">
                  <Skeleton />
                </span>
              </h2>
            </div>
            <div className="products-page-list-wrapper-loading">
              <Product key="1" loading />
              <Product key="2" loading />
              <Product key="3" loading />
              <Product key="4" loading />
              <Product key="5" loading />
              <Product key="6" loading />
              <Product key="7" loading />
            </div>
          </>
        )}
        {!cat2Loading && cat2Prods && (
          <Carousel title="Winter Section" viewAllLink={`/products/category/${cates[1].slug}`}>
            {cat2Prods.map(product => (
              <Product
                slug={product.slug}
                item
                id={product._id}
                key={product._id}
                title={product.name}
                description={product.textDescription}
                price={product.pricing.salePrice}
                img={product.images[0].thumbnail}
              />
            ))}
          </Carousel>
        )}
      </section> */}
    </>
  );
};

export default connect(mapStateToProps)(Home);
