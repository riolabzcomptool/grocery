import { homeActions } from './actions';

const initialState = {
  ads: [],
  adsError: '', // {}
  adsLoading: false,
  popularProducts: [],
  popularProdsLoading: false,
  popularProductsError: '',
  infoBanner: [],
  infoLoading: false,
  infoBannerError: ''

};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case homeActions.TEST: {
      console.log('in test home reducer');
      return { ...state, ...action.payload };
    }
    case homeActions.SET_STATE:
      return { ...state, ...action.payload };
    case homeActions.ADS_REQUEST:
      return { ...state, adsLoading: true };
    case homeActions.FETCH_ADS_SUCCESS:
      return { ...state, ads: action.payload, adsLoading: false };
    case homeActions.FETCH_ADS_ERROR:
      return { ...state, adsError: action.payload, adsLoading: false };
    case homeActions.GET_POPULAR_PRODUCTS_REQUEST:
      return { ...state, popularProdsLoading: true };
    case homeActions.GET_POPULAR_PRODUCTS_SUCCESS:
      return { ...state, popularProducts: action.payload, popularProdsLoading: false };
    case homeActions.GET_POPULAR_PRODUCTS_ERROR:
      return { ...state, popularProductsError: action.payload, popularProdsLoading: false };
    case homeActions.GET_INFO_BANNER_REQUEST:
      return { ...state, infoLoading: true };
    case homeActions.GET_INFO_BANNER_SUCCESS:
      return { ...state, infoBanner: action.payload, infoLoading: false };
    case homeActions.GET_INFO_BANNER_ERROR:
      return { ...state, infoBannerError: action.payload, infoLoading: false };
    default:
      return state;
  }
}
