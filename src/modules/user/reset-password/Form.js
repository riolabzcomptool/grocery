/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { withFormik } from 'formik';
import Input from 'components/Input';
import { resetPassword } from 'services/user';
import Button from 'components/Button';
import { updatePwdEmailSchema } from '_utils/Schemas';
// import Input from 'components/Input'
import FormItem from 'components/FormItem';
import { withRouter } from 'react-router';
import classNames from 'classnames';

const ResetPasswordForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    isSubmitting,
    status,
    history,
    // handleReset
  } = props;

  useEffect(() => {
    let timer = null;
    if (status && status.success) {
      timer = setTimeout(() => history.replace('/user/login'), 3000);
    }
    return () => {
      clearTimeout(timer);
    };
  }, [status, history]);

  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  console.log(props);

  return (
    <div className="text-left">
      <FormItem label="Email">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          name="email"
          errors={errors.email && touched.email ? errors.email : ''}
        />
      </FormItem>
      <FormItem label="New Password">
        <Input
          type="password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.newPassword}
          name="newPassword"
          errors={errors.newPassword && touched.newPassword ? errors.newPassword : ''}
        />
      </FormItem>
      <FormItem label="Confirm Password">
        <Input
          type="password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.confirmPassword}
          name="confirmPassword"
          errors={errors.confirmPassword && touched.confirmPassword ? errors.confirmPassword : ''}
        />
      </FormItem>
      <div className="log-forgot flex space-between align-center">
        {/* <div className="spinner-border spinner-border-sm" role="status">
          <span className="sr-only">Loading...</span>
        </div> */}
        <Button
          type="button"
          loading={isSubmitting}
          icon="fa-arrow-right"
          onClick={handleSubmit}
          className="btn big-login-btn"
        >
          Continue
          {/* <img className="lazy" src="/resources/images/kickill-right-long-arrow.svg" alt="" /> */}
        </Button>
        <span>
          Back to{' '}
          <Link to="/user/login" className="btn big-login-btn ml-10 capitalize">
            Login
          </Link>
        </span>
      </div>
      <div
        className={classNames('field-notify', 'lr-padding-zero', {
          hidden: !status || !(status && status.message),
        })}
      >
        {status && status.message}
        {/* Email does not exist in our records. Please enter a valid email and try again */}
      </div>
    </div>
  );
};

const MyEnhancedForm = withRouter(
  withFormik({
    mapPropsToValues: ({ newPassword = '', confirmPassword = '', email = '' }) => ({
      newPassword,
      email,
      confirmPassword,
    }),

    validationSchema: updatePwdEmailSchema,

    handleSubmit: async (values, { props, setSubmitting, setStatus }) => {
      console.log(props, setSubmitting);
      const { match } = props;
      const { resetPasswordToken } = match.params;
      const { newPassword: password, email } = values;
      const res = await resetPassword({ email, password, resetPasswordToken });
      console.log(res);
      setStatus({ success: res && res.success, message: res && res.message });
      setSubmitting(false);
    },

    displayName: 'ResetPasswordForm',
  })(ResetPasswordForm),
);

export default MyEnhancedForm;
