import React, { Component } from 'react';
import Hero from 'components/Hero';
import Banner from 'components/Banner';
import { connect } from 'react-redux';

import PageSection from 'components/PageSection';

import toaster from 'toasted-notes';
import ResetPasswordForm from './Form';

class ResetPassword extends Component {
  // handleFormSubmit = (values) => {

  // }

  componentDidMount() {
    const { user, history } = this.props;
    console.log(this.props);
    if (user.isLogged) {
      history.replace('/home');
      toaster.notify('Already logged in!', {
        duration: 5000,
      });
    }
  }

  render() {
    return (
      <PageSection>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="login-area-wrapper">
                <div className="login-area">
                  <div className="login-head-wrapper">
                    <h2 className="login-head">
                      <span>Reset</span> Password
                    </h2>
                  </div>
                  <ResetPasswordForm />
                </div>
                <Hero image="/resources/images/kickill-login-bg.jpg">
                  <Banner
                    title="zapkart"
                    subtitle="The Online Pharmacy"
                    slogan="WE Ensure the Lowest and Best Price for each MEDICINES"
                  />
                </Hero>
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    );
  }
}

export default connect(({ user }) => ({ user }))(ResetPassword);
