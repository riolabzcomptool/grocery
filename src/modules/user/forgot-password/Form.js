/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Link } from 'react-router-dom';
import { withFormik } from 'formik';
import Input from 'components/Input';
import Button from 'components/Button';
import { ForgotPasswordSchema } from '_utils/Schemas';
import classNames from 'classnames';
// import Input from 'components/Input'
import FormItem from 'components/FormItem';
import { forgotPassword } from 'services/user';

const ForgotPasswordForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    isSubmitting,
    status,
    // handleReset
  } = props;

  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  console.log(props);
  return (
    <div className="text-left">
      <FormItem label="Email address" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          name="email"
          errors={errors.email && touched.email ? errors.email : ''}
        />
      </FormItem>
      <div className="log-forgot flex space-between align-center">
        {/* <div className="spinner-border spinner-border-sm" role="status">
          <span className="sr-only">Loading...</span>
        </div> */}
        <Button
          type="button"
          loading={isSubmitting}
          icon="fa-arrow-right"
          onClick={handleSubmit}
          className="btn big-login-btn"
        >
          Continue 
          {/* <img className="lazy" src="/resources/images/kickill-right-long-arrow.svg" alt="" /> */}
        </Button>
        <span>
          Back to{' '}
          <Link to="/user/login" className="btn big-login-btn ml-10 capitalize">
            Login
          </Link>
        </span>
      </div>

      <div className={classNames('field-notify', 'lr-padding-zero', { hidden: !status })}>
        {status}
        {/* Email does not exist in our records. Please enter a valid email and try again */}
      </div>
    </div>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: ({ email = '' }) => ({
    email,
  }),

  validationSchema: ForgotPasswordSchema,

  handleSubmit: async (values, { props, setSubmitting, setStatus }) => {
    console.log(props, setSubmitting);
    const { email } = values;
    const msg = await forgotPassword({ email });
    setStatus(msg);
    setSubmitting(false);
  },
  hello: 'hello',
  displayName: 'ForgotPasswordForm',
})(ForgotPasswordForm);

export default MyEnhancedForm;
