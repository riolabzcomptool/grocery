import React, { Component } from 'react'
import PageSection from 'components/PageSection'
import Hero from 'components/Hero'
import Banner from 'components/Banner'
import RegisterForm from './Form'

class Register extends Component {

  render() {
      console.log(this.props)
    return (
      <PageSection className="bg-grey-lite">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="login-area-wrapper">
                <div className="login-area register-area">
                  <div className="login-head-wrapper">
                    <h2 className="login-head">
                      <span>Register to</span> Zapkart
                    </h2>
                    <h5 className="login-para">
                      Lorem ipsum dolor sit a ut labore et dolore magna aliqua. Quis ipsum
                      suspendisse ultrices gravida.{' '}
                    </h5>
                  </div>
                  <RegisterForm />
                </div>
                <Hero image="/resources/images/kickill-login-bg.jpg">
                  <Banner
                    title="zapkart"
                    subtitle="The Online Pharmacy"
                    slogan="WE Ensure the Lowest and Best Price for each MEDICINES"
                  />
                </Hero>
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    )
  }
}

export default Register
