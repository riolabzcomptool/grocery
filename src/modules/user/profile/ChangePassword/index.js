
import React from 'react';
import { Head as InfoHead } from 'components/Info';
import NotifyMsg from 'components/NotifyMsg';
import isEmpty from 'lodash/isEmpty';
// import Button from 'components/Button';
import { verifyPassword, updatePassword } from 'services/user';
import CurrentPasswordForm from './CurrentPasswordForm';
import NewPasswordForm from './NewPasswordForm';

class MyForm extends React.Component {
  state = {
    activeIndex: 1,
    isPass: true,
    message: null,
  };

  componentWillUnmount() {
    if (this.timer) clearTimeout(this.timer);
  }

  clearMessage = () => {
    const { message } = this.state;
    if (!isEmpty(message)) this.setState({ message: null });
  };

  handleSubmitCur = async vals => {
    this.clearMessage();
    const res = await verifyPassword(vals);
    if (res.success) this.setState({ activeIndex: 2 });
    else this.setState({ message: res.message });
  };

  handleSubmitNew = async a => {
    const { isPass } = this.state;
    this.clearMessage();
    const res = await updatePassword(a);
    if (res.success) {
      this.setState({ message: 'Password updated!' });
      if (isPass)
        this.timer = setTimeout(() => {
          this.setState({ isPass: false });
        }, 3000);
    } else this.setState({ message: res.message });
  };

  toggleShow = () => {
    this.setState(prevState => {
      const isShow = !prevState.isPass;
      let toReturn = {
        isPass: isShow,
        message: null,
      };
      if (!isShow) toReturn = { ...toReturn, activeIndex: 1 };
      return { ...toReturn };
    });
  };

  render() {
    const { activeIndex, isPass, message } = this.state;
    return (
      <>
        <InfoHead
          className="information-head-wrapper"
          title="Change Password"
          icon="/resources/images/kickill-help-icon.png"
        >
          {/* <Button
            type="button"
            onClick={this.toggleShow} // className="extra-small-btn"
          >
            <img className="lazy edit-icon" src="/resources/images/kickill-down-arrow.svg" alt="" />
          </Button> */}
        </InfoHead>
        {isPass ? (
          <>
            {activeIndex === 1 && (
              <CurrentPasswordForm active={activeIndex === 1} onSubmit={this.handleSubmitCur} />
            )}
            {activeIndex === 2 && (
              <NewPasswordForm active={activeIndex === 2} onSubmit={this.handleSubmitNew} />
            )}
            <NotifyMsg message={message} />
          </>
        ) : null}
      </>
    );
  }
}

export default MyForm;
