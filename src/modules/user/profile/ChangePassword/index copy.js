/* eslint-disable react/jsx-curly-newline */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-indent */
import React from 'react';
import { Transition } from 'react-spring/renderprops';
import { Head as InfoHead } from 'components/Info';
import CurrentPasswordForm from './CurrentPasswordForm';
import NewPasswordForm from './NewPasswordForm';

class MyForm extends React.Component {
  state = {
    activeIndex: '1',
    isPass: false,
  };

  handleSubmitCur = a => {
    console.log('yo', a);
    setTimeout(() => {
      this.setState({ activeIndex: '2' });
      // setSubmitting(false)
    }, 1000);
  };

  handleSubmitNew = a => {
    console.log('yo new pass', a);
  };

  render() {
    const { activeIndex, isPass } = this.state;
    return (
      <>
        <InfoHead
          className="information-head-wrapper"
          title="Change Password"
          icon="/resources/images/kickill-help-icon.png"
        >
          <button
            type="button"
            onClick={() =>
              this.setState(prevState => {
                return { isPass: !prevState.isPass };
              })
            }
            // className="extra-small-btn"
          >
            <img className="lazy edit-icon" src="/resources/images/kickill-down-arrow.svg" alt="" />
          </button>
        </InfoHead>

        <Transition
          items={activeIndex === '1' && isPass}
          from={{ transform: 'translate3d(0px,-100px,0)' }}
          enter={{ transform: 'translate3d(0px,0px,0)' }}
          leave={{ transform: 'translate3d(-500px,0,0)' }}
          config={{ duration: 400 }}
        >
          {isPassL =>
            isPassL
              ? props => (
              <div style={props}>
                    <CurrentPasswordForm
                      style={{ position: 'absolute' }}
                      active={activeIndex === '1'}
                      onSubmit={a => this.handleSubmitCur(a)}
                    />
                  </div>
                )
              : ''
          }
        </Transition>
        <Transition
          items={activeIndex === '2'}
          from={{ transform: 'translate3d(2000px,0px,0)' }}
          enter={{ transform: 'translate3d(0px,0px,0)' }}
          config={{ duration: 500 }}
        >
          {isPassL =>
            isPassL
              ? props => (
                  <div style={props}>
                    <NewPasswordForm
                      style={{ position: 'absolute' }}
                      active={activeIndex === '2'}
                      onSubmit={a => this.handleSubmitNew(a)}
                    />
                  </div>
                )
              : ''
          }
        </Transition>
      </>
    );
  }
}

export default MyForm;
