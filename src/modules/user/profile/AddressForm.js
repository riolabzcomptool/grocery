import React from 'react';
import { withFormik } from 'formik';
import { UserAddressSchema } from '_utils/Schemas';
import Input from 'components/Input';
import FormItem from 'components/FormItem';
import Select from 'components/Input/Select';
import Button from 'components/Button';

const marginAuto = {
  marginRight: 'auto',
};

const MyForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    onCancel,
    onDelete,
    loadingSubmit,
    loadingDelete,
  } = props;

  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  // console.log(errors);
  const attressTypeOptions = [
    {
      value: '',
      selected: true,
      disabled: true,
      hidden: true,
      label: '--Select address type--',
    },
    {
      value: 'home',
      label: 'Home',
    },
    {
      value: 'office',
      label: 'Office',
    },
    {
      value: 'other',
      label: 'Other',
    },
  ];

  const handleDelete = () => onDelete(values.id);

  return (
    <div
      style={{
        padding: '1rem',
      }}
    >
      <form>
        <FormItem horizontal label="Save address as">
          <Select
            value={values.addressType}
            options={attressTypeOptions}
            onChange={handleChange}
            onBlur={handleBlur}
            name="addressType"
            errors={errors.addressType && touched.addressType ? errors.addressType : ''}
          />
          {/* <Input
            placeholder="Address Name"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.addressType}
            name="addressType"
            errors={errors.addressType && touched.addressType ? errors.addressType : ''}
          /> */}
        </FormItem>
        <FormItem horizontal label="Name">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="name"
            value={values.name}
            errors={errors.name && touched.name ? errors.name : ''}
          />
        </FormItem>
        <FormItem horizontal label="Phone No">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="phoneNo"
            value={values.phoneNo}
            errors={errors.phoneNo && touched.phoneNo ? errors.phoneNo : ''}
          />
        </FormItem>
        <FormItem horizontal label="House No">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="houseNo"
            value={values.houseNo}
            errors={errors.houseNo && touched.houseNo ? errors.houseNo : ''}
          />
        </FormItem>
        <FormItem horizontal label="Street">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="street"
            value={values.street}
            errors={errors.street && touched.street ? errors.street : ''}
          />
        </FormItem>
        <FormItem horizontal label="Landmark">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="landmark"
            value={values.landmark}
            errors={errors.landmark && touched.landmark ? errors.landmark : ''}
          />
        </FormItem>
        <FormItem horizontal label="City">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="city"
            value={values.city}
            errors={errors.city && touched.city ? errors.city : ''}
          />
        </FormItem>
        <FormItem horizontal label="State">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="state"
            value={values.state}
            errors={errors.state && touched.state ? errors.state : ''}
          />
        </FormItem>
        <FormItem horizontal label="Pincode">
          <Input
            onChange={handleChange}
            onBlur={handleBlur}
            name="pincode"
            value={values.pincode}
            errors={errors.pincode && touched.pincode ? errors.pincode : ''}
          />
        </FormItem>
        <div className="modal-footer">
          {values.id && (
            <Button
              type="button"
              danger
              style={marginAuto}
              loading={loadingDelete}
              onClick={handleDelete}
              className="btn save-btn"
            >
              Delete
            </Button>
          )}
          <Button
            type="submit"
            loading={loadingSubmit}
            onClick={handleSubmit}
            className="btn save-btn"
          >
            Save changes
          </Button>
          <Button
            type="button"
            onClick={onCancel}
            // className="btn btn-secondary"
            className="btn save-btn"
            data-dismiss="modal"
          >
            Cancel
          </Button>
        </div>
      </form>
    </div>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: ({
    addressType = '',
    name = '',
    phoneNo = '',
    pincode = '',
    address = {},
    id = '',
  }) => ({
    addressType,
    name,
    phoneNo,
    pincode,
    id,
    houseNo: address.houseNo,
    street: address.street,
    landmark: address.landmark,
    city: address.city,
    state: address.state,
  }),

  validationSchema: UserAddressSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    // console.log(props);
    props.onSubmit(values);
    setTimeout(() => {
      // alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  },

  displayName: 'BasicForm',
})(MyForm);

export default MyEnhancedForm;
