import React, { Component, Suspense } from 'react';
import PageSection from 'components/PageSection';
import { connect } from 'react-redux';
import toaster from 'toasted-notes';
import {
  uploadPrescriptions,
  getPrescriptions,
  updatePrescriptions,
  deletePrescription,
} from 'services/prescription';
import Info, { Head as InfoHead } from 'components/Info';
import TabsPlain, { TabsPlainPanel } from 'components/TabsPlain';
import Loader from 'components/Loader';
import OrderList from 'modules/user/orders/components/OrderList';
import { userActions } from 'redux/actions';
import Placeholder from 'components/Placeholder';
import UploadPrescription from './UploadPrescription';

const Authorize = React.lazy(() => import('components/Authorize'));
const EditProfileForm = React.lazy(() => import('./EditProfileForm'));
const AddressList = React.lazy(() => import('./AddressList'));
// import ChangePasswordForm from './ChangePasswordForm'
const ChangePassword = React.lazy(() => import('./ChangePassword'));
const WishList = React.lazy(() => import('modules/wishlist'));

class UserProfile extends Component {
  state = {
    prescriptions: null,
    addressData: [],
    isAddrModalOpen: false,
    currentEditItem: {},
    loadingAddrEdit: false,
    loadingdelete: false,
    message: '',
    // prescriptionList:[]
  };

  componentDidMount() {
    this.fetchData();
  }

  componentWillUnmount() {
    if (this.timer) clearTimeout(this.timer);
  }

  fetchData = async () => {
    const presData = await getPrescriptions();

    console.log('presData', presData);

    this.setState({
      prescriptions:
        presData && presData.length > 0
          ? presData.map(i => ({
              thumbnail: i.thumbnail,
              url: i.url,
              uid: i.id,
              id: i.id,
            }))
          : null,
    });
  };

  handleDeletePres = async id => {
    const res = await deletePrescription(id);
    if (res && res.success) {
      toaster.notify('Prescription deleted');
      this.setState(prev => ({
        ...prev,
        prescriptions: prev.prescriptions.filter(i => String(i.uid) !== String(id)),
      }));
    }
    if (res && res.error) toaster.notify(res.error);
  };

  handlePresUpload = async ({ fileList }) => {
    const { prescriptions } = this.state;
    console.log('handlePresUpload', fileList, prescriptions);
    const res = !prescriptions
      ? await uploadPrescriptions({ fileList })
      : await updatePrescriptions({ fileList, origFileList: prescriptions });
    if (res && res.error) {
      toaster.notify(res.error);
      return false;
    }
    if (res && res.prescriptions) {
      this.setState({
        prescriptions: res.prescriptions.map(i => ({
          thumbnail: i.thumbnail,
          url: i.url,
          uid: i.id,
          id: i.id,
        })),
      });

      toaster.notify(`Prescription(s) ${prescriptions ? 'updated' : 'uploaded'}`);
      return true;
    }
    return false;
  };

  handleLogout = () => {
    console.log('need to logout', this.props);
    const { dispatch } = this.props;
    dispatch({
      type: userActions.LOGOUT,
    });
  };

  wrapSuspense = a => {
    return (
      <Suspense
        fallback={
          <Info>
            <Loader />
          </Info>
        }
      >
        {a}
      </Suspense>
    );
  };

  render() {
    const { message, prescriptions } = this.state;
    console.log('message', message);

    return (
      <Authorize
        fallback={
          <Placeholder
            title="Please login!"
            subtitle="Login to view your profile"
            buttonText="Login"
            to="/user/login"
          />
        }
      >
        <PageSection className="bg-grey-lite">
          <TabsPlain>
            <TabsPlainPanel label="Edit Account">
              {this.wrapSuspense(
                <Info>
                  <EditProfileForm />
                </Info>,
              )}
            </TabsPlainPanel>
            <TabsPlainPanel label="Password">
              {this.wrapSuspense(
                <Info>
                  <ChangePassword />
                </Info>,
              )}
            </TabsPlainPanel>
            <TabsPlainPanel label="Address Book">
              {this.wrapSuspense(
                <Info>
                  <AddressList />
                </Info>,
              )}
            </TabsPlainPanel>
            <TabsPlainPanel label="Wish List">
              {this.wrapSuspense(
                <Info>
                  <WishList />
                </Info>,
              )}
            </TabsPlainPanel>
            <TabsPlainPanel label="Order History">
              {this.wrapSuspense(<OrderList outerClass="pt-0" fullSpan />)}
            </TabsPlainPanel>
            <TabsPlainPanel label="Prescriptions">
              {this.wrapSuspense(
                <Info>
                  <InfoHead title="My prescriptions" />
                  <UploadPrescription
                    files={prescriptions}
                    handleUpload={this.handlePresUpload}
                    onRemove={this.handleDeletePres}
                  />
                </Info>,
              )}
            </TabsPlainPanel>
            <TabsPlainPanel label="Logout" onClick={this.handleLogout} />
          </TabsPlain>
        </PageSection>
      </Authorize>
    );
  }
}

export default connect()(UserProfile);
