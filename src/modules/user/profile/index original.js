import React, { Component } from 'react';
import PageSection from 'components/PageSection';
import Modal from 'components/Modal';
import Button from 'components/Button';
import toaster from 'toasted-notes';
import NotifyMsg from 'components/NotifyMsg';
import styled from 'styled-components';
import Toaster from 'components/Toaster';
import Info, { Head as InfoHead } from 'components/Info';
import Authorize from 'components/Authorize';
import {
  uploadPrescriptions,
  getPrescriptions,
  updatePrescriptions,
  deletePrescription,
} from 'services/prescription';
import { getAddresses, editAddress, addAddress, deleteAddress } from 'services/address';
import UserAddress from './components/UserAddress';
import EditProfileForm from './EditProfileForm';
import AddressForm from './AddressForm';
// import ChangePasswordForm from './ChangePasswordForm'
import ChangePassword from './ChangePassword';
import UploadPrescription from './UploadPrescription';

const marginBottomStyle = { marginBottom: '.5rem' };
const marginRightStyle = { marginRight: '.5rem' };

const StyledNotifyMsg = styled.div`
  text-align: center;
  margin-top: -2rem;
`;

class UserProfile extends Component {
  state = {
    prescriptions: null,
    addressData: [],
    isAddrModalOpen: false,
    currentEditItem: {},
    loadingAddrEdit: false,
    loadingdelete: false,
    message: '',
    // prescriptionList:[]
  };

  componentDidMount() {
    this.fetchData();
  }

  componentWillUnmount() {
    if (this.timer) clearTimeout(this.timer);
  }

  fetchData = async () => {
    const presData = await getPrescriptions();
    const addressData = await getAddresses();
    console.log('presData', presData);

    this.setState({
      addressData: addressData && addressData.length > 0 ? addressData : [],
      prescriptions:
        presData && presData.length > 0
          ? presData.map(i => ({
            thumbnail: i.thumbnail,
            url: i.url,
            uid: i.id,
            id: i.id,
          }))
          : null,
    });
  };

  closeModalAfterDelay = (delay = 3000) => {
    this.timer = setTimeout(() => this.handleCloseAddrModal(), delay);
  };

  handleEditAddressClick = event => {
    const { dataset } = event.currentTarget;
    console.log(dataset, dataset.id);
    console.log('clicked to edit address', dataset.id);
    const { addressData } = this.state;
    let currentEditItem = {};
    if (dataset.id) {
      // edit current
      currentEditItem = addressData.find(item => item.id === Number(dataset.id));
    }
    console.log(currentEditItem);
    this.setState({
      isAddrModalOpen: true,
      currentEditItem,
    });
  };

  handleDeleteAddress = async id => {
    this.setState({
      loadingdelete: true,
    });
    const isDeleted = await deleteAddress(id);
    this.setState({
      loadingdelete: false,
    });
    if (isDeleted) {
      this.setState(prev => ({
        addressData: prev.addressData.filter(i => i.id !== id),
        message: 'Deleted successsfully',
      }));
      this.closeModalAfterDelay(2000);
    } else
      this.setState({
        message: 'Error deleting. Please try again later',
      });
  };

  handleCloseAddrModal = () => {
    console.log('closing modal');
    this.setState({ isAddrModalOpen: false, message: '' });
  };

  handleSaveAddressForm = async obj => {
    console.log('saving address form', obj);
    // const { addressData } = this.state;
    this.setState({
      loadingAddrEdit: true,
    });
    const { address: editedAddr, message } = obj.id
      ? await editAddress(obj)
      : await addAddress(obj);
    if (editedAddr)
      this.setState(prev => {
        let { addressData } = prev;
        console.log('address data', addressData);
        const index = addressData.findIndex(i => i.id === obj.id);
        if (index > -1) addressData[index] = { ...editedAddr };
        else
          addressData = [
            {
              ...editedAddr,
            },
            ...addressData,
          ];
        console.log('new address data', addressData);
        this.closeModalAfterDelay(2000);
        return {
          loadingAddrEdit: false,
          addressData,
          message: obj.id ? 'Edited successfully' : 'Added successfully',
        };
      });
    else
      this.setState({
        loadingAddrEdit: false,
        message,
      });
  };

  handleDeletePres = async id => {
    const res = await deletePrescription(id);
    if (res && res.success) {
      toaster.notify("Prescription deleted")
      this.setState(
        prev => ({
          ...prev,
          prescriptions: prev.prescriptions.filter(i => String(i.uid) !== String(id)),
        })
      );
    }
    if (res && res.error) toaster.notify(res.error)
  };

  handlePresUpload = async ({ fileList }) => {
    const { prescriptions } = this.state;
    console.log('handlePresUpload', fileList, prescriptions);
    const res = !prescriptions
      ? await uploadPrescriptions({ fileList })
      : await updatePrescriptions({ fileList, origFileList: prescriptions });
    if (res && res.error)
      toaster.notify(
        () => (
          <>
            <Toaster text={res.error} error />
          </>
        ),
        {
          position: 'top-right',
          // duration: null,
        },
      );
    if (res && res.prescriptions) {
      this.setState({
        prescriptions: res.prescriptions.map(i => ({
          thumbnail: i.thumbnail,
          url: i.url,
          uid: i.id,
          id: i.id,
        })),
      });
      toaster.notify(
        () => (
          <>
            <Toaster text={`Prescription(s) ${prescriptions ? 'updated' : 'uploaded'}`} success />
          </>
        ),
        {
          position: 'top-right',
          // duration: null,
        },
      );
    }
  };

  render() {
    const {
      addressData,
      isAddrModalOpen,
      currentEditItem,
      loadingAddrEdit,
      message,
      loadingdelete,
      prescriptions,
    } = this.state;
    console.log('message', message);
    let currentEditAddr = {};
    if (currentEditItem) {
      currentEditAddr = {
        houseNo: currentEditItem.houseNo,
        street: currentEditItem.street,
        landmark: currentEditItem.landmark,
        city: currentEditItem.city,
        state: currentEditItem.state,
      };
    }
    const addresses = addressData.length > 0 ? addressData.map(item => {
      const address = {
        houseNo: item.houseNo,
        street: item.street,
        landmark: item.landmark,
        city: item.city,
        state: item.state,
      };
      return (
        <UserAddress
          id={item.id}
          addressType={item.address_type}
          name={item.fullName}
          phoneNo={item.mobileNo}
          address={address}
          pincode={item.pincode}
          data-id={item.id}
          onEditAddress={this.handleEditAddressClick}
          key={item.id}
        />
      );
    }) : <div className="field-notify">No saved addresses..</div>;

    return (
      <Authorize>
        <PageSection className="bg-grey-lite">
          <Modal
            noFooter
            title="Edit Address"
            show={isAddrModalOpen}
            onClose={this.handleCloseAddrModal}
          >
            {isAddrModalOpen && (
              <>
                <AddressForm
                  id={currentEditItem.id}
                  addressType={currentEditItem.address_type}
                  phoneNo={currentEditItem.mobileNo}
                  name={currentEditItem.fullName}
                  address={currentEditAddr}
                  pincode={currentEditItem.pincode}
                  loadingSubmit={loadingAddrEdit}
                  loadingDelete={loadingdelete}
                  onCancel={this.handleCloseAddrModal}
                  onSubmit={this.handleSaveAddressForm}
                  onDelete={this.handleDeleteAddress}
                />

                <StyledNotifyMsg>
                  <NotifyMsg message={message} />
                </StyledNotifyMsg>
              </>
            )}
          </Modal>
          <div className="inner-container">
            <div className="row">
              <div className="col-lg-6">
                <Info style={marginBottomStyle}>
                  <EditProfileForm data="s" onSubmit={this.handleSubmitProfile} />
                </Info>
                <Info style={marginBottomStyle}>
                  <ChangePassword />
                </Info>
                <Info>
                  <InfoHead title="My prescriptions" />
                  <UploadPrescription
                    files={prescriptions}
                    handleUpload={this.handlePresUpload}
                    onRemove={this.handleDeletePres}
                  />
                </Info>
              </div>

              <div className="col-lg-6">
                <Info>
                  <div className="all-address-area">
                    <InfoHead title="Manage Addresses">
                      <Button
                        size="small"
                        icon="fa-edit"
                        type="button"
                        onClick={this.handleEditAddressClick}
                      // className="extra-small-btn"
                      >
                        <span style={marginRightStyle}>
                          <i className="fas fa-map-marker-alt" />
                        </span>
                        Add New Address
                      </Button>
                    </InfoHead>
                    <div className="content">{addresses}</div>
                  </div>
                </Info>
              </div>
            </div>
          </div>
        </PageSection>
      </Authorize>
    );
  }
}

export default UserProfile;
