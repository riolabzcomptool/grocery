import React from 'react'

const UserAddress = ({ addressType, name, phoneNo, address, id, pincode, onEditAddress }) => {
  console.log(id);
  const { houseNo,
    street,
    landmark,
    city,
    state, } = address;

  
  return (
    <div className="address-wrapper">
      <div className="address-location">{addressType}</div>
      <button type="button" className="address-edit-icon" data-id={id} onClick={onEditAddress}>
        <img
          src="/resources/images/kickill-edit-icons-grey.svg"
          alt=""
          className="lazy edit-icon"
        />
      </button>
      <h4 className="address-name">{`${name} ${phoneNo}`}</h4>
      <p>{`${houseNo || ''} ${street ? `, ${street}` : ''} ${landmark ? `, ${landmark}` : ''}`}</p>
      <p>{`${pincode}`}</p>
      <p>{`${city}`}</p>
      <p>{`${state}`}</p>
    </div>
  )
}

export default UserAddress
