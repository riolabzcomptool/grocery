import React, { useState } from 'react';
import { withFormik } from 'formik';
import { connect } from 'react-redux';
import { UserProfileSchema } from '_utils/Schemas';
import Input, { RadioButtona } from 'components/Input';
import FormItem from 'components/FormItem';
import Row from 'components/Row';
// import editIcon from 'assets/images/kickill-edit-icons.svg';
import { Head as InfoHead } from 'components/Info';
import Button from 'components/Button';
import { userActions } from 'redux/actions';
import { updateProfile } from 'services/user';
import NotifyMsg from 'components/NotifyMsg';
import PhoneVerify from 'components/PhoneVerify';

const MyForm = props => {
  const [openCount, setOpenCount] = useState(0);
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
    status,
  } = props;
  const [isEdit, setEdit] = useState(false);
  console.log(props);
  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')

  const openVerify = () => setOpenCount(prev => prev + 1);

  console.log(errors);
  return (
    <>
      <PhoneVerify showOnMount={false} delay={0} openCount={openCount} />
      <InfoHead title="Personal Information">
        <Button
          size="small"
          icon="fa-edit"
          type="button"
          onClick={() => {
            setEdit(prevEdit => !prevEdit);
            handleReset();
          }}
          // className="extra-small-btn"
        >
          {/* <img className="lazy edit-icon" src={editIcon} alt="" /> */}
          Edit Information
        </Button>
      </InfoHead>
      {!isEdit && (
        <div className="user-details-wrapper">
          <dl className="row">
            <dt className="col-sm-3">Name</dt>
            <dd className="col-sm-9 text-capitalize">
              {values.firstName} {values.lastName || ''}
            </dd>

            <dt className="col-sm-3">Gender</dt>
            <dd className="col-sm-9 text-capitalize">{values.gender || '-'}</dd>

            <dt className="col-sm-3">Email address</dt>
            <dd className="col-sm-9">{values.email}</dd>

            <dt className="col-sm-3">Phone No</dt>
            <dd className="col-sm-9">{values.phoneNo}</dd>
          </dl>
        </div>
      )}
      {isEdit && (
        <>
          <form onSubmit={handleSubmit}>
            <Row>
              <FormItem className="col-xl-6 col-lg-6" label="First Name">
                <Input
                  placeholder="First Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.firstName}
                  name="firstName"
                  errors={errors.firstName && touched.firstName ? errors.firstName : ''}
                  disabled={!isEdit}
                />
              </FormItem>
              <FormItem className="col-xl-6 col-lg-6" label="Last Name">
                <Input
                  placeholder="Last Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="lastName"
                  value={values.lastName}
                  errors={errors.lastName && touched.lastName ? errors.lastName : ''}
                  // disabled={!isEdit}
                />
              </FormItem>
              <FormItem className="col-xl-12 col-lg-12">
                <div className="gender">
                  <span>Your Gender</span>
                  <RadioButtona
                    // disabled={!isEdit}
                    label="Male"
                    type="radio"
                    name="gender"
                    value="male"
                    defaultChecked={values.gender === 'male'}
                    onChange={handleChange}
                  />
                  <RadioButtona
                    // disabled={!isEdit}
                    label="Female"
                    type="radio"
                    name="gender"
                    value="female"
                    defaultChecked={values.gender === 'female'}
                    onChange={handleChange}
                  />
                </div>
              </FormItem>
              <FormItem className="col-xl-6 col-lg-6" label="Email address">
                <Input
                  placeholder="Email Address"
                  label="Email Address"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="email"
                  value={values.email}
                  errors={errors.email && touched.email ? errors.email : ''}
                  // disabled={!isEdit}
                  disabled
                />
              </FormItem>
              <FormItem className="col-xl-6 col-lg-6" label="Mobile No">
                <Input
                  placeholder="Mobile No"
                  name="phoneNo"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phoneNo || '-'}
                  // disabled={!isEdit}
                  disabled
                  errors={errors.phoneNo && touched.phoneNo ? errors.phoneNo : ''}
                />
              </FormItem>
              <FormItem className="col-xl-6 col-lg-6">
                <button
                  type="button"
                  onClick={openVerify}
                  className="d-block font-weight-normal link"
                >
                  {`${!values.phoneNo || values.phoneNo === "" ? 'Change' : 'Add'} phone number`}
                </button>
              </FormItem>
            </Row>
          </form>
          <Button type="button" onClick={handleSubmit} loading={isSubmitting} className="save-btn">
            Save
          </Button>
          <NotifyMsg message={status} />
        </>
      )}
    </>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: props => {
    const { user } = props;
    const { firstName, lastName, phoneNo, gender, email } = user;
    return {
      firstName,
      lastName,
      email,
      gender,
      phoneNo,
    };
  },

  enableReinitialize: true,
  validationSchema: UserProfileSchema,

  handleSubmit: async (values, { props, setSubmitting, setStatus }) => {
    setSubmitting(true);
    console.log(props);
    const { dispatch } = props;
    const { firstName, lastName, gender } = values;
    // props.onSubmit(values, () => setSubmitting(false));
    const updated = await updateProfile({ firstName, lastName, gender });
    if (updated.user) {
      dispatch({
        type: userActions.SET_STATE,
        payload: {
          firstName: updated.user.firstName,
          lastName: updated.user.lastName,
          gender: updated.user.gender,
        },
      });
      setStatus('Updated!');
    }
    if (updated.error) setStatus(updated.error.message);
    // setTimeout(() => {
    //   // alert(JSON.stringify(values, null, 2));
    //   setSubmitting(true)
    // }, 1000)
  },

  displayName: 'BasicForm',
})(MyForm);

export default connect(({ user }) => ({ user }))(MyEnhancedForm);
