import React, { useReducer, useEffect } from 'react';

import { Head as InfoHead } from 'components/Info';
import Button from 'components/Button';
import Modal from 'components/Modal';
import styled from 'styled-components';
import NotifyMsg from 'components/NotifyMsg';
import { getAddresses, deleteAddress, editAddress, addAddress } from 'services/address';
import AddressForm from '../AddressForm';
import UserAddress from '../components/UserAddress';


const marginRightStyle = { marginRight: '.5rem' };

const StyledNotifyMsg = styled.div`
  text-align: center;
  margin-top: -2rem;
`;

const styles = {
  modal:{
    padding: '0 0 1.2em 0'
  }
}

const initialState = {
  isAddrModalOpen: false,
  currentEditItem: {},
  loadingAddrEdit: false,
  loadingdelete: false,
  message: null,
  addressData: [],
};

function reducer(state, action) {
  switch (action.type) {
    case 'setMessage':
      return { ...state, message: action.payload };
    case 'closeModal':
      return { ...state, isAddrModalOpen: false, message: '' };
    case 'openModal':
      return { ...state, isAddrModalOpen: true };
    case 'setAddressData':
      return { ...state, addressData: action.payload };
    case 'setCurrentEditItem':
      return { ...state, currentEditItem: action.payload };
    case 'setLoadingdelete':
      return { ...state, loadingdelete: action.payload };
    case 'setLoadingAddrEdit':
      return { ...state, loadingAddrEdit: action.payload };
    case 'removeAddressItem':
      return {
        ...state,
        addressData: state.addressData.filter(i => i.id !== action.payload),
        message: 'Deleted!',
      };
    case 'replaceAddressItem': {
      const index = state.addressData.findIndex(i => i.id === action.payload.id);
      let msgObj = {};
      if (index > -1) {
        state.addressData[index] = { ...action.payload.data };
        msgObj = {
          message: 'Edited!',
        };
      }
      return { ...state, ...msgObj };
    }
    case 'addAddressItem': {
      return {
        ...state,
        addressData: [...state.addressData, action.payload.data],
        message: 'Added!',
      };
    }
    default:
      return {...state};
  }
}

const AddressList = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { isAddrModalOpen, currentEditItem, loadingAddrEdit, loadingdelete, message, addressData } = state;
  console.log(state)
  let currentEditAddr = {};
  if (currentEditItem) {
    currentEditAddr = {
      houseNo: currentEditItem.houseNo,
      street: currentEditItem.street,
      landmark: currentEditItem.landmark,
      city: currentEditItem.city,
      state: currentEditItem.state,
    };
  }

  useEffect(() => {
    fetchAddress();
  }, []);

  const fetchAddress = async () => {
    const addrResAr = await getAddresses();
    if (addrResAr && addrResAr.length > 0)
      dispatch({
        type: 'setAddressData',
        payload: addrResAr,
      });
  };

  const handleEditAddressClick = event => {
    const { dataset } = event.currentTarget;
    console.log(dataset, dataset.id);
    console.log('clicked to edit address', dataset.id);
    let editItem = {};
    if (dataset.id) {
      // edit current
      editItem = addressData.find(item => item.id === Number(dataset.id));
    }
    console.log(currentEditItem);
    dispatch({
      type: 'openModal',
    });
    dispatch({
      type: 'setCurrentEditItem',
      payload: editItem,
    });
  };

  const handleDeleteAddress = async id => {
    dispatch({
      type: 'setLoadingdelete',
      payload: true,
    });
    const isDeleted = await deleteAddress(id);
    dispatch({
      type: 'setLoadingdelete',
      payload: false,
    });
    if (isDeleted) {
      dispatch({
        type: 'removeAddressItem',
        payload: id,
      });
      setTimeout(
        () =>
          dispatch({
            type: 'closeModal',
          }),
        2000,
      );
    } else
      dispatch({
        type: 'setMessage',
        payload: 'Error deleting. Please try again later',
      });
  };

  const handleCloseAddrModal = () => {
    console.log('closing modal');
    dispatch({
      type: 'closeModal',
    });
  };

  const handleSaveAddressForm = async obj => {
    console.log('saving address form', obj);
    // const { addressData } = this.state;
    dispatch({
      type: 'setLoadingAddrEdit',
      payload: true,
    });
    const { address: editedAddr } = obj.id
      ? await editAddress(obj)
      : await addAddress(obj);
    if (editedAddr) {
      if (obj.id)
        dispatch({
          type: 'replaceAddressItem',
          payload: {
            id: obj.id,
            data: editedAddr,
          },
        });
      else
        dispatch({
          type: 'addAddressItem',
          payload: {
            data: editedAddr,
          },
        });
    }
    dispatch({
      type: 'setLoadingAddrEdit',
      payload: false,
    });
  };

  const addresses = addressData.length > 0 ? addressData.map(item => {
    const address = {
      houseNo: item.houseNo,
      street: item.street,
      landmark: item.landmark,
      city: item.city,
      state: item.state,
    };
    return (
      <UserAddress
        id={item.id}
        addressType={item.address_type}
        name={item.fullName}
        phoneNo={item.mobileNo}
        address={address}
        pincode={item.pincode}
        data-id={item.id}
        onEditAddress={handleEditAddressClick}
        key={item.id}
      />
    );
  }) : <div className="field-notify">No saved addresses..</div>;

  return (
    <>
      <Modal noFooter title="Edit Address" show={isAddrModalOpen} onClose={handleCloseAddrModal} fullHeight={false} style={styles.modal}>
        {isAddrModalOpen && (
          <>
            <AddressForm
              id={currentEditItem.id}
              addressType={currentEditItem.address_type}
              phoneNo={currentEditItem.mobileNo}
              name={currentEditItem.fullName}
              address={currentEditAddr}
              pincode={currentEditItem.pincode}
              loadingSubmit={loadingAddrEdit}
              loadingDelete={loadingdelete}
              onCancel={handleCloseAddrModal}
              onSubmit={handleSaveAddressForm}
              onDelete={handleDeleteAddress}
            />

            <StyledNotifyMsg>
              <NotifyMsg message={message} />
            </StyledNotifyMsg>
          </>
        )}
      </Modal>
      <div className="all-address-area">
        <InfoHead title="Manage Addresses">
          <Button
            size="small"
            icon="fa-edit"
            type="button"
            onClick={handleEditAddressClick}
            // className="extra-small-btn"
          >
            <span style={marginRightStyle}>
              <i className="fas fa-map-marker-alt" />
            </span>
            Add New Address
          </Button>
        </InfoHead>
        <div className="content">{addresses}</div>
      </div>
    </>
  );
};

export default AddressList;
