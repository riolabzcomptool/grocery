import React from 'react';
import Upload from 'components/Upload';

const UploadPrescription = props => {
  const { handleUpload, files, isRemove, onSelect, onRemove, errors, action } = props;
  const handleRemove = id => {
    if (onRemove) onRemove(id);
  };
  return (
    <Upload
      fileList={files}
      multiple
      handleUpload={handleUpload}
      isRemove={isRemove}
      onSelect={onSelect}
      onRemove={handleRemove}
      errors={errors}
      action={action}
      
    />
  );
};

UploadPrescription.defaultProps = {
  isRemove: true,
};

export default UploadPrescription;
