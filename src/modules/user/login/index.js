import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Hero from 'components/Hero';
import Banner from 'components/Banner';
import PageSection from 'components/PageSection';

import SocialSignup from 'components/SocialSignup';
import LoginForm from './Form';

const mapStateToProps = ({ user }) => ({ user });

class Login extends Component {
  render() {
    const {
      user, // lastLocation
    } = this.props;

    const { location } = this.props;
    const { state } = location;
    let from = '/home';
    if (state && state.from) from = state.from;

    if (user.isLogged) {
      // history.goBack();
      // return <Redirect to={lastLocation || '/home'} />;
      return <Redirect to={from} />;
    }
    return (
      <>
        <Helmet title="Login" />
        <PageSection className="bg-grey-lite">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="login-area-wrapper">
                  <div className="login-area">
                    <div className="login-head-wrapper">
                      <h2 className="login-head">
                        <span>Login to</span> Zapkart
                      </h2>
                      <h5 className="login-para">
                        Lorem ipsum dolor sit a ut labore et dolore magna aliqua. Quis ipsum
                        suspendisse ultrices gravida.{' '}
                      </h5>
                    </div>
                    <LoginForm />
                    <div className="social-signup">
                      <h3 className="signup-with">Or Login with</h3>
                      <SocialSignup />
                    </div>

                    <div className="newuser-register all-center">
                      <Link to="/user/register" className="medium-btn bordered-btn">
                        New User?
                      </Link>
                      <Link to="/user/register" className="medium-btn bold register-btn">
                        Register now
                      </Link>
                    </div>
                  </div>
                  <Hero image="/resources/images/kickill-login-bg.jpg">
                    <Banner
                      title="Zapkart"
                      subtitle="The Online Pharmacy"
                      slogan="WE Ensure the Lowest and Best Price for each MEDICINES"
                    />
                  </Hero>
                </div>
              </div>
            </div>
          </div>
        </PageSection>
      </>
    );
  }
}

export default connect(mapStateToProps)(Login);
