/* eslint-disable no-underscore-dangle */
import React from 'react';

import Authorize from 'components/Authorize';
import PageSection from 'components/PageSection';
import Placeholder from 'components/Placeholder';

import OrderList from '../components/OrderList';

const OrdersIndex = props => {
  console.log(props);
  const { location } = props;
  const { pathname } = location;

  return (
    <Authorize
      fallback={
        <Placeholder
          title="Please login!"
          subtitle="Login to view your orders!"
          buttonText="Login"
          to="/user/login"
          from={pathname}
        />
      }
    >
      <PageSection>
        <OrderList fullSpan />
      </PageSection>
      {/* <section className="inner-page-wrapper">
        <div className="container">

          <div className="row">
            <div className="col-lg-6">
              <OrderItem />
            </div>
            <div className="col-lg-6">
              <OrderItem />
            </div>
            <div className="col-lg-6">
              <OrderItem />
            </div>
            <div className="col-lg-6">
              <OrderItem />
            </div>
            <div className="col-lg-6">
              <OrderItem />
            </div>
          </div>
        </div>
      </section> */}
    </Authorize>
  );
};

export default OrdersIndex;
