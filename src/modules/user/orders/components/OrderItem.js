import React, { useState, useEffect } from 'react';
// import productImg from 'assets/images/products/kickill-product-3.png';
import Modal from 'components/Modal';
import AddReview from 'modules/product/components/AddReview';
import { createReview, editReview } from 'services/review';
import NotifyMsg from 'components/NotifyMsg';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Img from 'components/Img';

const styles = {
  rightZero: {
    right: 0,
  },
  textLeft: {
    textAlign: 'left',
  },
};

const OrderItem = ({ item }) => {
  console.log('orderitem', item.shippingStatus);
  const [isModal, setModal] = useState(false);
  const [message, setMessage] = useState('');
  const [reviewState, setReviewState] = useState(item.review);

  useEffect(() => {
    setReviewState(item.review);
  }, [item.review]);

  const {
    deliveredDate,
    // createdAt,
    shippedCreatedDate,
    dispatchDate,
    cancelledDate,
    thumbImg,
    img,
    // orderNo,
    name,
    brand,
    shippingStatus,
    // description,
    // paymentType,
    // createdAt,
    // subtotal,
    review,
  } = item;

  const handleCloseModal = () => {
    setModal(false);
    setMessage(null);
  };
  const handleOpenModal = () => setModal(true);

  const handleSubmitReview = async values => {
    console.log('will submit review', values);
    const { text, rating, title } = values;
    const { orderId, productId } = item;

    const res = review
      ? await editReview(
          {
            text,
            rating,
            title,
          },
          review.id,
        )
      : await createReview({
          orderItemId: item.id,
          orderId,
          productId,
          text,
          rating,
          title,
        });
    if (res && res.data) {
      if (item.review) {
        setMessage('Your review has been updated');
      } else setMessage('Your review has been posted and is pending for approval.');
      setReviewState(res.data);
    }
    if (res && res.error) setMessage(res.error);
    return true;
  };

  const currentStep = shippingStatus === 'pending' || !shippingStatus ? 'ordered' : shippingStatus;
  // const currentStep = 'cancelled';

  const imgSrc = thumbImg ? `/${thumbImg}` : `/${img}`;

  let reviewFormProps = {};
  if (reviewState) {
    const { text, rating, title } = reviewState;
    reviewFormProps = {
      text,
      rating,
      title,
    };
  }

  const getTextDate = () => {
    // dispatchDate deliveredDate cancelledDate shippedCreatedDate
    switch (currentStep) {
      // case 'ordered':
      //   return {
      //     text: createdAt ? 'Ordered on ' : '',
      //     date: createdAt ? new Date(createdAt).toDateString() : '',
      //   };
      case 'shipped':
        return {
          text: shippedCreatedDate ? 'Shipped on ' : '',
          date: shippedCreatedDate ? new Date(shippedCreatedDate).toDateString() : '',
        };
      case 'dispatched':
        return {
          text: dispatchDate ? 'Dispatched on ' : '',
          date: dispatchDate ? new Date(dispatchDate).toDateString() : '',
        };
      case 'delivered':
        return {
          text: deliveredDate ? 'Delivered on ' : '',
          date: deliveredDate ? new Date(deliveredDate).toDateString() : '',
        };
      case 'cancelled':
        return {
          text: cancelledDate ? 'Delivered on ' : '',
          date: cancelledDate ? new Date(cancelledDate).toDateString() : '',
        };
      default:
        return { text: '', date: '' };
    }
  };

  const textDate = getTextDate();

  return (
    <>
      <Modal show={isModal} onClose={handleCloseModal}>
        <Modal.Body>
          <AddReview noWrapper onSubmit={handleSubmitReview} {...reviewFormProps} />
          <NotifyMsg message={message} />
        </Modal.Body>
      </Modal>
      <div className="row order-item-address">
        <div className="col-xl-3 col-lg-12 col-md-3">
          <div className="cart-item row">
            <div className="cart-item-img col">
              <Img src={imgSrc} alt="" className="product-image" />
            </div>

            <div className="cart-item-details cart-item-details-sk col">
              <h5 className="product-name">{name}</h5>
              <h6 className="product-subheading">{brand}</h6>
              {/* <h6 className="weight-500">bottle of 60.0 tablets</h6> */}
            </div>
          </div>
        </div>
        <div className="col-xl-6 col-lg-12 col-md-6 mb-4">
          <div className="horizontal timeline">
            <div className="steps">
              <div className={classNames('step', { current: currentStep === 'ordered' })}>
                <span className="capitalize">Ordered</span>
              </div>
              {currentStep === 'cancelled' && (
                <div className="step current cancelled">
                  <span className="capitalize">Cancelled</span>
                </div>
              )}
              {currentStep !== 'cancelled' && (
                <div className={classNames('step', { current: currentStep === 'dispatched' })}>
                  <span className="capitalize">Dispatched</span>
                </div>
              )}
              {currentStep !== 'cancelled' && (
                <div className={classNames('step', { current: currentStep === 'shipped' })}>
                  <span className="capitalize">Shipped</span>
                </div>
              )}
              {currentStep !== 'cancelled' && (
                <div className={classNames('step', { current: currentStep === 'delivered' })}>
                  <span className="capitalize">Delivered</span>
                </div>
              )}
            </div>

            <div
              style={
                currentStep === 'delivered' || currentStep === 'cancelled' ? styles.rightZero : null
              }
              className={classNames('line', {
                'w-100': currentStep === 'cancelled' || currentStep === 'delivered',
                'w-75': currentStep === 'shipped',
                'w-50': currentStep === 'dispatched',
                'w-25': currentStep === 'ordered',
              })}
            />
          </div>
        </div>
        <div className="col-xl-3 col-lg-12 col-md-3">
          <div className="order-address order-address-sk">
            <h5>
              <strong>{`${textDate.text} ${textDate.date}`}</strong>
            </h5>
            {/* {currentStep === 'delivered' && deliveredDate && (
              <h5>
                <strong>Delivered on Oct 21, 2020</strong>
                <strong>Delivered on {getFormattedDate(deliveredDate)}</strong>
              </h5>
            )} */}
            {/* also do cancelled, dispatched, shipped on */}

            <button type="button" style={styles.textLeft} onClick={handleOpenModal}>
              <h4>RATE & REVIEW PRODUCT </h4>
            </button>
            <h4>NEED HELP? </h4>
          </div>
        </div>
      </div>
    </>
  );
};

OrderItem.propTypes = {
  item: PropTypes.shape({
    orderId: PropTypes.any.isRequired,
    productId: PropTypes.string.isRequired,
    orderNo: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    thumbImg: PropTypes.string,
    name: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    description: PropTypes.string,
    paymentType: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    subtotal: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }).isRequired,
};

// OrderItem.defaultProps = {
//   item: {
//     orderNo: "OD115088359520775000",
//     name: "HealthVit Mulvit Multivitamins and Minerals Tablet",
//     brand: "Morepen Laboratories Ltd",
//     description: "bottle of 60.0 tablets",
//     paymentType: "Cash",
//     subtotal: "234.95"
//   }
// }

export default OrderItem;
