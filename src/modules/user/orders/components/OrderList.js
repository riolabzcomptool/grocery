/* eslint-disable no-underscore-dangle */
import React from 'react';
import { useUserOrders, useUserReviews } from 'hooks/api';
import classNames from 'classnames';
import Loader from 'components/Loader';
import find from 'lodash/find';
import Placeholder from 'components/Placeholder';
import Img from 'components/Img';
import OrderItem from './OrderItem';

const styles = {
  orderId: {
    textAlign: 'left',
  },
};

const OrderList = ({ fullSpan, outerClass }) => {
  const { data: response, isLoading } = useUserOrders();
  const { data: reviewsRes } = useUserReviews();
  console.log('orderlist', 'reviews', reviewsRes && reviewsRes.data.reviews);
  console.log('orderlist', 'orders', response);

  const renderOrderProductItem = (orderItem, mainOrder) => {
    const item = {
      review:
        reviewsRes && reviewsRes.data && reviewsRes.data.reviews
          ? find(reviewsRes.data.reviews, i => i.productId === orderItem.product._id)
          : null,
      id: orderItem.id,
      orderId: mainOrder.id,
      productId: orderItem.product._id,
      name: orderItem.product && orderItem.product.name ? orderItem.product.name : '',
      brand:
        orderItem.product && orderItem.product.brand && orderItem.product.brand.name
          ? orderItem.product.brand.name
          : '',
      paymentType: mainOrder.paymentType,
      orderNo: mainOrder.orderNo,
      subtotal: orderItem.subtotal,
      thumbImg:
        orderItem.product && orderItem.product.images.length > 0
          ? orderItem.product.images[0].thumbnail
          : '',
      img:
        orderItem.product && orderItem.product.images.length > 0
          ? orderItem.product.images[0].url
          : '',
      createdAt: mainOrder.createdAt,
      shippingStatus:
        orderItem.shipping &&
        orderItem.shipping.shipment &&
        orderItem.shipping.shipment.shippingStatus
          ? orderItem.shipping.shipment.shippingStatus
          : null,
      deliveredDate:
        orderItem.shipping &&
        orderItem.shipping.shipment &&
        orderItem.shipping.shipment.deliveredDate
          ? orderItem.shipping.shipment.deliveredDate
          : null,
      shippedCreatedDate:
        orderItem.shipping &&
        orderItem.shipping.shipment &&
        orderItem.shipping.shipment.shippedCreatedDate
          ? orderItem.shipping.shipment.shippedCreatedDate
          : null,
      dispatchDate:
        orderItem.shipping &&
        orderItem.shipping.shipment &&
        orderItem.shipping.shipment.dispatchDate
          ? orderItem.shipping.shipment.dispatchDate
          : null,
      cancelledDate:
        orderItem.shipping &&
        orderItem.shipping.shipment &&
        orderItem.shipping.shipment.cancelledDate
          ? orderItem.shipping.shipment.cancelledDate
          : null,
    };

    return (
      <div
        className={classNames({ 'col-lg-6': !fullSpan, 'col-lg-12': fullSpan })}
        key={orderItem.id}
      >
        <OrderItem item={item} />
      </div>
    );
  };

  const renderShippingAddress = (addr = null) => {
    if (!addr) return null;
    return (
      <div className="col-xl-4 col-lg-12 col-md-4">
        <div className="address-wrapper sk-order">
          <h4 className="address-name">Delivery Address</h4>
          <h3>{addr.fullName || ''}</h3>

          <p>
            {`${addr.houseNo},` || ''} {`${addr.street},` || ''} {`${addr.landmark}` || ''}{' '}
            {`${addr.city},` || ''} {`${addr.state},` || ''}
            {` - ${addr.pincode}` || ''}
          </p>
          <h3>{addr.mobileNo || ''}</h3>
        </div>
      </div>
    );
  };

  return (
    <>
      {isLoading && <Loader />}
      {response && response.data && response.data.length === 0 && (
        <Placeholder
          title="No orders!"
          subtitle="You have not placed any orders yet"
          buttonText="Start shopping"
          to="/"
        />
      )}
      {response && response.data && response.data.length > 0 && (
        <section className={`inner-page-wrapper ${outerClass}`}>
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                {response.data.map(i => {
                  return (
                    <div className="orders-list" key={i.id}>
                      <h4 className="order-id" style={styles.orderId}>
                        Order No:&nbsp;<span>{i.orderNo}</span>
                        <span className="float-right">₹&nbsp;{i.orderSubtotal}</span>
                      </h4>
                      <p className="order-date-heading">
                        {`Ordered on ${new Date(i.createdAt).toDateString()}`}
                      </p>
                      <div className="row order-item-address">
                        {renderShippingAddress(i.shippingAddress)}
                        <div className="col-xl-4 col-lg-12 col-md-4">
                          <div className="address-wrapper sk-order">
                            <h4 className="address-name">Your Rewards</h4>
                            <div className="review-subject">
                              <span className="ratings-area review-green">
                                {' '}
                                <i className="fas fa-star" />
                              </span>
                              <div className="sk-order-rew">
                                <h5>16 SuperCoins</h5>
                                <p>Go to the SuperCoin Zone to know more</p>
                              </div>
                            </div>

                            <div className="review-subject">
                              <span className="ratings-area review-green">
                                {' '}
                                <i className="fas fa-star" />
                              </span>
                              <div className="sk-order-rew">
                                <h5>Free Delivery</h5>
                                <p>For Zapkart plus Members</p>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="col-xl-4 col-lg-12 col-md-4">
                          <div className="address-wrapper sk-order">
                            <h4 className="address-name">More Actions</h4>
                            <div className="review-subject downlode_in">
                              <span className="ratings-area review-green">
                                {' '}
                                <i className="fas fa-star" />
                              </span>

                              <h5>Download invoice</h5>

                              <button className="btn big-login-btn" type="button">
                                Download
                                <Img alt="" src="/resources/images/kickill-down-arrow.svg" />
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>

                      {i.order_items.map(m => renderOrderProductItem(m, i))}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
};

OrderList.defaultProps = {
  fullSpan: false,
  outerClass: '',
};

export default OrderList;
