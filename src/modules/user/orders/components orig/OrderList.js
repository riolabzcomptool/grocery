/* eslint-disable no-underscore-dangle */
import React from 'react';
import { useUserOrders, useUserReviews } from 'hooks/api';
import classNames from 'classnames';
import Loader from 'components/Loader';
import find from 'lodash/find';
import Placeholder from 'components/Placeholder';
import OrderItem from './OrderItem';

const OrderList = ({ fullSpan }) => {
  const { data: response, isLoading } = useUserOrders();
  const { data: reviewsRes } = useUserReviews();
  console.log('orderlist', 'reviews', reviewsRes&& reviewsRes.data.reviews);
  console.log('orderlist', 'orders', response);
  const renderOrderProductItem = (orderItem, mainOrder) => {
    const item = {
      review:
        reviewsRes && reviewsRes.data && reviewsRes.data.reviews? find(reviewsRes.data.reviews, i => i.productId === orderItem.product._id) : null,
      id: orderItem.id,
      orderId: mainOrder.id,
      productId: orderItem.product._id,
      name: orderItem.product && orderItem.product.name ? orderItem.product.name : '',
      brand:
        orderItem.product && orderItem.product.brand && orderItem.product.brand.name
          ? orderItem.product.brand.name
          : '',
      paymentType: mainOrder.paymentType,
      orderNo: mainOrder.orderNo,
      subtotal: orderItem.subtotal,
      thumbImg:
        orderItem.product && orderItem.product.images.length > 0
          ? orderItem.product.images[0].thumbnail
          : '',
      img:
        orderItem.product && orderItem.product.images.length > 0
          ? orderItem.product.images[0].url
          : '',
      createdAt: mainOrder.createdAt,
    };
    return (
      <div
        className={classNames({ 'col-lg-6': !fullSpan, 'col-lg-12': fullSpan })}
        key={orderItem.id}
      >
        <OrderItem item={item} />
      </div>
    );
  };
  return (
    <>
      {isLoading && <Loader />}
      {response && response.data && response.data.length === 0 && (
        <Placeholder
          title="No orders!"
          subtitle="You have not placed any orders yet"
          buttonText="Start shopping"
          to="/"
        />
      )}
      {response && response.data && response.data.length > 0 && (
        <div className="container">
          <div className="row">
            {response.data.map(i => {
              console.log(i);
              const { order_items: orderItems } = i;
              console.log(orderItems);
              return orderItems.map(m => renderOrderProductItem(m, i));
            })}
          </div>
        </div>
      )}
    </>
  );
};

OrderList.defaultProps = {
  fullSpan: false,
};

export default OrderList;
