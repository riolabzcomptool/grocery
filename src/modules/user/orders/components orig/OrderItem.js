import React, { useState, useEffect } from 'react';
// import productImg from 'assets/images/products/kickill-product-3.png';
import truckSvg from 'assets/images/kickill-truck.svg';
import cancelSvg from 'assets/images/kickill-cancel.svg';
import reloadSvg from 'assets/images/kickill-reload.svg';
import Button from 'components/Button';
import Modal from 'components/Modal';
import AddReview from 'modules/product/components/AddReview';
import { createReview, editReview } from 'services/review';
import NotifyMsg from 'components/NotifyMsg';
import PropTypes from 'prop-types';
import { getFormattedDate } from '_utils';

const OrderItem = ({ item }) => {
  console.log('orderitem', item);
  const [isModal, setModal] = useState(false);
  const [message, setMessage] = useState('');
  const [reviewState, setReviewState] = useState(item.review);

  useEffect(() => {
    setReviewState(item.review)
  }, [item.review])

  const {
    thumbImg,
    img,
    orderNo,
    name,
    brand,
    description,
    paymentType,
    createdAt,
    subtotal,
    review,
  } = item;

  const handleCloseModal = () => {
    setModal(false);
    setMessage(null);
  };
  const handleOpenModal = () => setModal(true);

  const handleSubmitReview = async values => {
    console.log('will submit review', values);
    const { text, rating, title } = values;
    const { orderId, productId } = item;

    const res = review
      ? await editReview(
        {
          text,
          rating,
          title,
        },
        review.id,
      )
      : await createReview({
        orderItemId: item.id,
        orderId,
        productId,
        text,
        rating,
        title,
      });
    if (res && res.data) {
      if (item.review) {
        setMessage('Your review has been updated');
      }
      else setMessage('Your review has been posted and is pending for approval.');
      setReviewState(res.data)
    }
    if (res && res.error) setMessage(res.error);
    return true;
  };



  console.log('THUMBIMG', `aaa${thumbImg}aaa`);
  const imgSrc = thumbImg ? `/${thumbImg}` : `/${img}`;

  let reviewFormProps = {};
  if (reviewState) {
    const { text, rating, title } = reviewState;
    reviewFormProps = {
      text,
      rating,
      title,
    };
  }

  return (
    <>
      <Modal show={isModal} onClose={handleCloseModal}>
        <Modal.Body>
          <AddReview onSubmit={handleSubmitReview} {...reviewFormProps} />
          <NotifyMsg message={message} />
        </Modal.Body>
      </Modal>
      <div className="orders-list">
        <div className="id-tracking-wrapper">
          <div className="order-id">{orderNo}</div>
          <div className="help-track">
            <div className="need-help-wrapper">
              <a href="/#" className="need-help">
                Need Help ?
              </a>
            </div>
            <div className="track-button">
              <img className="lazy truck-icon" src={truckSvg} alt="" />
            </div>
          </div>
        </div>
        <div className="row order-item-address">
          <div className="col-xl-6 col-lg-12 col-md-6">
            <div className="cart-item">
              <div className="cart-item-img">
                <img src={imgSrc} alt="" className="product-image lazy" />
              </div>

              <div className="cart-item-details">
                <h5 className="product-name">{name}</h5>
                <h6 className="product-subheading">{brand}</h6>
                <h6 className="weight-500">{description}</h6>
              </div>
            </div>
          </div>

          <div className="col-xl-6 col-lg-12 col-md-6">
            <div className="payment-type-wrapper">
              <h4>Payment Type</h4>
              <span className="payment-type">
                {paymentType === 'onlinePayment' ? 'online payment' : paymentType}
              </span>
            </div>
            <div className="order-address">
              <h5>
                <strong>Delivery expected by Fri, Apr 5th &apos;20</strong>
              </h5>
              <h5>Your Order has been placed.</h5>
              {createdAt && (
                <h5>
                  <strong>Ordered On {getFormattedDate(createdAt)}</strong>
                  {/* <strong>Ordered On Sun, Mar 31st &apos;20</strong> */}
                </h5>
              )}
            </div>
          </div>
        </div>

        <div className="order-buttons">
          <div className="row">
            <div className="col-xl-12">
              <a href="/#" className="cancel-btn">
                <img src={cancelSvg} alt="" className="lazy" /> Cancel this Item
              </a>
              <a href="/#" className="blue-btn ml-10 ease1s">
                Reorder <img src={reloadSvg} alt="" />
              </a>
              <Button
                href="/#"
                className="blue-btn ml-10 ease1s"
                onClick={handleOpenModal}
                icon="fa-pencil-alt"
              >
                {reviewState ? 'Edit review' : 'Write a review'}
                {/* <img src={reloadSvg} alt="" /> */}
              </Button>
            </div>
            <div className="col-xl-12">
              <div className="order-price">
                <span>Order Total : </span>{' '}
                <strong>
                  <i className="fas fa-rupee-sign" /> {subtotal}
                </strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

OrderItem.propTypes = {
  item: PropTypes.shape({
    orderId: PropTypes.any.isRequired,
    productId: PropTypes.string.isRequired,
    orderNo: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    thumbImg: PropTypes.string,
    name: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    description: PropTypes.string,
    paymentType: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    subtotal: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }).isRequired,
};

// OrderItem.defaultProps = {
//   item: {
//     orderNo: "OD115088359520775000",
//     name: "HealthVit Mulvit Multivitamins and Minerals Tablet",
//     brand: "Morepen Laboratories Ltd",
//     description: "bottle of 60.0 tablets",
//     paymentType: "Cash",
//     subtotal: "234.95"
//   }
// }

export default OrderItem;
