import React from 'react';
import aboutImg from 'assets/images/about/about.jpg';

const index = () => {
  return (
    <section className="about-page section-b-space">
      <div className="container">
        <div className="row">
          <div className="col-xl-7">
            <h4>Information page1</h4>
            <p>
              On the other hand,Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium Sed ut perspiciatis unde omnis iste natus error sit
              voluptatem accusantium doloremque laudantium we denounce with righteous indignation
              and dislike men who are so beguiled and demoralized.
            </p>
            <p>
              that they cannot foresee the pain and trouble that are bound to ensue; and equal blame
              belongs to those who fail in their duty through weakness of will, which is the same as
              saying through shrinking from toil and pain. These cases are perfectly simple and easy
              to distinguish. In a free hour, when
            </p>
            <p>
              our power of choice is untrammelled and when nothing prevents our being able to do
              what we like best, every pleasure is to be welcomed and every pain avoided. But in
              certain circumstances and owing to the claims of duty or the obligations of business
              it will frequently occur that pleasures have to be repudiated and annoyances
              acceptedso blinded by desire, that they cannot foresee the pain and trouble that are
              bound to ensue.
            </p>
          </div>
          <div className="col-xl-5 about-image">
            <img src={aboutImg} className="img-fluid" alt="" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default index;
