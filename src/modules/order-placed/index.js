import React from 'react';
import { connect } from 'react-redux';
import { cartActions } from 'redux/actions';
import Authorize from 'components/Authorize';
import { Redirect } from 'react-router';
import { useOrder } from 'hooks/api';
import Loader from 'components/Loader';
import { startCase } from 'lodash';
import Placeholder from 'components/Placeholder';
import { Helmet } from 'react-helmet';

const OrderPlaced = props => {
  console.log('olla', props);
  const { dispatch, location } = props;
  const { state } = location;
  const isRedirect = !(state && state.orderId);
  const { orderId } = state;
  dispatch({
    type: cartActions.LOAD_CART,
  });

  const { data: response, isLoading } = useOrder(orderId);

  if (isLoading) return <Loader />;

  return (
    <Authorize redirect="/">
      {!Redirect && !response?.data && (
        <Placeholder
          title="Order does not exist!"
          // subtitle="You have not placed any orders yet"
          buttonText="Start shopping"
          to="/"
        />
      )}
      {!isRedirect && response?.data && (
        <>
          <Helmet>
            <title>Order placed</title>
          </Helmet>
          <section className="section-b-space light-layout">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="success-text">
                    <i className="fa fa-check-circle" aria-hidden="true" />
                    <h2>thank you</h2>
                    <p>Payment is successfully processsed and your order is on the way</p>
                    <p>
                      Transaction ID: <strong>{response.data.orderNo}</strong>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section className="section-b-space">
            <div className="container">
              <div className="row">
                <div className="col-lg-6">
                  <div className="product-order">
                    <h3 className="title pt-0">your order details</h3>
                    {response.data.order_items.map(i => (
                      <div className="row product-order-detail">
                        <div className="col-3">
                          <img
                            src={i.product?.images?.[0]?.thumbnail}
                            alt=""
                            className="img-fluid "
                          />
                        </div>
                        <div className="col-3 order_detail">
                          <div>
                            <h4>product name</h4>
                            <h5>{i.product?.name}</h5>
                          </div>
                        </div>
                        <div className="col-3 order_detail">
                          <div>
                            <h4>quantity</h4>
                            <h5>{i.quantity}</h5>
                          </div>
                        </div>
                        <div className="col-3 order_detail">
                          <div>
                            <h4>price</h4>
                            <h5>${i.subtotal}</h5>
                          </div>
                        </div>
                      </div>
                    ))}

                    <div className="total-sec">
                      <ul>
                        <li>
                          subtotal{' '}
                          <span>
                            ${+response.data.orderSubtotal - +response.data.shippingAmount}
                          </span>
                        </li>
                        <li>
                          shipping <span>${response.data.shippingAmount}</span>
                        </li>
                        {/* <li>
                          tax(GST) <span>$10.00</span>
                        </li>  */}
                      </ul>
                    </div>
                    <div className="final-total">
                      <h3>
                        total <span>${response.data.orderSubtotal}</span>
                      </h3>
                    </div>
                  </div>
                </div>
                {/* fullName mobileNo houseNo street landmark city state pincode */}
                <div className="col-lg-6">
                  <div className="row order-success-sec">
                    <div className="col-sm-6">
                      <h4>summary</h4>
                      <ul className="order-detail">
                        <li>order ID: {response.data.orderNo}</li>
                        <li>Order Date: {response.data.createdAt.toDateString()}</li>
                        <li>Order Total: ${response.data.orderSubtotal}</li>
                      </ul>
                    </div>
                    <div className="col-sm-6">
                      <h4>shipping address</h4>
                      <ul className="order-detail">
                        <li>{response.data.shippingAddress?.fullName}</li>
                        <li>{`${response.data.shippingAddress?.houseNo} ${response.data.shippingAddress?.street} ${response.data.shippingAddress?.landmark}`}</li>
                        <li>{`${response.data.shippingAddress?.city}, ${response.data.shippingAddress?.state}, ${response.data.shippingAddress?.pincode}`}</li>
                        <li>Contact No. {response.data.shippingAddress?.mobileNo}</li>
                      </ul>
                    </div>
                    <div className="col-sm-12 payment-mode">
                      <h4>payment method</h4>
                      <p>
                        {startCase(response.data.paymentType)}
                        {/* Pay on Delivery (Cash/Card). Cash on delivery (COD) available. Card/Net
                        banking acceptance subject to device availability. */}
                      </p>
                    </div>
                    <div className="col-md-12">
                      <div className="delivery-sec">
                        <h3>expected date of delivery</h3>
                        <h2>october 22, 2020</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      )}
      {isRedirect && <Redirect to="/" />}
    </Authorize>
  );
};

export default connect()(OrderPlaced);
