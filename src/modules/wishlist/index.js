/* eslint-disable no-underscore-dangle */
import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import Authorize from 'components/Authorize';
// import PageSection from 'components/PageSection';
// import Button from 'components/Button';
import Placeholder from 'components/Placeholder';
// import { Link } from 'react-router-dom';
// import Product from 'components/Product';
import { wishlistActions, cartActions } from 'redux/actions';

import heart from 'assets/images/heart-paper.svg';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

// import styles from './style.module.scss';

// const NotLoggedWishlist = () => {
//   return (
//     <div className={styles.wishlistErrorContainer}>
//       <div className={styles.header}>
//         <h2 className="login-head">Please login</h2>
//       </div>
//       <div className={styles.subtitle}>
//         <h5 className="login-para">Login to view items in your wishlist</h5>
//       </div>
//       <div className={styles.loginBtn}>
//         <Link to="/user/login">
//           <Button>Login</Button>
//         </Link>
//       </div>
//     </div>
//   );
// };

const Wishlist = ({ wishlist, dispatch, loading }) => {
  console.log(wishlist);

  const handleRemove = useCallback(
    id => {
      return e => {
        e.preventDefault();
        console.log('need to remove', id);
        dispatch({
          type: wishlistActions.EDIT_WISHLIST,
          payload: {
            type: 'remove',
            id,
          },
        });
      };
    },
    [dispatch],
  );

  const handleAddToCart = useCallback(
    product => {
      return e => {
        e.preventDefault();
        // console.log('need to remove', id);
        dispatch({
          type: cartActions.ADD_TO_CART,
          payload: {
            productId: product._id,
            slug: product.slug,
            name: product.name,
            qty: 1,
            listPrice: product.pricing ? product.pricing.listPrice : '',
            salePrice: product.pricing ? product.pricing.salePrice : '',
            img: product.images ? product.images[0].url : '',
            thumbImg: product.images ? product.images[0].thumbnail : '',
            maxOrderQty: product.maxOrderQty,
            minOrderQty: product.minOrderQty,
            prescriptionNeeded: product.prescriptionNeeded,
            deleted: product.deleted,
            status: product.status,
            outOfStockStatus: product.stock ? product.stock.outOfStockStatus : '',
          },
        });
      };
    },
    [dispatch],
  );

  const renderWishlist = () => {
    if (loading) return null;
    if (wishlist.length > 0)
      return (
        <>
          <Helmet>
            <title>My Wishlist</title>
          </Helmet>
          <section className="wishlist-section section-b-space">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <table className="table cart-table table-responsive-xs">
                    <thead>
                      <tr className="table-head">
                        <th scope="col">image</th>
                        <th scope="col">product name</th>
                        <th scope="col">price</th>
                        <th scope="col">availability</th>
                        <th scope="col">action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {wishlist.map(i => (
                        <tr>
                          <td>
                            <Link to={`/product/${i.slug}`}>
                              <img src={`/${i.images?.[0]?.thumbnail}`} alt="" />
                            </Link>
                          </td>
                          <td>
                            <Link to={`/product/${i.slug}`}>{i.name}</Link>
                            <div className="mobile-cart-content row">
                              <div className="col-xs-3">
                                <p>{i.availability}</p>
                              </div>
                              <div className="col-xs-3">
                                <h2 className="td-color">${i.pricing?.salePrice}</h2>
                              </div>
                              <div className="col-xs-3">
                                <h2 className="td-color">
                                  <a href="#" className="icon mr-1" onClick={handleRemove(i)}>
                                    <i className="fa fa-close" />
                                  </a>

                                  <a href="#" className="cart" onClick={handleAddToCart(i)}>
                                    <i className="fa fa-shopping-cart" />
                                  </a>
                                </h2>
                              </div>
                            </div>
                          </td>
                          <td>
                            <h2>${i.pricing?.salePrice}</h2>
                          </td>
                          <td>
                            <p>{i.availability}</p>
                          </td>
                          <td>
                            <a href="#" className="icon mr-3" onClick={handleRemove(i._id)}>
                              <i className="fa fa-close" />{' '}
                            </a>

                            <a href="#" className="cart" onClick={handleAddToCart(i)}>
                              <i className="fa fa-shopping-cart" />
                            </a>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="row cart-buttons">
                <div className="col-6">
                  <Link to="/" className="btn btn-solid">
                    continue shopping
                  </Link>
                </div>
                <div className="col-6">
                  <Link to="/checkout" className="btn btn-solid">
                    check out
                  </Link>
                </div>
              </div>
            </div>
          </section>
        </>
      );
    return (
      <>
        <Helmet>
          <title>My Wishlist</title>
        </Helmet>
        <Placeholder
          title="Your wishlist is empty!"
          subtitle="You have not saved anything yet!"
          buttonText="Start shopping"
          to="/"
          img={heart}
        />
      </>
    );
  };

  return (
    <Authorize
      fallback={
        <Placeholder
          title="Please login!"
          subtitle="Login to view items in your wishlist"
          buttonText="Login"
          to="/user/login"
        />
      }
    >
      {renderWishlist()}
    </Authorize>
  );
};

export default connect(({ wishlist }) => ({
  wishlist: wishlist.products,
  loading: wishlist.loading,
}))(Wishlist);
