import React, { useMemo, useCallback, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import Authorize from 'components/Authorize';
import Loader from 'components/Loader';
import RadioGroup, { RadioOption as RadioGroupOption } from 'components/RadioGroup';
import { usePaymentMethods } from 'hooks/api';
import toaster from 'toasted-notes';
import ToastedNotes from 'components/ToastedNotes';

const Checkout = ({ cart }) => {
  const [payment, setPayment] = useState(null);

  const { data: paymentRes, refetch: refetchPayments } = usePaymentMethods(true);
  useEffect(() => {
    if (!cart.loading && cart.products?.length > 0) refetchPayments();
  }, [cart.loading, cart.products, refetchPayments]);

  const paymentOptions = useMemo(() => {
    if (paymentRes?.data?.length > 0)
      return paymentRes.data.map(i => ({
        id: i.id,
        label: i.label,
      }));
    return [];
  }, [paymentRes]);

  const onChangePayment = useCallback(value => {
    setPayment(value);
  }, []);

  const onPlaceOrder = useCallback(
    e => {
      e.preventDefault();
      if (!payment)
        toaster.notify(({ onClose }) => (
          <ToastedNotes error text="Payment required" onClose={onClose} />
        ));
    },
    [payment],
  );

  if (cart.loading) return <Loader />;
  if (cart.products?.length === 0) return <Redirect to="/" />;

  const products = cart.products.map(i => ({ ...i.product, quantity: i.quantity }));
  let total = 0;
  let discount = 0;
  let cartAmount = 0;
  let shippingCost = 0;
  let subtotal = 0;
  if (products && products.length > 0) {
    console.log('aaa products', products);
    total = products.reduce((sum, item) => sum + item.pricing.listPrice * item.quantity, 0);
    discount = products.reduce(
      (sum, item) => sum + (item.pricing.listPrice - item.pricing.salePrice) * item.quantity,
      0,
    );
    // shippingCost = 75;
    shippingCost = 0;
    cartAmount = total - discount;
    subtotal = cartAmount + shippingCost;
  }
  return (
    <Authorize>
      <section className="section-b-space">
        <div className="container">
          <div className="checkout-page">
            <div className="checkout-form">
              <form>
                <div className="row">
                  <div className="col-lg-6 col-sm-12 col-xs-12">
                    <div className="checkout-details">
                      <div className="order-box">
                        <div className="title-box">
                          <div>
                            Product <span>Total</span>
                          </div>
                        </div>
                        <ul className="qty">
                          {cart.products.map(i => (
                            <li key={i.productId}>
                              {i.name}&nbsp;×{i.quantity}
                              <span>${i.salePrice}</span>
                            </li>
                          ))}
                        </ul>
                        <ul className="sub-total">
                          <li>
                            Subtotal <span className="count">${cartAmount}</span>
                          </li>
                          <li>
                            Shipping
                            <div className="shipping">
                              <div className="shopping-option">
                                <input type="checkbox" name="free-shipping" id="free-shipping" />
                                <label htmlFor="free-shipping">Free Shipping</label>
                              </div>
                              <div className="shopping-option">
                                <input type="checkbox" name="local-pickup" id="local-pickup" />
                                <label htmlFor="local-pickup">Local Pickup</label>
                              </div>
                            </div>
                          </li>
                        </ul>
                        <ul className="total">
                          <li>
                            Total <span className="count">${subtotal}</span>
                          </li>
                        </ul>
                      </div>
                      <div className="payment-box">
                        <div className="upper-box">
                          <RadioGroup name="payment-group" onChange={onChangePayment}>
                            {paymentOptions.map(i => (
                              <RadioGroupOption key={i.id} id={i.id}>
                                {i.label}
                              </RadioGroupOption>
                            ))}
                          </RadioGroup>
                        </div>
                        <div className="text-right">
                          <a href="#" onClick={onPlaceOrder} className="btn-solid btn">
                            Place Order
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </Authorize>
  );
};

export default connect(({ user, cart }) => ({ user, cart }))(Checkout);
