import React, { useEffect } from 'react';
import { useGetInformation } from 'hooks/api';
import Loader from 'components/Loader';
import Placeholder from 'components/Placeholder';
import BreadCrumbs from 'components/BreadCrumbs';
import { Helmet } from 'react-helmet';

const InformationTwo = props => {
  const {
    match: {
      params: { slug },
    },
  } = props;

  const { refetch, data: res, isFetching } = useGetInformation(slug);

  useEffect(() => {
    if (slug) refetch();
  }, [refetch, slug]);

  if (isFetching) return <Loader />;
  if (res?.data) {
    const bread = [
      {
        url: '/home',
        text: 'Home',
      },
      {
        text: res.data.name,
      },
    ];
    return (
      <>
        <Helmet>
          <title>{res.data.seo?.metaTitle}</title>
          <meta name="description" content={res.data.seo?.metaDescription} />
          <meta name="keywords" content={res.data.seo?.metaKeywords} />
        </Helmet>
        <BreadCrumbs bread={bread} />
        <section className="about-page section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-xl-12">
                <h4>{res.data.name}</h4>
                <div dangerouslySetInnerHTML={{ __html: res.data.htmlContent }} />
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }

  return (
    <Placeholder
      title="PAGE NOT FOUND!"
      subtitle="The page you are looking for does not exist"
      buttonText="Go back to home"
      to="/"
    />
  );
};

export default InformationTwo;
