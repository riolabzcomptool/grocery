import React, { useMemo } from 'react';
import Slider from 'react-slick';

const ProductsSlider = ({ title, className, children }) => {
  const sliderSettings = useMemo(
    () => ({
      dots: true,
      infinite: true,
      slidesToShow: 5,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    }),
    [],
  );
  return (
    <section className={`section-b-space ratio_square ${className}`}>
      <div className="container">
        <div className="row">
          <div className="col-12 product-related">
            <h2 className="title pt-0">{title}</h2>
          </div>
        </div>
        {/* <div className="slide-6"> */}
        <Slider {...sliderSettings}>{children}</Slider>

        {/* </div> */}
      </div>
    </section>
  );
};

export default ProductsSlider;
