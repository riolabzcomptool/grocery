/* eslint-disable no-underscore-dangle */
import React from 'react';
import useFetching from 'hooks/useFetching';
import { CATALOG_API } from '_constants';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const VariantList = ({ productId, isVariant, currentProductId }) => {
  console.log('hjkhjk', productId, isVariant);
  const [{ response, loading }] = useFetching(
    `${CATALOG_API.getVariants}/${productId}?status=active`,
  );
  console.log(loading, response);

  if (!isVariant) {
    console.log('not variant');
    // return null;
  }
  // if (!response) return null;
  if (response) {
    if (response.data && response.data.length === 0) return null;
  }
  if (response && response.data) {
    console.log('767', response.data);
    const renderVariant = response.attributeslist.map((attrlist, index) =>
      attrlist.map(i => {
        console.log('7772', index, i.attributes[index]);
        return i._id !== currentProductId && i.attributes[index] ? (
          <li key={i._id} className="variant-list-item">
            <h5>{i.attributes[index].attributeGroup.name}</h5>
            <Link key={i.slug} to={`/product/${i.slug}`} className="item">
              {i.attributes.map(m => {
                console.log(m);
                return (
                  <div className="variant-item-content" key={i._id}>
                    <span>{i.name}&nbsp;</span>
                    <span>{m.value.value}&nbsp;</span>
                    <span><i className="fas fa-rupee-sign" />&nbsp;{i.pricing.salePrice}</span>
                  </div>
                );
              })}
            </Link>
          </li>
        ) : (
          ''
        );
      }),
    );

    // const renderVariant = response.data.map(i => {
    //   return (i._id!==currentProductId)?
    //   <span>
    //     <h5>Variant</h5>
    //   <Link key={i.slug} to={`/product/${i._id}`} className="item">
    //     {i.attributes.map(m => {
    //       console.log(m);
    //       return (
    //         <div className="variants-item-text" key={i._id}>
    //           {/* <div className="attributeGroup">{m.attributeGroup.name}</div>
    //           <div className="attrValue">{m.value.value}</div> */}
    //           <span>{m.value.value}</span>
    //         </div>
    //       );
    //     })}
    //   </Link> </span>:
    //   <span>
    //     <h5>Variant</h5>
    //   <Link key={i.slug} to={`/product/${i.parentId._id}`} className="item">
    //   {i.attributes.map(m => {
    //     console.log(m);
    //     return (
    //       <div className="variants-item-text" key={i.parentId._id}>
    //         <div className="attributeGroup">{m.attributeGroup.name}</div>
    //         <div className="attrValue">{m.value.value}</div>

    //       </div>
    //     );
    //   })}
    // </Link> </span>
    // });
    return (
      // <div className="variants-names-list">
      <div className="variant-list-wrapper">
        {/* <div className="flex flex-wrap justify-content-start variants-names-list-area"> */}
        <ul className="variant-list">{renderVariant}</ul>
      </div>
    );
  }

  return null;
};

VariantList.propTypes = {
  productId: PropTypes.string.isRequired,
  isVariant: PropTypes.bool,
};

VariantList.defaultProps = {
  isVariant: false,
};

export default VariantList;
