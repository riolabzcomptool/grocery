import React from 'react';
// import thumbIcon from 'assets/images/kickill-thumb-icon.png';
import Input, { TextArea } from 'components/Input';

import FormItem from 'components/FormItem';
import Button from 'components/Button';
import StarsRating from 'components/StarsRating';
import { withFormik } from 'formik';
import { reviewSchema } from '_utils/Schemas';
import classNames from 'classnames'

const AddReview = props => {
  console.log('AddReview', props);
  const {
    noWrapper,
    text,
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    // setValues,
    setFieldValue,
    isSubmitting,
    // handleReset
  } = props;

  const handleRatingChange = a => {
    console.log('new rating', a);
    // setValues({ ...values, rating: a });
    setFieldValue('rating', a, false);
  };

  return (
    <div className={classNames('w-100', { 'reviews-wrapper': !noWrapper })}>
      <h4 className="review-head">
        <span>{text ? 'Edit Your Review' : 'Add Your Review'}</span>
      </h4>

      <div className="review-form-wrapper">
        <h5>Rate this product</h5>
        <FormItem errors={errors.rating && touched.rating ? errors.rating : ''}>
          <StarsRating initialValue={values.rating} onChange={handleRatingChange} />
        </FormItem>
      </div>

      <div className="form-group">
        <FormItem
          label="Add a headline"
          className="mt-2"
          errors={errors.title && touched.title ? errors.title : ''}
        >
          <Input onChange={handleChange} onBlur={handleBlur} value={values.title} name="title" />
        </FormItem>
        <FormItem label="Write your review" errors={errors.text && touched.text ? errors.text : ''}>
          <TextArea
            name="text"
            id=""
            cols="30"
            rows="10"
            // className="form-control review-field"
            className="review-field mt-0"
            placeholder="What did you like or dislike about this product"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.text}
          />
        </FormItem>
      </div>

      <div className="button-main">
        <Button
          type="button"
          className="btn submit-btn post-btn"
          onClick={handleSubmit}
          loading={isSubmitting}
          icon="fa-thumbs-up"
          iconStyle="regular"
        >
          Post Your review
          {/* <img className="lazy submit-icon" src={thumbIcon} alt="" /> */}
        </Button>
      </div>
    </div>
  );
};

AddReview.defaultProps = {
  noWrapper: false,
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: props => {
    const { text = '', title = '', rating = 0 } = props;
    console.log(props);
    return {
      text,
      title,
      rating,
    };
  },

  validationSchema: reviewSchema,

  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);
    props.onSubmit(values);

    setSubmitting(true);
    const isRes = await props.onSubmit();
    if (isRes) setSubmitting(false);

    // setTimeout(() => {
    //   // alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false);
    // }, 1000);
  },

  displayName: 'ReviewForm',
})(AddReview);

export default MyEnhancedForm;
