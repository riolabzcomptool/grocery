import React, { useEffect } from 'react';
import { useProductReviews } from 'hooks/api';
import Review from './Review';

const ReviewsList = ({ productId }) => {
  const { refetch, data: res, isFetching, isLoading } = useProductReviews(productId);

  useEffect(() => {
    console.log('refetching reviews');
    refetch();
  }, [productId, refetch]);

  let reviewsContent = '';

  if (isLoading) reviewsContent = <div className="font-weight-bolder">Loading reviews...</div>;

  console.log(res);

  if (!isFetching && res && res.data && res.data.reviews) {
    const { reviews } = res.data;
    console.log(res.data.reviews);
    reviewsContent =
      reviews.length > 0 ? (
        reviews.map(item => {
          const reviewData = {
            rating: item.rating || 'No rating',
            reviewText: item.text || '', 
            title: item.title || '',
            dateTime: item.createdAt,
          };
          return <Review id={item.id} key={item.id} review={reviewData} />;
        })
      ) : (
        <div className="font-weight-bolder">
          No reviews have been posted for this product yet...
        </div>
      );
  }
  return <div>{reviewsContent}</div>;
};

export default ReviewsList;
