/* eslint-disable no-underscore-dangle */
import React from 'react';
// import useFetching from 'hooks/useFetching';
// import { CATALOG_API } from '_constants';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import { useVariants } from 'hooks/api';

const VariantList = React.memo(({ productId, isVariant, currentProductId }) => {
  // console.log('hjkhjk', productId, isVariant);
  const { data: response } = useVariants(productId);
  console.log('111', response);

  if (!isVariant) {
    console.log('not variant');
    // return null;
  }
  // if (!response) return null;
  if (response) {
    if (response.data && response.data.length === 0) return null;
  }
  if (response && response.data) {
    // console.log('767', response.attributeslist);

    const renderVariant = response.attributeslist.map((attrlist, index) => {
      const aa = find(attrlist, i => i._id !== currentProductId && i.attributes[index]);
      if (isEmpty(aa)) return null;
      console.log('ana', aa);
      return (
        <div className="variant-lists-group" key={attrlist._id}>
          <h5 className="variant-list-title">
            {/* {attrlist[0].attributes[index].attributeGroup.name} */}
            {aa.attributes[index].attributeGroup.name}
          </h5>
          <ul className="variant-list">
            {attrlist.map(i => {
              // console.log('7772', i._id !== currentProductId && i.attributes[index]);
              // console.log(
              //   '7772',
              //   index,
              //   i.attributes[index] ? i.attributes[index].attributeGroup.name : null,
              // );
              return (
                <>
                  {i._id !== currentProductId && i.attributes[index] ? (
                    <li key={i._id} className="variant-list-item">
                      {/* <h5 className="variant-list-item-title">
                  {i.attributes[index].attributeGroup.name}
                </h5> */}
                      <Link key={i.slug} to={`/product/${i.slug}`}>
                        {i.attributes.map(m => {
                          // console.log('888', i.attributes);
                          if (m.attributeGroup._id === i.attributes[index].attributeGroup._id)
                            return (
                              <div className="variant-item-content" key={i._id}>
                                {/* <div>{i.name}&nbsp;</div> */}
                                {/* {i.images && i.images.length > 0 &&<div><img alt={i.name} src={`/${i.images[0].thumbnail}`} /></div>} */}
                                <div className="variant-item-content-value">
                                  {m.value.value}&nbsp;
                                </div>
                                <div className="variant-item-content-pricing">
                                  <i className="fas fa-rupee-sign" />
                                  &nbsp;{i.pricing.salePrice}
                                </div>
                              </div>
                            );
                          return null;
                        })}
                      </Link>
                    </li>
                  ) : (
                    ''
                  )}
                </>
              );
            })}
          </ul>
        </div>
      );
    });

    // const renderVariant = response.data.map(i => {
    //   return (i._id!==currentProductId)?
    //   <span>
    //     <h5>Variant</h5>
    //   <Link key={i.slug} to={`/product/${i._id}`} className="item">
    //     {i.attributes.map(m => {
    //       console.log(m);
    //       return (
    //         <div className="variants-item-text" key={i._id}>
    //           {/* <div className="attributeGroup">{m.attributeGroup.name}</div>
    //           <div className="attrValue">{m.value.value}</div> */}
    //           <span>{m.value.value}</span>
    //         </div>
    //       );
    //     })}
    //   </Link> </span>:
    //   <span>
    //     <h5>Variant</h5>
    //   <Link key={i.slug} to={`/product/${i.parentId._id}`} className="item">
    //   {i.attributes.map(m => {
    //     console.log(m);
    //     return (
    //       <div className="variants-item-text" key={i.parentId._id}>
    //         <div className="attributeGroup">{m.attributeGroup.name}</div>
    //         <div className="attrValue">{m.value.value}</div>

    //       </div>
    //     );
    //   })}
    // </Link> </span>
    // });
    return (
      // <div className="variants-names-list">
      <div className="variant-list-wrapper">
        {/* <div className="flex flex-wrap justify-content-start variants-names-list-area"> */}
        <div className="variant-lists">{renderVariant}</div>
      </div>
    );
  }

  return null;
});

VariantList.propTypes = {
  productId: PropTypes.string.isRequired,
  isVariant: PropTypes.bool,
};

VariantList.defaultProps = {
  isVariant: false,
};

export default VariantList;
