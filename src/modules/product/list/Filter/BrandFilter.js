/* eslint-disable no-underscore-dangle */
import React, { useCallback, useState, useEffect } from 'react';
import Checkbox from 'components/Input/Checkbox';
import Skeleton from 'react-loading-skeleton';
import FilterContext from '../context';

const BrandFilter = () => {
  const { brands, onChange, selectedOptions } = React.useContext(FilterContext);
  const [searchText, setSearchText] = useState(null);
  const [isErrorSearch, setIsErrorSearch] = useState(false);
  // used prop directly before - changed since need to filter by search
  const [currentBrands, setCurrentBrands] = useState(brands && brands.data ? brands.data : []);

  const onSearch = useCallback(
    e => {
      if (e) e.preventDefault();
      if (searchText) {
        const brandsFilter = currentBrands.filter(i => {
          console.log('search', i.name, i.name.toLowerCase().startsWith(searchText));
          return i.name && i.name.toLowerCase().startsWith(searchText);
        });
        if (brandsFilter && brandsFilter.length > 0) setCurrentBrands(brandsFilter);
        else setIsErrorSearch(true);
      }
    },
    [currentBrands, searchText],
    [],
  );

  useEffect(() => {
    if (brands && brands.data) setCurrentBrands(brands.data);
  }, [brands]);

  const onChangeSearch = e => {
    setIsErrorSearch(false);
    e.preventDefault();
    const { value } = e.target;
    setSearchText(value);
    if (value === '') setCurrentBrands(brands.data);
  };

  const onKeyDownSearch = e => {
    if (e.key === 'Enter') {
      onSearch(e)
    }
  }

  const handleBrandsChange = useCallback(
    val => {
      onChange({ filters: { brand: val } });
    },
    [onChange],
  );

  const isSelectedProps = id => {
    let { brand } = selectedOptions;
    if (brand) {
      brand = Array.isArray(brand) ? brand : [brand];
    }
    return {
      isSelected: brand ? brand.findIndex(i => i === id) >= 0 : false,
    };
  };
  const options =
    currentBrands &&
    currentBrands.map(i => ({
      label: `${i.name} ${i.productCount ? `(${i.productCount})` : ''}`,
      disableSelection: i.disableSelection || false,
      value: i._id,
      ...isSelectedProps(i._id),
    }));
  // const options =
  //   brands.data &&
  //   brands.data.map(i => ({ label: i.name, value: i._id, ...isSelectedProps(i._id) }));
  return (
    <div className="filter-sec">
      <h5 className="filter-sec-head">Brands</h5>
      <div className="form-group mb-4">
        <div className="filter-search">
          <input
            type="text"
            className="form-control"
            placeholder="Search Brands"
            onChange={onChangeSearch}
            onKeyDown={onKeyDownSearch}
            disabled={brands && brands.loading}
          />
          <button type="button" onClick={onSearch} disabled={brands && brands.loading}>
            <img src="/resources/images/kickill-filter-search-icon.png" alt="" />
          </button>
        </div>
        {isErrorSearch && <div className="error-field">Your search does not match the filters</div>}
      </div>
      {brands && brands.loading && (
        <div>
          <Skeleton width={20} /><span className="mr-2" /><Skeleton width={100} />
          <div>
            <Skeleton width={20} /><span className="mr-2" /><Skeleton width={100} />
          </div>
          <div>
            <Skeleton width={20} /><span className="mr-2" /><Skeleton width={100} />
          </div>
          <div>
            <Skeleton width={20} /><span className="mr-2" /><Skeleton width={100} />
          </div>
          <div>
            <Skeleton width={20} /><span className="mr-2" /><Skeleton width={100} />
          </div>
        </div>
      )}
      {brands && brands.data && !brands.loading &&
        <Checkbox multiple options={options} name="brands" onChange={handleBrandsChange} />}
    </div>
  );
};

export default BrandFilter;
