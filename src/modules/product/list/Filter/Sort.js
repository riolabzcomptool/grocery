import React from 'react';
import FilterContext from '../context';

class Sort extends React.PureComponent {
  state = {
    sorters: {},
  };

  // componentDidUpdate(prevProps, prevState) {
  //   const { onChange } = this.context;
  //   const { sorters: prevSorters } = prevState;
  //   const { sorters: currentSorters } = this.state;
  //   if (prevSorters !== currentSorters) onChange({ sorters: currentSorters });
  // }

  onChangeSort = e => {
    const { name, value } = e.target;
    const { onChange } = this.context;
    if (value === 'reset')
      this.setState(
        prev => ({ ...prev, sorters: { } }),
        () => {
          const { sorters } = this.state;
          if (onChange) onChange({ sorters });
        },
      );
    else
      this.setState(
        prev => ({ ...prev, sorters: { ...prev.sorters, [name]: value } }),
        () => {
          const { sorters } = this.state;
          if (onChange) onChange({ sorters });
        },
      );
  };

  render() {
    console.log(this.context);
    return (
      <div className="sortby">
        Sort By
        <div className="filter-selectbox">
          <select name="pricing.salePrice" id="" onChange={this.onChangeSort}>
            <option value="1">Price: Low to High</option>
            <option value="-1">Price: High to Low</option>
            <option value="reset">Reset</option>
          </select>
        </div>
      </div>
    );
  }
}

// const Sort = () => {
//   console.log('hiii');
//   const [sorter, setSorters] = useState({});
//   const contextValue = React.useContext(FilterContext);
//   console.log('contextValue', contextValue);

//   const { onChange } = React.useContext(FilterContext);

//   useDidMountEffect(() => {
//     onChange({ sorters: sorter });
//   }, [sorter, onChange]);

//   const onChangeSort = e => {
//     const { name, value } = e.target;
//     setSorters(prev => ({ ...prev, [name]: value }));
//   };
//   return (
//     <div className="sortby">
//       Sort By
//       <div className="filter-selectbox">
//         <select name="price" id="" onChange={onChangeSort}>
//           <option value="1">Price: Low to High</option>
//           <option value="-1">Price: High to Low</option>
//         </select>
//       </div>
//     </div>
//   );
// };

Sort.contextType = FilterContext;
export default Sort;
