/* eslint-disable no-underscore-dangle */
import React from 'react';
import filterIcon from 'assets/images/kickill-filter-icon.svg';
import TagSelect from 'components/TagSelect';
import isEmpty from 'lodash/isEmpty';
import find from 'lodash/find';
import FilterContext from '../context';

const SelectedFilters = () => {
  const context = React.useContext(FilterContext);
  const { selectedOptions, brands, categories, onChange } = context;
  const transformedSelectedOptions = [];

  

  const handleClose = (id, type) => {
    
    if (type === 'category')
      onChange({ filters: { category: "" } });     
    if (type === "brand"){
      const updatedBrands = selectedOptions.brand.filter(i=> i!== id);
      onChange({filters: {brand:updatedBrands}});
    }  
  };

  if (!isEmpty(selectedOptions)) {
    Object.entries(selectedOptions).forEach(([key, value]) => {
      
      const type = key;
      let label = '';
      if (type === 'category') {
        if (categories && categories.data) {
          
          const obj = find(categories.data, i => {
            return i._id === value;
          });
          if (obj) label = obj.name;
        }
        transformedSelectedOptions.push({ label, value, type  });
      }
      if (type === 'brand') {
        value = !Array.isArray(value) ? [value] : value;
        value.forEach(i => {
          if (brands && brands.data) {
            const obj = find(brands.data, m => m._id === i);
            if (obj)
              transformedSelectedOptions.push({
                disableSelection: obj.disableSelection || false,
                label: obj.name,
                value: obj._id,
                type,
              });
          }
        });
      }
    });
  }

  return (
    <div>
      <div className="filter-head">
        Filter <img src={filterIcon} alt="filter-icon" />
      </div>

      <TagSelect onClose={handleClose} closeable options={transformedSelectedOptions} />
    </div>
  );
};

export default SelectedFilters;
