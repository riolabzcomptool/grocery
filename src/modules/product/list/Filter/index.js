import React from 'react';
import classNames from 'classnames';
import CategoryFilter from './CategoryFilter';
import BrandFilter from './BrandFilter';
import Sort from './Sort';
import SelectedFilters from './SelectedFilters';

const centerAlign = { textAlign: 'center' };
class FilterIndex extends React.PureComponent {
  state = {
    sort: {},
    toggleFilterMenu: false,
  };

  handleToggleFilterMenu = () => {
    this.setState(prev => ({ ...prev, toggleFilterMenu: !prev.toggleFilterMenu }));
  };

  render() {
    const { toggleFilterMenu } = this.state;
    console.log(this.state);
    return (
      <>
        <div
          className={classNames('filter-toggle', { active: toggleFilterMenu })}
          role="button"
          tabIndex={0}
          onKeyDown={this.handleToggleFilterMenu}
          onClick={this.handleToggleFilterMenu}
        >
          <div className="expand small-btn" style={centerAlign}>
            {/* <i className="fas fa-bars">Sort & Filter</i> */}
            Sort & Filter
          </div>
          <div className="close small-btn">
            <i className="fas fa-times" />
          </div>
        </div>
        <div className={classNames('filter-wrapper', { active: toggleFilterMenu })}>
          <Sort />

          <SelectedFilters />

          <CategoryFilter />

          <BrandFilter />
        </div>
      </>
    );
  }
}

export default FilterIndex;
