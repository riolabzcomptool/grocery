import React from 'react';

export default React.createContext({
  brands: {
    data: null,
    loading: false,
  },
  categories: {
    data: null,
    loading: false,
  },
});
