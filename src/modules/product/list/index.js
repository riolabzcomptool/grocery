/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import PageSection from 'components/PageSection';
// import useDidMountEffect from 'hooks/useDidMountEffect';
import { useProductsTest, useBrands, useCategories, useGetBrand, useGetCategory } from 'hooks/api';
// import ManageLoadingStates from 'components/ManageLoadingStates';
import has from 'lodash/has';
import isEmpty from 'lodash/isEmpty';
import qs from 'qs';
import omit from 'lodash/omit';
import Filter from './Filter';
import List from './List';
import FilterContext from './context';

const Index = props => {
  // const searchStr = '';
  console.log('PRODUCT LIST PROPS', props);
  const { match, location } = props;
  const { params } = match;
  const { type } = params;
  const { id } = params;
  console.log('mount', type, id);
  const { state } = location;

  console.log(state);

  let title = null;
  if (state && state.title) title = state.title;

  let search = null;
  if (location.search) {
    const parsedQuery = qs.parse(location.search, { ignoreQueryPrefix: true });
    if (parsedQuery.search) search = parsedQuery.search
  }

  React.useEffect(() => {
    console.log('seems props have changed !');
  }, [props]);

  // filter products according to these selected options
  const [selectedOptions, setSelectedOptions] = useState({});

  // set initial selectedOptions on component mount
  // useDidMountEffect(() => {

  const { data: brandRes, refetch: refetchBrand } = useGetBrand(id, true);
  const { data: categoryRes, refetch: refetchCategory } = useGetCategory(id, true);

  useEffect(() => {
    console.log('useDidMount', type, id);
    let options = {};
    // if (type && id) {
    //   if (type === 'brand') options = { [type]: [id] };
    //   else options = { [type]: id };
    // } // category: "11"
    if (type && id) {
      if (type === 'brand' && brandRes && brandRes.data) options = { brand: [brandRes.data._id] };
      if (type === 'category' && categoryRes && categoryRes.data)
        options = { category: categoryRes.data._id };
    } // category: "11"
    if (type && !id) options = { [type]: true }; // featured: true
    if (type === 'brand') refetchBrand();
    if (type === 'category') refetchCategory();

    setSelectedOptions(options);
  }, [type, id, refetchBrand, refetchCategory, brandRes, categoryRes]);

  console.log('selectedOptions', selectedOptions);

  // const { data, isLoading, isFetching } = useProducts(options);
  const { data, isFetching: isLoadingProds } = useProductsTest(search ? {...selectedOptions, search:{name:search, "productExtraInfo.description":search}} : selectedOptions);

  console.log(brandRes, categoryRes);

  const { data: resBrands, isFetching: isLoadingBrands, refetch: refetchBrands } = useBrands(
    {},
    true,
  );
  if (!type === 'brand') refetchBrands(); // if brand if not in url

  const { data: resCats, isFetching: isLoadingCats } = useCategories();
  console.log(data, resBrands, resCats);

  let brandsList = resBrands && resBrands.data ? resBrands.data : [];
  // if (!selectedOptions.brand && data && data.data && data.data.brands_list && data.data.brands_list.length > 0){
  if (
    data &&
    data.data &&
    data.data.brands_list &&
    data.data.brands_list.length > 0 &&
    type !== 'brand'
  ) {
    brandsList = data.data.brands_list.map(i => ({ ...i.brand, productCount: i.productCount }));
  }

  // url has brand
  if (type === 'brand' && brandRes && brandRes.data) {
    brandsList = [{ ...brandRes.data, disableSelection: true }];
  }

  const handleFiltersSorters = a => {
    Object.entries(a).forEach(([key, value]) => {
      console.log(key, value);
      const { brand } = value;
      const { category } = value;
      console.log(value);
      switch (key) {
        case 'filters':
          console.log(brand, category);
          console.log(has(value, 'category'), isEmpty(category), category);
          if (brand) setSelectedOptions(prev => ({ ...prev, brand }));
          if (has(value, 'category')) {
            if (isEmpty(category)) setSelectedOptions(prev => omit(prev, 'category'));
            else setSelectedOptions(prev => ({ ...prev, category }));
          }
          break;
        case 'sorters':
          setSelectedOptions(prev => {
            return {
              ...prev,
              sort: { ...value },
            };
          });
          break;
        default:
          break;
      }
    });
  };

  return (
    <PageSection>
      <div className="container">
        <div className="flex-wrapper products-wrapper">
          <FilterContext.Provider
            value={{
              brands: {
                // data: resBrands && resBrands.data ? resBrands.data : [],
                data: brandsList,
                loading: isLoadingProds || isLoadingBrands,
              },
              categories: {
                data: resCats && resCats.data ? resCats.data : [],
                loading: isLoadingCats,
              },
              selectedOptions,
              onChange: handleFiltersSorters,
            }}
          >
            <Filter />

            <List
              loading={isLoadingProds}
              data={data && data.data && data.data.products ? data.data.products : []}
              type={type}
              title={title || null}
            />
          </FilterContext.Provider>
          {/* {isLoading && <div>Loading...</div>}
          {data &&
            (isFetching ? (
              <div>Refreshing...</div>
            ) : (
              <Filter data={data}>
                {filteredData => {
                  console.log(filteredData);
                  return <List data={filteredData} />;
                }}
              </Filter>
            ))}
          {!isLoading && !data && !isFetching && <div>No data available</div>} */}
        </div>
      </div>
    </PageSection>
  );
};

export default Index;
