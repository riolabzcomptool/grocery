import React from 'react';
import PropTypes from 'prop-types';
import useFetching from 'hooks/useFetching';
import qs from 'qs';

const Filter = props => {
  const { render } = props;
  console.log(qs.parse('/api/catalog/v1?brands[]=abc&brands[]=def&brands[]=ghi'));
  const [{ response }] = useFetching('/api/catalog/v1/products?brands[]=abc&brands[]=def');
  const data = null;
  console.log(response && response.data);
  return (
    <>
      <div className="filter-wrapper">
        <div className="sortby">
          Sort By
          <div className="filter-selectbox">
            <select name="" id="">
              <option value="">Price: High to Low</option>
              <option value="">Price: High to Low</option>
              <option value="">Price: High to Low</option>
            </select>
          </div>
        </div>

        <div className="filter-head">
          Filter <img src="/resources/images/kickill-filter-icon.svg" alt="" />
        </div>

        <div className="filter-sec">
          <h5 className="filter-sec-head">Categories</h5>
          <div className="filter-menu-accordion">
            {/* Accordion wrapper */}
            <div
              className="accordion md-accordion"
              id="accordionEx"
              role="tablist"
              aria-multiselectable="true"
            >
              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingOne1">
                  <a
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#collapseOne1"
                    aria-expanded="true"
                    aria-controls="collapseOne1"
                  >
                    <h5 className="mb-0">
                      Fertility & Gynecology <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="collapseOne1"
                  className="collapse show"
                  role="tabpanel"
                  aria-labelledby="headingOne1"
                  data-parent="#accordionEx"
                >
                  <div className="card-body">
                    <div className="filter-list">
                      <a href="/#">Wellness Pure Tablet</a>
                      <a href="/#">Confido Tablet</a>
                      <a href="/#">Himcolin Gel</a>
                      <a href="/#">Liv. 52 DS Tablet</a>
                      <a href="/#">Speman Tablet</a>
                      <a href="/#">Jiva Ashwagandha Tablet</a>
                      <a href="/#">Wellness Pure Tablet</a>
                      <a href="/#">Confido Tablet</a>
                      <a href="/#">Himcolin Gel</a>
                      <a href="/#">Liv. 52 DS Tablet</a>
                      <a href="/#">Speman Tablet</a>
                      <a href="/#">Jiva Ashwagandha Tablet</a>
                    </div>
                  </div>
                </div>
              </div>
              {/* Accordion card */}

              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingTwo2">
                  <a
                    className="collapsed"
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#collapseTwo2"
                    aria-expanded="false"
                    aria-controls="collapseTwo2"
                  >
                    <h5 className="mb-0">
                      Ayurvedic & Homeopathy <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="collapseTwo2"
                  className="collapse"
                  role="tabpanel"
                  aria-labelledby="headingTwo2"
                  data-parent="#accordionEx"
                >
                  <div className="card-body" />
                </div>
              </div>
              {/* Accordion card */}

              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingThree3">
                  <a
                    className="collapsed"
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#collapseThree3"
                    aria-expanded="false"
                    aria-controls="collapseThree3"
                  >
                    <h5 className="mb-0">
                      Medical Devices <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="collapseThree3"
                  className="collapse"
                  role="tabpanel"
                  aria-labelledby="headingThree3"
                  data-parent="#accordionEx"
                >
                  <div className="card-body">12345678</div>
                </div>
              </div>
              {/* Accordion card */}

              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingThree4">
                  <a
                    className="collapsed"
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#collapseThree4"
                    aria-expanded="false"
                    aria-controls="collapseThree3"
                  >
                    <h5 className="mb-0">
                      Non Prescription <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="collapseThree4"
                  className="collapse"
                  role="tabpanel"
                  aria-labelledby="headingThree3"
                  data-parent="#accordionEx"
                >
                  <div className="card-body">12345678</div>
                </div>
              </div>
              {/* Accordion card */}
            </div>
            {/* Accordion wrapper */}
          </div>
        </div>

        <div className="filter-sec">
          <h5 className="filter-sec-head">Brands</h5>
          <div className="filter-search">
            <input type="text" className="form-control" placeholder="Search Brands" />
            <button type="button">
              <img src="/resources/images/kickill-filter-search-icon.png" alt="" />
            </button>
          </div>
          <div className="filter-menu-accordion">
            {/* Accordion wrapper */}
            <div
              className="accordion md-accordion"
              id="accordionEx"
              role="tablist"
              aria-multiselectable="true"
            >
              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingOne1">
                  <a
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#brandsOne1"
                    aria-expanded="true"
                    aria-controls="collapseOne1"
                  >
                    <h5 className="mb-0">
                      Fertility & Gynecology <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="brandsOne1"
                  className="collapse show"
                  role="tabpanel"
                  aria-labelledby="headingOne1"
                  data-parent="#accordionEx"
                >
                  <div className="card-body">
                    <div className="filter-list">
                      <a href="/#">Wellness Pure Tablet</a>
                      <a href="/#">Confido Tablet</a>
                      <a href="/#">Himcolin Gel</a>
                      <a href="/#">Liv. 52 DS Tablet</a>
                      <a href="/#">Speman Tablet</a>
                      <a href="/#">Jiva Ashwagandha Tablet</a>
                      <a href="/#">Wellness Pure Tablet</a>
                      <a href="/#">Confido Tablet</a>
                      <a href="/#">Himcolin Gel</a>
                      <a href="/#">Liv. 52 DS Tablet</a>
                      <a href="/#">Speman Tablet</a>
                      <a href="/#">Jiva Ashwagandha Tablet</a>
                    </div>
                  </div>
                </div>
              </div>
              {/* Accordion card */}

              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingTwo2">
                  <a
                    className="collapsed"
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#brandsTwo2"
                    aria-expanded="false"
                    aria-controls="collapseTwo2"
                  >
                    <h5 className="mb-0">
                      Ayurvedic & Homeopathy <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="brandsTwo2"
                  className="collapse"
                  role="tabpanel"
                  aria-labelledby="headingTwo2"
                  data-parent="#accordionEx"
                >
                  <div className="card-body" />
                </div>
              </div>
              {/* Accordion card */}

              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingThree3">
                  <a
                    className="collapsed"
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#brandsThree3"
                    aria-expanded="false"
                    aria-controls="collapseThree3"
                  >
                    <h5 className="mb-0">
                      Medical Devices <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="brandsThree3"
                  className="collapse"
                  role="tabpanel"
                  aria-labelledby="headingThree3"
                  data-parent="#accordionEx"
                >
                  <div className="card-body">12345678</div>
                </div>
              </div>
              {/* Accordion card */}

              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingThree4">
                  <a
                    className="collapsed"
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#brandsThree4"
                    aria-expanded="false"
                    aria-controls="collapseThree3"
                  >
                    <h5 className="mb-0">
                      Non Prescription <i className="fas fa-angle-down rotate-icon" />
                    </h5>
                  </a>
                </div>

                {/* Card body */}
                <div
                  id="brandsThree4"
                  className="collapse"
                  role="tabpanel"
                  aria-labelledby="headingThree3"
                  data-parent="#accordionEx"
                >
                  <div className="card-body">12345678</div>
                </div>
              </div>
              {/* Accordion card */}
            </div>
            {/* Accordion wrapper */}
          </div>
        </div>
      </div>
      {data && render(data)}
    </>
  );
};

Filter.propTypes = {
  // data: PropTypes.object,
  render: PropTypes.func.isRequired,
};

Filter.defaultProps = {
  // data: null,
};

export default Filter;
