import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useProducts } from 'hooks/api';
import Filter from './Filter';
import Sort from './Sort';

const centerAlign = { textAlign: 'center' };

const SortFilter = React.memo(props => {
  const { render, options } = props;

  const { data: response } = useProducts(options);

  // console.log(data)
  // data = _.uniq(data, '_id');
  // console.log('unique data', data)
  const [toggleFilterMenu, setToggleFilterMenu] = useState(false);
  const handleToggleFilterMenu = () => {
    setToggleFilterMenu(prev => !prev);
  };
  return (
    <>
      <div
        className={classNames('filter-toggle', { active: toggleFilterMenu })}
        role="button"
        tabIndex={0}
        onKeyDown={handleToggleFilterMenu}
        onClick={handleToggleFilterMenu}
      >
        <div className="expand small-btn" style={centerAlign}>
          {/* <i className="fas fa-bars">Sort & Filter</i> */}
          Sort & Filter
        </div>
        <div className="close small-btn">
          <i className="fas fa-times" />
        </div>
      </div>
      <div className={classNames('filter-wrapper', { active: toggleFilterMenu })}>
        <Sort />

        <div className="filter-head">
          Filter <img src="/resources/images/kickill-filter-icon.svg" alt="" />
        </div>
        <Filter title="categories" type="category" search />
        <Filter title="brands" type="brand" search />
      </div>
      {response && response.data && render(response.data)}
    </>
  );
});

SortFilter.propTypes = {
  render: PropTypes.func.isRequired,
};

SortFilter.defaultProps = {};

export default SortFilter;
