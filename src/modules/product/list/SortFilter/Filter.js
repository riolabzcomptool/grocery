/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { getCategories } from 'services/category';
import { getBrands } from 'services/brands';

class Filter extends React.PureComponent {
  state = {
    data: [],
  };

  componentDidMount() {
    const { type } = this.props;
    this.getData(type);
  }

  getData = async type => {
    if (type === 'category') {
      const list = await getCategories();
      this.setState({ data: list });
    }
    if (type === 'brand') {
      const list = await getBrands();
      this.setState({ data: list });
    }
  };

  render() {
    const { title, search } = this.props;
    const { data } = this.state;
    console.log(data);
    return (
      <div className="filter-sec">
        <h5 className="filter-sec-head">{title}</h5>
        {search && (
          <div className="filter-search">
            <input type="text" className="form-control" placeholder={`Search ${title}`} />
            <button type="button">
              <img src="/resources/images/kickill-filter-search-icon.png" alt="" />
            </button>
          </div>
        )}
        <div className="filter-menu-accordion">
          {/* Accordion wrapper */}
          <div
            className="accordion md-accordion"
            id="accordionEx"
            role="tablist"
            aria-multiselectable="true"
          >
            {/* Accordion card */}
            {data
              .filter(i => !i.parent)
              .map(i => (
                <div className="card">
                  <div className="card-header" role="tab" id={`${i._id}_head`}>
                    <a
                      data-toggle="collapse"
                      data-parent="#accordionEx"
                      href={`#${i._id}collapse`}
                      aria-expanded="true"
                      aria-controls={`${i._id}collapse`}
                    >
                      <h5 className="mb-0">
                        Fertility & Gynecology <i className="fas fa-angle-down rotate-icon" />
                      </h5>
                    </a>
                  </div>

                  <div
                    id={`${i._id}collapse`}
                    className="collapse show"
                    role="tabpanel"
                    aria-labelledby={`${i._id}_head`}
                    data-parent="#accordionEx"
                  >
                    <div className="card-body">
                      <div className="filter-list">
                        {data
                          .filter(m => m.parent && m.parent._id === m._id)
                          .map(m => (
                            <a href="#">{m.name}</a>
                          ))}
                      </div>
                    </div>
                  </div>
                </div>
              ))}

            {/* Accordion card */}
            <div className="card">
              {/* Card header */}
              <div className="card-header" role="tab" id="headingTwo2">
                <a
                  className="collapsed"
                  data-toggle="collapse"
                  data-parent="#accordionEx"
                  href="#collapseTwo2"
                  aria-expanded="false"
                  aria-controls="collapseTwo2"
                >
                  <h5 className="mb-0">
                    Ayurvedic & Homeopathy <i className="fas fa-angle-down rotate-icon" />
                  </h5>
                </a>
              </div>

              {/* Card body */}
              <div
                id="collapseTwo2"
                className="collapse"
                role="tabpanel"
                aria-labelledby="headingTwo2"
                data-parent="#accordionEx"
              >
                <div className="card-body" />
              </div>
            </div>
            {/* Accordion card */}

            {/* Accordion card */}
            <div className="card">
              {/* Card header */}
              <div className="card-header" role="tab" id="headingThree3">
                <a
                  className="collapsed"
                  data-toggle="collapse"
                  data-parent="#accordionEx"
                  href="#collapseThree3"
                  aria-expanded="false"
                  aria-controls="collapseThree3"
                >
                  <h5 className="mb-0">
                    Medical Devices <i className="fas fa-angle-down rotate-icon" />
                  </h5>
                </a>
              </div>

              {/* Card body */}
              <div
                id="collapseThree3"
                className="collapse"
                role="tabpanel"
                aria-labelledby="headingThree3"
                data-parent="#accordionEx"
              >
                <div className="card-body">12345678</div>
              </div>
            </div>
            {/* Accordion card */}

            {/* Accordion card */}
            <div className="card">
              {/* Card header */}
              <div className="card-header" role="tab" id="headingThree4">
                <a
                  className="collapsed"
                  data-toggle="collapse"
                  data-parent="#accordionEx"
                  href="#collapseThree4"
                  aria-expanded="false"
                  aria-controls="collapseThree3"
                >
                  <h5 className="mb-0">
                    Non Prescription <i className="fas fa-angle-down rotate-icon" />
                  </h5>
                </a>
              </div>

              {/* Card body */}
              <div
                id="collapseThree4"
                className="collapse"
                role="tabpanel"
                aria-labelledby="headingThree3"
                data-parent="#accordionEx"
              >
                <div className="card-body">12345678</div>
              </div>
            </div>
            {/* Accordion card */}
          </div>
          {/* Accordion wrapper */}
        </div>
      </div>
    );
  }
}

Filter.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  search: PropTypes.bool,
};

Filter.defaultProps = {
  title: '',
  type: null,
  search: false,
};

export default Filter;
