/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect, Suspense } from 'react';

import PageSection from 'components/PageSection';
import { AddButton } from 'components/Button';
// import Slider from 'react-slick';
import { connect } from 'react-redux';
import { cartActions } from 'redux/actions';
import find from 'lodash/find';
// import { Link } from 'react-router-dom';
import toaster from 'toasted-notes';
// import { STRINGS } from '_constants';
import { useProduct } from 'hooks/api';
import usePrevious from 'hooks/usePrevious';
import './styles.scss';
// import Toaster from 'components/Toaster';
import Loader from 'components/Loader';
import Tabs, { TabsPanel } from 'components/TabsOld';
import { createMarkup } from '_utils';
import Carousel from 'components/Carousel';
import OwlCarousel from 'react-owl-carousel';
import Product from 'components/Product';


const BreadCrumbs = React.lazy(() => import('components/BreadCrumbs'));
const ProductDetail = React.lazy(() => import('../components/ProductDetail'));
const ProductPricing = React.lazy(() => import('../components/ProductPricing'));
const SimilarProducts = React.lazy(() => import('../components/SimilarProducts'));
// const RelatedProducts = React.lazy(() => import('../components/RelatedProducts'));
// const AddReview = React.lazy(() => import('../components/AddReview'));
// const FrequentlyBought = React.lazy(() => import('../components/FrequentlyBought'));
// import data from './data.json';
const VariantList = React.lazy(() => import('../components/VariantList'));
const ReviewsList = React.lazy(() => import('../components/ReviewsList'));

const carImgStyle = {
  width: '100%',
};

// const freqBought = [
//   {
//     id: '123',
//     name: 'Becadexamin Soft Gelatin Capsule',
//     pricing: {
//       listPrice: 3450,
//       salePrice: 3000,
//     },
//     image: '/resources/images/products/kickill-product-6.png',
//   },
//   {
//     id: '124',
//     name: 'Becadexamin Soft Gelatin Tablet',
//     pricing: {
//       listPrice: 3450,
//       salePrice: 2000,
//     },
//     image: '/resources/images/products/kickill-product-6.png',
//   },
// ];

const ProductDetailPage = props => {
  const [selectedQty, setSelectedQty] = useState(0);
  // const [goToCart, setGoToCart] = useState(0);

  const { match, history, dispatch, cartLoading } = props;
  const { params } = match;
  const { slug } = params;
  const prevSlug = usePrevious(slug);
  console.log(props);

  const { refetch: refetchProds, data: resProds, isFetching: isLoadingProds, error } = useProduct(
    slug,
  );

  useEffect(() => {
    if (resProds && resProds.data) setSelectedQty(resProds.data.minOrderQty);
  }, [resProds]);

  console.log('error', error);

  useEffect(() => {
    if (slug !== prevSlug) {
      refetchProds();
      // refetchReviews();
    }
  }, [slug, refetchProds, prevSlug]);

  const handleBuyProduct = () => {
    if (resProds && resProds.data && resProds.data) {
      const product = resProds.data;
      dispatch(
        {
          type: cartActions.ADD_TO_CART,
          payload: {
            productId: product._id,
            slug: product.slug,
            name: product.name,
            qty: selectedQty,
            listPrice: product.pricing ? product.pricing.listPrice : '',
            salePrice: product.pricing ? product.pricing.salePrice : '',
            img: product.images ? product.images[0].url : '',
            thumbImg: product.images ? product.images[0].thumbnail : '',
            maxOrderQty: product.maxOrderQty,
            minOrderQty: product.minOrderQty,
            prescriptionNeeded: product.prescriptionNeeded,
            deleted: product.deleted,
            status: product.status,
            outOfStockStatus: product.stock ? product.stock.outOfStockStatus : '',
          },
        },
        // toaster.notify(STRINGS.added_to_cart, { position: 'top-right' }),
        // toaster.notify(
        //   ({ onClose }) => (
        //     <>
        //       <Toaster onClose={onClose} text={STRINGS.added_to_cart} success />
        //     </>
        //   ),
        //   {
        //     position: 'top-right',
        //     // duration: null,
        //   },
        // ),
        // setGoToCart(true),
      );
    }
  };

  const handleQtyChange = e => {
    setSelectedQty(parseInt(e.target.value, 10));
  };

  // const handleBuyFrequently = () => {
  //   console.log('clicked to buy frequently bought');
  // };

  if (error)
    return (
      <PageSection>
        <div>Error fetching product</div>
      </PageSection>
    );

  if (isLoadingProds)
    return (
      <PageSection>
        <Loader />
      </PageSection>
    );

  if (!resProds || !resProds.data) return null;
  if (!isLoadingProds && resProds && !resProds.data) {
    toaster.notify('Product not available');
    history.go(-1);
    return null;
  }

  const product = resProds && resProds.data ? resProds.data : {};

  const { images, attributes, productExtraInfo } = product;

  //     keyBenefits
  // directionsForUse
  // safetyInfo
  // otherInfo
  const tabs = [
    {
      title: 'Description',
      content:
        productExtraInfo && productExtraInfo.description !== '' ? (
          <div className="tab-content">
            <div
              dangerouslySetInnerHTML={createMarkup(
                productExtraInfo ? productExtraInfo.description : '',
              )}
            />
          </div>
        ) : null,
    },
    {
      title: 'Key Benefits',
      content:
        productExtraInfo && productExtraInfo.keyBenefits !== '' ? (
          <div className="tab-content">
            <div
              dangerouslySetInnerHTML={createMarkup(
                productExtraInfo ? productExtraInfo.keyBenefits : '',
              )}
            />
          </div>
        ) : null,
    },
    {
      title: 'Directions for use/dosage',
      content:
        productExtraInfo && productExtraInfo.directionsForUse !== '' ? (
          <div className="tab-content">
            <div
              dangerouslySetInnerHTML={createMarkup(
                productExtraInfo ? productExtraInfo.directionsForUse : '',
              )}
            />
          </div>
        ) : null,
    },
    {
      title: 'Safety Informations/precaution',
      content:
        productExtraInfo && productExtraInfo.safetyInfo !== '' ? (
          <div className="tab-content">
            <div
              dangerouslySetInnerHTML={createMarkup(
                productExtraInfo ? productExtraInfo.safetyInfo : '',
              )}
            />
          </div>
        ) : null,
    },
    {
      title: 'Other Informations',
      content:
        productExtraInfo && productExtraInfo.otherInfo !== '' ? (
          <div className="tab-content">
            <div
              dangerouslySetInnerHTML={createMarkup(
                productExtraInfo ? productExtraInfo.otherInfo : '',
              )}
            />
          </div>
        ) : null,
      // content: (
      //   <>
      //     <p>Not returnable Read policy</p>
      //     <p>Frequent searches leading to this page</p>
      //     <p>
      //       HealthVit Vitamin B12 2000mcg Tablet Price, Buy HealthVit Vitamin B12 2000mcg Tablet
      //       Online,
      //     </p>
      //   </>
      // ),
    },
    {
      title: 'Reviews',
      content: <ReviewsList productId={product._id} />,
    },
  ];

  const batchTypeAttr = find(attributes, a => {
    console.log(a);
    return a.attributeGroup.attribute_group_code === 'type';
  });
  const batchQtyAttr = find(attributes, a => {
    return a.attributeGroup.attribute_group_code === 'batch-qty';
  });

  console.log('geeee', batchTypeAttr, batchQtyAttr);
  const batchType = batchTypeAttr ? batchTypeAttr.value.value : '';
  const batchQty = batchQtyAttr ? batchQtyAttr.value.value : '';

  console.log(batchTypeAttr, batchQtyAttr);
  console.log(batchQty, product.medicineType);
  const quantities = [];
  let { minOrderQty } = product;
  const { maxOrderQty } = product;
  if (minOrderQty && maxOrderQty) {
    while (minOrderQty <= maxOrderQty) {
      quantities.push(minOrderQty);
      minOrderQty += 1;
    }
  }
  console.log(quantities);

  let bread = [];

  bread = [
    {
      url: '/home',
      text: 'Home',
    },
    {
      text: product.name,
      url: `/products/${product._id}`,
    },
  ];

  const renderProducts = type => {
    // relatedProducts
    // frequentlyBought
    let text = '';
    switch (type) {
      case 'relatedProducts':
        text = `Products related to ${product.name}`;
        break;
      case 'frequentlyBought':
        text = 'Frequently bought together';
        break;
      default:
        break;
    }
    if (product[type] && product[type].length > 0) {
      return (
        <div className="similar-products-wrapper">
          {type !== 'frequentlyBought' && <h4 className="similar-products-offers-head">{text}</h4>}
          {type === 'frequentlyBought' && <h5 className="brought-head h5-left-border">{text}</h5>}
          <Carousel resetDefaults className="similar-product-slider">
            {product[type].map(i => (
              <Product
                slug={i.slug}
                key={i._id}
                id={i._id}
                title={i.name}
                description={i.textDescription}
                price={i.pricing.salePrice}
                img={i.images.length > 0 ? i.images[0].thumbnail : ''}
                noButton
                noFav
              />
            ))}
          </Carousel>
        </div>
      );
    }
    return null;
  };

  return (
    <PageSection className="blogs-wrapper">
      <Suspense fallback={<Loader />}>
        <div className="container">
          <div className="products-detail-3-sections">
            <OwlCarousel
              className="products-image-slider"
              center
              nav
              items={1}
              lazyLoad
              lazyContent={false}
              // dots
              // className="owl-theme owl-carousel"
              margin={10}
            >
              {images.map(i => (
                <div key={i._id}>
                  <img style={carImgStyle} src={`/${i.url}`} alt="" />
                </div>
              ))}
            </OwlCarousel>
            {/* <Slider {...carouselSettings} className="products-image-slider">
                {images.map(i => (
                  <div key={i._id}>
                    <img style={carImgStyle} src={`/${i.url}`} alt="" />
                  </div>
                ))}
              </Slider> */}
            <div className="products-details-wrapper">
              <ProductDetail
                prescriptionNeeded={product.prescriptionNeeded}
                title={product.name || ''}
                brand={product.brand && product.brand.name ? product.brand.name : ''}
                // count={product.purchaseCount || 0}
              />

              {product.pricing && (
                <ProductPricing
                  originalPrice={product.pricing.listPrice}
                  offerPrice={product.pricing.salePrice}
                >
                  <div className="productdetail-qty-addtocart">
                    <div className="productdetail-qty-wrapper">
                      <select
                        value={selectedQty}
                        onChange={handleQtyChange}
                        className="form-control"
                      >
                        {quantities.map(item => (
                          <option key={item} value={item}>
                            {`${item} ${batchType}`}
                          </option>
                        ))}
                      </select>
                      {/* Of {`${product.qtyPerBatch || ''} ${product.medicineType}`} */}
                      Of {`${batchQty} ${product.medicineType.name}`}
                    </div>

                    <AddButton loading={cartLoading} onClick={handleBuyProduct}>
                      Add to cart
                    </AddButton>
                    {/* {goToCart ? (
                      <Link className="left-10" to="/cart">
                        <AddButton>Go to Cart</AddButton>
                      </Link>
                    ) : null} */}
                  </div>
                </ProductPricing>
              )}
              <VariantList
                productId={product.parentId === null ? product._id : product.parentId}
                isVariant={product.variantType === 'single' && product.variantCount > 0}
                currentProductId={product._id}
              />
              {/* <FrequentlyBought products={freqBought} onBuyFrequently={handleBuyFrequently} /> */}

              {renderProducts('frequentlyBought')}
            </div>

            <div className="similar-products-offers">
              {/* <div className="breadcrumbs-wrapper">
                  <ul>
                    <li>
                      <a href="/#"">Home</a>
                    </li>
                    <li>/</li>
                    <li>
                      <a href="/#"">Products</a>
                    </li>
                    <li>/</li>
                    <li>Product Details</li>
                  </ul>
                </div> */}
              <BreadCrumbs bread={bread} />

              <SimilarProducts
                products={product.similarProducts && product.similarProducts.length ? product.similarProducts : []}
                productId={product._id}
                organics={product.organic}
                productTitle={product.name || ''}
              />

              <div className="additional-offers-wrapper">
                <span className="additional-offers-head">Additional offers</span>
                <p>
                  Freecharge: 25% Cashback up to ₹100 | Valid once per user from 18 Sep to 30 Apr
                </p>
                <p>Mobikwik: 20% SuperCash up to ₹250 | Valid twice per user from 1 to 30 Apr</p>
              </div>
            </div>
          </div>
          {renderProducts('relatedProducts')}
          <div className="tabs-reviews-wrapper">
            <Tabs>
              {tabs
                .filter(i => i.content !== null)
                .map(tabContent => (
                  <TabsPanel id={tabContent.title} label={tabContent.title} key={tabContent.title}>
                    {tabContent.content}
                  </TabsPanel>
                ))}
            </Tabs>

            {/* <AddReview /> */}
          </div>
        </div>
      </Suspense>
    </PageSection>
  );
};

export default connect(({ user, cart }) => {
  console.log(cart);
  return { user, cartLoading: cart.loading };
})(ProductDetailPage);
