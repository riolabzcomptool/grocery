export const productActions = {
  GET_PRODUCT: 'product/GET_PRODUCT',
  SET_STATE: 'product/SET_STATE',
}
