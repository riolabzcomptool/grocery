/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect, useMemo } from 'react';
import Slider from 'react-slick';
import PageSection from 'components/PageSection';
// import { AddButton } from 'components/Button';
// import Slider from 'react-slick';
import { connect } from 'react-redux';
import { cartActions } from 'redux/actions';
// import find from 'lodash/find';
// import { Link } from 'react-router-dom';
import toaster from 'toasted-notes';
// import { STRINGS } from '_constants';
import { useProduct } from 'hooks/api';
import usePrevious from 'hooks/usePrevious';
// import './styles.scss';
// import Toaster from 'components/Toaster';
import Loader from 'components/Loader';
import Tabs, { TabsPanel } from 'components/TabsOld';
// import { createMarkup } from '_utils';
// import Carousel from 'components/Carousel';
// import OwlCarousel from 'react-owl-carousel';
import Product from 'components/Product';
import ReviewForm from 'components/ReviewForm';
import { createReview } from 'services/review';
import QtyInput from 'components/QtyInput';
import { Helmet } from 'react-helmet';

const BreadCrumbs = React.lazy(() => import('components/BreadCrumbs'));
// const ProductDetail = React.lazy(() => import('../components/ProductDetail'));
// const ProductPricing = React.lazy(() => import('../components/ProductPricing'));
// const SimilarProducts = React.lazy(() => import('../components/SimilarProducts'));
// const RelatedProducts = React.lazy(() => import('../components/RelatedProducts'));
// const AddReview = React.lazy(() => import('../components/AddReview'));
// const FrequentlyBought = React.lazy(() => import('../components/FrequentlyBought'));
// import data from './data.json';
// const VariantList = React.lazy(() => import('../components/VariantList'));
// const ReviewsList = React.lazy(() => import('../components/ReviewsList'));

// const carImgStyle = {
//   width: '100%',
// };

// const styles = {
//   notify: {
//     display: 'inline-block',
//     margin: '0px auto',
//     position: 'fixed',
//     transition: 'all 0.5s ease-in-out 0s',
//     zIndex: '1031',
//     top: '20px',
//     right: '20px',
//   },
//   notifyBtn: {
//     position: 'absolute',
//     right: '10px',
//     top: '5px',
//     zIndex: '1033',
//   },
//   progressBar: {
//     width: '120%',
//   },
// };

// const freqBought = [
//   {
//     id: '123',
//     name: 'Becadexamin Soft Gelatin Capsule',
//     pricing: {
//       listPrice: 3450,
//       salePrice: 3000,
//     },
//     image: '/resources/images/products/kickill-product-6.png',
//   },
//   {
//     id: '124',
//     name: 'Becadexamin Soft Gelatin Tablet',
//     pricing: {
//       listPrice: 3450,
//       salePrice: 2000,
//     },
//     image: '/resources/images/products/kickill-product-6.png',
//   },
// ];

const ProductDetailPage = props => {
  // eslint-disable-next-line no-unused-vars
  const [selectedQty, setSelectedQty] = useState(0);
  // const [goToCart, setGoToCart] = useState(0);

  const { match, history, dispatch, cartLoading, cartProducts, user } = props;
  const { params } = match;
  const { slug } = params;
  const prevSlug = usePrevious(slug);
  console.log(props);

  const { refetch: refetchProds, data: resProds, isFetching: isLoadingProds, error } = useProduct(
    slug,
  );

  useEffect(() => {
    if (resProds && resProds.data) setSelectedQty(resProds.data.minOrderQty);
  }, [resProds]);

  console.log('error', error);

  useEffect(() => {
    if (slug !== prevSlug) {
      refetchProds();
      // refetchReviews();
    }
  }, [slug, refetchProds, prevSlug]);

  const product = resProds && resProds.data ? resProds.data : {};
  const sliderSettings = useMemo(() => {
    if (product)
      return {
        customPaging: i => {
          return (
            <a>
              <img src={`/${product.images[i].thumbnail}`} alt={product.images[0].title} />
            </a>
          );
        },
        dots: true,
        dotsClass: 'slider-nav',
        fade: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
      };
    return {};
  }, [product]);

  const relatedSliderSettings = useMemo(
    () => ({
      arrows: true,
      dots:true,
      slidesToShow: 7,
      infinite: true,
      speed: 500,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 5,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3,
          },
        },
      ],
    }),
    [],
  );

  console.log('ahaha', relatedSliderSettings);

  if (error)
    return (
      <PageSection>
        <div>Error fetching product</div>
      </PageSection>
    );

  if (isLoadingProds)
    return (
      <PageSection>
        <Loader />
      </PageSection>
    );

  if (!resProds || !resProds.data) return null;
  if (!isLoadingProds && resProds && !resProds.data) {
    toaster.notify('Product not available');
    history.go(-1);
    return null;
  }

  const quantities = [];
  let { minOrderQty } = product;
  const { maxOrderQty } = product;
  if (minOrderQty && maxOrderQty) {
    while (minOrderQty <= maxOrderQty) {
      quantities.push(minOrderQty);
      minOrderQty += 1;
    }
  }
  console.log(quantities);

  let bread = [];

  bread = [
    {
      url: '/home',
      text: 'Home',
    },
    {
      text: product.name,
      url: `/products/${product._id}`,
    },
  ];

  // const renderProducts = type => {
  //   // relatedProducts
  //   // frequentlyBought
  //   let text = '';
  //   switch (type) {
  //     case 'relatedProducts':
  //       text = `Products related to ${product.name}`;
  //       break;
  //     case 'frequentlyBought':
  //       text = 'Frequently bought together';
  //       break;
  //     default:
  //       break;
  //   }
  //   if (product[type] && product[type].length > 0) {
  //     return (
  //       <div className="similar-products-wrapper">
  //         {type !== 'frequentlyBought' && <h4 className="similar-products-offers-head">{text}</h4>}
  //         {type === 'frequentlyBought' && <h5 className="brought-head h5-left-border">{text}</h5>}
  //         <Carousel resetDefaults className="similar-product-slider">
  //           {product[type].map(i => (
  //             <Product
  //               slug={i.slug}
  //               key={i._id}
  //               id={i._id}
  //               title={i.name}
  //               description={i.textDescription}
  //               price={i.pricing.salePrice}
  //               img={i.images.length > 0 ? i.images[0].thumbnail : ''}
  //               noButton
  //               noFav
  //             />
  //           ))}
  //         </Carousel>
  //       </div>
  //     );
  //   }
  //   return null;
  // };

  const discount = Math.floor(
    ((product?.pricing?.listPrice - product?.pricing?.salePrice) / product?.pricing?.listPrice) *
      100,
  ).toFixed(2);

  const currentQty = cartProducts.find(i => i.productId === product._id)?.qty || 1;
  console.log(
    'pooh',
    currentQty,
    cartProducts,
    cartProducts.find(i => i.productId === product._id),
  );
  const onIncQty = () => {
    dispatch({
      type: cartActions.INC_QTY,
      payload: {
        productId: product._id,
        maxOrderQty: product.maxOrderQty,
        ...product,
      },
    });
  };

  const onDecQty = () => {
    dispatch({
      type: cartActions.DEC_QTY,
      payload: {
        productId: product._id,
        minOrderQty: product.minOrderQty,
        ...product,
      },
    });
  };

  const onSubmitReview = async vals => {
    console.log('will submit review', vals);
    await createReview({
      // orderItemId: item.id,
      // orderId,
      productId: product._id,
      text: vals.text,
      rating: vals.rating,
      title: vals.title,
    });
  };

  return (
    <>
      <Helmet>
        <title>{product.seo?.metaTitle}</title>
        <meta name="description" content={product.seo?.metaDescription} />
        <meta name="keywords" content={product.seo?.metaKeywords} />
      </Helmet>
      <BreadCrumbs bread={bread} />
      <section className="">
        <div className="collection-wrapper">
          <div className="container">
            <div className="row">
              <div className="col-lg-9 col-sm-12 col-xs-12">
                <div className="container-fluid p-0">
                  <div className="row">
                    <div className="col-xl-12">
                      <div className="filter-main-btn mb-2">
                        <span className="filter-btn">
                          <i className="fa fa-filter" aria-hidden="true" /> filter
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6">
                      <Slider {...sliderSettings}>
                        {product.images.map(i => (
                          <div key={i.url}>
                            <img className="img-fluid" src={`/${i.url}`} alt={i.title || ''} />
                          </div>
                        ))}
                      </Slider>
                    </div>
                    {/* slick slider */}
                    {/* <div className="col-lg-6">
                      <div className="product-slick">
                        <div>
                          <img
                            src="assets/images/product/1.jpg"
                            alt=""
                            className="img-fluid  image_zoom_cls-0"
                          />
                        </div>
                        <div>
                          <img
                            src="assets/images/product/2.jpg"
                            alt=""
                            className="img-fluid  image_zoom_cls-1"
                          />
                        </div>
                        <div>
                          <img
                            src="assets/images/product/27.jpg"
                            alt=""
                            className="img-fluid  image_zoom_cls-2"
                          />
                        </div>
                        <div>
                          <img
                            src="assets/images/product/27.jpg"
                            alt=""
                            className="img-fluid  image_zoom_cls-3"
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 p-0">
                          <div className="slider-nav">
                            <div>
                              <img
                                src="assets/images/product/1.jpg"
                                alt=""
                                className="img-fluid "
                              />
                            </div>
                            <div>
                              <img
                                src="assets/images/product/2.jpg"
                                alt=""
                                className="img-fluid "
                              />
                            </div>
                            <div>
                              <img
                                src="assets/images/product/27.jpg"
                                alt=""
                                className="img-fluid "
                              />
                            </div>
                            <div>
                              <img
                                src="assets/images/product/27.jpg"
                                alt=""
                                className="img-fluid "
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> */}
                    {/* slick slider */}
                    {/* <div
                      data-notify="container"
                      className="col-xs-11 col-sm-3 alert alert-info animated fadeInDown fadeOutUp"
                      role="alert"
                      data-notify-position="top-right"
                      style={styles.notify}
                      data-closing="true"
                    >
                      <button
                        type="button"
                        aria-hidden="true"
                        className="close"
                        data-notify="dismiss"
                        style={styles.notifyBtn}
                      >
                        ×
                      </button>
                      <span data-notify="icon" className="fa fa-check" />
                      <span data-notify="title">Success!</span>{' '}
                      <span data-notify="message">Item Successfully added in wishlist</span>
                      <div className="progress" data-notify="progressbar">
                        <div
                          className="progress-bar progress-bar-info"
                          role="progressbar"
                          aria-valuenow="120"
                          aria-valuemin="0"
                          aria-valuemax="100"
                          style={styles.progressBar}
                        />
                      </div>
                      <a href="#" target="_blank" data-notify="url" />
                    </div> */}
                    <div className="col-lg-6 rtl-text">
                      <div className="product-right">
                        <h2>{product.name}</h2>
                        <h4>
                          <del>${product?.pricing?.listPrice}</del>
                          {discount > 0 && <span>{`${discount}% off`} </span>}
                        </h4>
                        <h3>${product?.pricing?.salePrice}</h3>
                        {/* <ul className="color-variant">
                          <li className="bg-light0 active" />
                          <li className="bg-light1" />
                          <li className="bg-light2" />
                        </ul> */}
                        <div className="product-description border-product">
                          {/* <h6 className="product-title size-text">
                            select size
                            <span>
                              <a href="" data-toggle="modal" data-target="#sizemodal">
                                size chart
                              </a>
                            </span>
                          </h6> */}
                          {/* <div
                            className="modal fade"
                            id="sizemodal"
                            tabIndex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalLabel"
                            aria-hidden="true"
                          >
                            <div className="modal-dialog modal-dialog-centered" role="document">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title" id="exampleModalLabel">
                                    Sheer Straight Kurta
                                  </h5>
                                  <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div className="modal-body">
                                  <img
                                    src="../assets/images/size-chart.jpg"
                                    alt=""
                                    className="img-fluid "
                                  />
                                </div>
                              </div>
                            </div>
                          </div> */}
                          {/* <div className="size-box">
                            <ul>
                              <li className="active">
                                <a href="javascript:void(0)">s</a>
                              </li>
                              <li>
                                <a href="javascript:void(0)">m</a>
                              </li>
                              <li>
                                <a href="javascript:void(0)">l</a>
                              </li>
                              <li>
                                <a href="javascript:void(0)">xl</a>
                              </li>
                            </ul>
                          </div> */}
                          <h6 className="product-title">quantity</h6>
                          <QtyInput
                            loading={cartLoading}
                            onInc={onIncQty}
                            onDec={onDecQty}
                            value={currentQty}
                          />
                        </div>
                        <div className="product-buttons">
                          <button
                            onClick={onIncQty}
                            type="button"
                            // data-toggle="modal"
                            // data-target="#addtocart"
                            className="btn btn-solid"
                          >
                            add to cart
                          </button>{' '}
                          <a href="#" className="btn btn-solid">
                            buy now
                          </a>
                        </div>
                        <div className="border-product">
                          <h6 className="product-title">product details</h6>
                          {product?.productExtraInfo?.description && (
                            <div
                              dangerouslySetInnerHTML={{
                                __html: product?.productExtraInfo?.description,
                              }}
                            />
                          )}
                        </div>
                        <div className="border-product">
                          <h6 className="product-title">share it</h6>
                          <div className="product-icon">
                            <ul className="product-social">
                              <li>
                                <a href="#">
                                  <i className="fa fa-facebook" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="fa fa-google-plus" />
                                </a>
                              </li>

                              <li>
                                <a href="#">
                                  <i className="fa fa-twitter" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="fa fa-instagram" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="fa fa-rss" />
                                </a>
                              </li>
                            </ul>
                            <form className="d-inline-block">
                              <button type="button" className="wishlist-btn">
                                <i className="fa fa-heart" />
                                <span className="title-font">Add To WishList</span>
                              </button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <Tabs>
                  <TabsPanel id="description" label="Description">
                    {product?.productExtraInfo?.description && (
                      <div
                        dangerouslySetInnerHTML={{ __html: product?.productExtraInfo?.description }}
                      />
                    )}
                  </TabsPanel>
                  <TabsPanel id="details" label="Details">
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      Lorem Ipsum has been the industry&aquot;s standard dummy text ever since the
                      1500s, when an unknown printer took a galley of type and scrambled it to make
                      a type specimen book. It has survived not only five centuries, but also the
                      leap into electronic typesetting, remaining essentially unchanged. It was
                      popularised in the 1960s with the release of Letraset sheets containing Lorem
                      Ipsum passages, and more recently with desktop publishing software like Aldus
                      PageMaker including versions of Lorem Ipsum.
                    </p>
                    <div className="single-product-tables">
                      <table>
                        <tbody>
                          <tr>
                            <td>Febric</td>
                            <td>Chiffon</td>
                          </tr>
                          <tr>
                            <td>Color</td>
                            <td>Red</td>
                          </tr>
                          <tr>
                            <td>Material</td>
                            <td>Crepe printed</td>
                          </tr>
                        </tbody>
                      </table>
                      <table>
                        <tbody>
                          <tr>
                            <td>Length</td>
                            <td>50 Inches</td>
                          </tr>
                          <tr>
                            <td>Size</td>
                            <td>S, M, L .XXL</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </TabsPanel>
                  <TabsPanel id="video" label="Video">
                    <div className="mt-3 text-center">
                      <iframe
                        title="product video"
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/BUWzX78Ye_8"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                      />
                    </div>
                  </TabsPanel>
                  {user.isLogged ? (
                    <TabsPanel id="write-review" label="Write review">
                      <ReviewForm onSubmit={onSubmitReview} />
                    </TabsPanel>
                  ) : null}
                </Tabs>
                {/* <section className="tab-product m-0">
                  <div className="row">
                    <div className="col-sm-12 col-lg-12">
                      <ul className="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <li className="nav-item">
                          <a
                            className="nav-link active"
                            id="top-home-tab"
                            data-toggle="tab"
                            href="#top-home"
                            role="tab"
                            aria-selected="true"
                          >
                            <i className="icofont icofont-ui-home" />
                            Description
                          </a>
                          <div className="material-border" />
                        </li>
                        <li className="nav-item">
                          <a
                            className="nav-link"
                            id="profile-top-tab"
                            data-toggle="tab"
                            href="#top-profile"
                            role="tab"
                            aria-selected="false"
                          >
                            <i className="icofont icofont-man-in-glasses" />
                            Details
                          </a>
                          <div className="material-border" />
                        </li>
                        <li className="nav-item">
                          <a
                            className="nav-link"
                            id="contact-top-tab"
                            data-toggle="tab"
                            href="#top-contact"
                            role="tab"
                            aria-selected="false"
                          >
                            <i className="icofont icofont-contacts" />
                            Video
                          </a>
                          <div className="material-border" />
                        </li>
                        <li className="nav-item">
                          <a
                            className="nav-link"
                            id="review-top-tab"
                            data-toggle="tab"
                            href="#top-review"
                            role="tab"
                            aria-selected="false"
                          >
                            <i className="icofont icofont-contacts" />
                            Write Review
                          </a>
                          <div className="material-border" />
                        </li>
                      </ul>
                      <div className="tab-content nav-material" id="top-tabContent">
                        <div
                          className="tab-pane fade show active"
                          id="top-home"
                          role="tabpanel"
                          aria-labelledby="top-home-tab"
                        >
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting
                            industry. Lorem Ipsum has been the industry's standard dummy text ever
                            since the 1500s, when an unknown printer took a galley of type and
                            scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged. It was popularised in the 1960s with the release
                            of Letraset sheets containing Lorem Ipsum passages, and more recently
                            with desktop publishing software like Aldus PageMaker including versions
                            of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            text ever since the 1500s, when an unknown printer took a galley of type
                            and scrambled it to make a type specimen book. It has survived not only
                            five centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged. It was popularised in the 1960s with the release
                            of Letraset sheets containing Lorem Ipsum passages, and more recently
                            with desktop publishing software like Aldus PageMaker including versions
                            of Lorem Ipsum.
                          </p>
                        </div>
                        <div
                          className="tab-pane fade"
                          id="top-profile"
                          role="tabpanel"
                          aria-labelledby="profile-top-tab"
                        >
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting
                            industry. Lorem Ipsum has been the industry&aquot;s standard dummy text
                            ever since the 1500s, when an unknown printer took a galley of type and
                            scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged. It was popularised in the 1960s with the release
                            of Letraset sheets containing Lorem Ipsum passages, and more recently
                            with desktop publishing software like Aldus PageMaker including versions
                            of Lorem Ipsum.
                          </p>
                          <div className="single-product-tables">
                            <table>
                              <tbody>
                                <tr>
                                  <td>Febric</td>
                                  <td>Chiffon</td>
                                </tr>
                                <tr>
                                  <td>Color</td>
                                  <td>Red</td>
                                </tr>
                                <tr>
                                  <td>Material</td>
                                  <td>Crepe printed</td>
                                </tr>
                              </tbody>
                            </table>
                            <table>
                              <tbody>
                                <tr>
                                  <td>Length</td>
                                  <td>50 Inches</td>
                                </tr>
                                <tr>
                                  <td>Size</td>
                                  <td>S, M, L .XXL</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div
                          className="tab-pane fade"
                          id="top-contact"
                          role="tabpanel"
                          aria-labelledby="contact-top-tab"
                        >
                          <div className="mt-3 text-center">
                            <iframe
                              title="product video"
                              width="560"
                              height="315"
                              src="https://www.youtube.com/embed/BUWzX78Ye_8"
                              allow="autoplay; encrypted-media"
                              allowFullScreen
                            />
                          </div>
                        </div>
                        <div
                          className="tab-pane fade"
                          id="top-review"
                          role="tabpanel"
                          aria-labelledby="review-top-tab"
                        >
                          <form className="theme-form">
                            <div className="form-row">
                              <div className="col-md-12">
                                <div className="media">
                                  <label>Rating</label>
                                  <div className="media-body ml-3">
                                    <div className="rating three-star">
                                      <i className="fa fa-star" /> <i className="fa fa-star" />{' '}
                                      <i className="fa fa-star" /> <i className="fa fa-star" />{' '}
                                      <i className="fa fa-star" />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="name">Name</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  id="name"
                                  placeholder="Enter Your name"
                                  required
                                />
                              </div>
                              <div className="col-md-6">
                                <label htmlFor="email">Email</label>
                                <input
                                  id="email"
                                  type="text"
                                  className="form-control"
                                  placeholder="Email"
                                  required
                                />
                              </div>
                              <div className="col-md-12">
                                <label htmlFor="review">Review Title</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder="Enter your Review Subjects"
                                  required
                                />
                              </div>
                              <div className="col-md-12">
                                <label htmlFor="review">Review Title</label>
                                <textarea
                                  className="form-control"
                                  placeholder="Wrire Your Testimonial Here"
                                  id="exampleFormControlTextarea1"
                                  rows="6"
                                />
                              </div>
                              <div className="col-md-12">
                                <button className="btn btn-solid" type="submit">
                                  Submit YOur Review
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </section> */}
              </div>
              <div className="col-sm-3 collection-filter">
                {/* side-bar single product slider start  */}
                <div className="theme-card">
                  <h5 className="title-border">new product</h5>
                  <div className="offer-slider slide-1">
                    <div>
                      <div className="media">
                        <a href="">
                          <img className="img-fluid " src="../assets/images/product/1.jpg" alt="" />
                        </a>
                        <div className="media-body align-self-center">
                          <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                          </a>
                          <h4>$500.00</h4>
                        </div>
                      </div>
                      <div className="media">
                        <a href="">
                          <img className="img-fluid " src="../assets/images/product/2.jpg" alt="" />
                        </a>
                        <div className="media-body align-self-center">
                          <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                          </a>
                          <h4>$500.00</h4>
                        </div>
                      </div>
                      <div className="media">
                        <a href="">
                          <img className="img-fluid " src="../assets/images/product/3.jpg" alt="" />
                        </a>
                        <div className="media-body align-self-center">
                          <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                          </a>
                          <h4>$500.00</h4>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div className="media">
                        <a href="">
                          <img className="img-fluid " src="../assets/images/product/1.jpg" alt="" />
                        </a>
                        <div className="media-body align-self-center">
                          <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                          </a>
                          <h4>$500.00</h4>
                        </div>
                      </div>
                      <div className="media">
                        <a href="">
                          <img className="img-fluid " src="../assets/images/product/2.jpg" alt="" />
                        </a>
                        <div className="media-body align-self-center">
                          <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                          </a>
                          <h4>$500.00</h4>
                        </div>
                      </div>
                      <div className="media">
                        <a href="">
                          <img className="img-fluid " src="../assets/images/product/3.jpg" alt="" />
                        </a>
                        <div className="media-body align-self-center">
                          <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                          </a>
                          <h4>$500.00</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* side-bar single product slider end  */}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div>
          {product.relatedProducts?.length > 0 && (
            <Slider {...relatedSliderSettings}>
              {product.relatedProducts.map(i => (
                <Product
                  slug={i.slug}
                  key={i._id}
                  id={i._id}
                  title={i.name}
                  description={i.textDescription}
                  listPrice={i.pricing?.listPrice}
                  salePrice={i.pricing?.salePrice}
                  img={i.images?.[0]?.thumbnail}
                />
              ))}
            </Slider>
          )}
        </div>
      </section>
    </>
  );
};

export default connect(({ user, cart }) => {
  console.log(cart);
  return { user, cartLoading: cart.loading, cartProducts: cart.products };
})(ProductDetailPage);
