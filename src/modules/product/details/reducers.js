import { productActions as actions } from './actions'

const initialState = {
  product: {},
  productSuccess:{},
  productError:{},
  loading: false
}

export default function productReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}
