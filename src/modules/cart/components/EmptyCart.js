import React from 'react';
import { Link } from 'react-router-dom';

const EmptyCart = () => {
  return (
    <div className="inner-page-wrapper">
      <div className="container">
        <div>
          <p>
            Your cart is <strong>empty!</strong>{' '}
          </p>
          <p>
            <Link to="/products">Click here to add some products!</Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default EmptyCart;
