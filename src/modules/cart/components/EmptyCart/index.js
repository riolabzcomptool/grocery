import React from 'react';
import { Link } from 'react-router-dom';
import Button from 'components/Button';
import shoppingCart from 'assets/images/shopping-cart.svg'
import styles from './styles.module.scss'

const EmptyCart = () => {
  return (
    // <div className="inner-page-wrapper">
    //   <div className="container">
    //     <div>
    //       <p>
    //         Your cart is <strong>empty!</strong>{' '}
    //       </p>
    //       <p>
    //         <Link to="/products">Click here to add some products!</Link>
    //       </p>
    //     </div>
    //   </div>
    // </div>
    <div className={styles.wishlistErrorContainer}>
      <div className={styles.header}>
        <h2 className="login-head">Your wishlist is empty!</h2>
      </div>
      <div className={styles.image}>
        <img src={shoppingCart} alt="shopping cart" />
      </div>
      <div className={styles.subtitle}>
        <h5 className="login-para">You have not saved anything yet!</h5>
      </div>
      <div className={styles.loginBtn}>
        <Link to="/">
          <Button>Start shopping</Button>
        </Link>
      </div>
    </div>
  );
};

export default EmptyCart;
