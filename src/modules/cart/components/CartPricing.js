import React from 'react';
import { connect } from 'react-redux';

const discountStyle = {
  color: 'green',
};

const marginRight = {
  marginRight: '5px',
};

const CartPricing = props => {
  // const { total, discount, shippingCost, cartAmount, cart } = props;
  const { cart, title } = props;
  console.log(cart);

  const total = cart.products.reduce(
    (sum, item) => sum + Number(item.listPrice) * Number(item.qty),
    0,
  );
  const discount = cart.products.reduce(
    (sum, item) => sum + (Number(item.listPrice) - Number(item.salePrice)) * item.qty,
    0,
  );
  // const shippingCost = 75;
  // const cartAmount = total - discount + shippingCost;
  const cartAmount = total - discount ;

  return (
    <>
      <div className="price-details-section">
        {title && <h4 className="price-detail-head">{title}</h4>}
        <div className="price-list-row">
          <span>MRP Total </span>
          <span>
            <i className="fas fa-rupee-sign" /> <strong>{total}</strong>
          </span>
        </div>
        <div className="price-list-row">
          <span>Price Discount </span>

          <span style={discount > 0 ? discountStyle : {}}>
            {discount > 0 && <i className="fas fa-minus" style={marginRight} />}
            <i className="fas fa-rupee-sign" /> <strong>{discount}</strong>
          </span>
        </div>

        {/* <div className="price-list-row">
          <span>Shipping Charges </span>
          <span>
            <i className="fas fa-rupee-sign" /> <strong>{shippingCost}</strong>
          </span>
        </div> */}
        <div className="tobe-paid">
          <span>To be paid</span>
          <span>
            <i className="fas fa-rupee-sign" /> <strong>{cartAmount}</strong>
          </span>
        </div>
      </div>
      {/* <div className="total-savings">
        Total Savings <i className="fas fa-rupee-sign" />{' '}
        <strong>{Math.floor(total - cartAmount)}</strong>
      </div> */}
    </>
  );
};

// CartPricing.propTypes = {
//   total: PropTypes.number,
//   discount: PropTypes.number,
//   shippingCost: PropTypes.number,
//   cartAmount: PropTypes.number,
// };

// CartPricing.defaultProps = {
//   total: 0,
//   discount: 0,
//   shippingCost: 0,
//   cartAmount: 0,
// };

CartPricing.defaultProps = {
  title: 'Price details',
};

export default connect(({ cart }) => ({ cart }))(CartPricing);
