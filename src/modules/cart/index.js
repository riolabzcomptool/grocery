/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';
// import { Transition } from 'react-spring/renderprops';
// import PageSection from 'components/PageSection';
import find from 'lodash/find';
import groupBy from 'lodash/groupBy';
// import { getProductsFromIds } from 'services/products';
import { cartActions } from 'redux/actions';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
// import shoppingCart from 'assets/images/shopping-cart.svg';

// import Placeholder from 'components/Placeholder';
// import CartItem from './components/CartItem';
// import CartPricing from './components/CartPricing';

// import data from './data.json';

class Cart extends Component {
  state = {
    // products: this.props.cart.products,
    // total: 0,
    // discount: 0,
    // shippingCost: 0,
    // cartAmount: 0,
    // loading: false,
  };

  onRemove = id => {
    console.log('in onRemove', id);
    // console.log(id);
    const { dispatch } = this.props;
    return () => {
      dispatch({
        type: cartActions.REMOVE_FROM_CART,
        payload: { productId: id },
      });
    };
  };

  onIncQuantity = id => {
    const { dispatch, cart } = this.props;
    const { products } = cart;
    return () => {
      const selectedProc = find(products, { productId: id });
      console.log('onIncQuantity', selectedProc, products);
      if (selectedProc)
        dispatch({
          type: cartActions.INC_QTY,
          payload: {
            productId: id,
            maxOrderQty: selectedProc.maxOrderQty,
          },
        });
    };
  };

  onDecQuantity = id => {
    const { dispatch, cart } = this.props;
    const { products } = cart;
    return () => {
      const selectedProc = find(products, { productId: id });
      if (selectedProc)
        dispatch({
          type: cartActions.DEC_QTY,
          payload: {
            productId: id,
            minOrderQty: selectedProc.minOrderQty,
          },
        });
    };
  };

  render() {
    const { cart } = this.props;
    const {
      products,
      // , loading
    } = cart;
    console.log(products);
    let total = 0;
    let discount = 0;
    let cartAmount = 0;
    const shippingCost = 0;
    if (products && products.length > 0) {
      console.log('products', products);
      total = products.reduce((sum, item) => sum + item.listPrice * item.qty, 0);
      discount = products.reduce(
        (sum, item) => sum + (item.listPrice - item.salePrice) * item.qty,
        0,
      );
      // shippingCost = 75;
      cartAmount = total + shippingCost - discount;
    }
    const prescProds = groupBy(products, 'prescriptionNeeded');
    console.log(prescProds);

    return (
      <>
        <Helmet>
          <title>My Cart</title>
        </Helmet>
        <section className="cart-section section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <table className="table cart-table table-responsive-xs striped-table">
                  <thead>
                    <tr className="table-head">
                      <th scope="col">image</th>
                      <th scope="col">product name</th>
                      <th scope="col">price</th>
                      <th scope="col">quantity</th>
                      <th scope="col">action</th>
                      <th scope="col">total</th>
                    </tr>
                  </thead>
                  <tbody>
                    {products &&
                      products.length > 0 &&
                      products.map(item => {
                        console.log('jajaja', item);
                        return (
                          <tr key={item.id}>
                            <td>
                              <Link to={`/product/${item.slug}`} aria-label={item.name}>
                                <img src={item.thumbImg} alt="" />
                              </Link>
                            </td>
                            <td>
                              <Link to={`/product/${item.slug}`} aria-label={item.name}>
                                {item.name}
                              </Link>
                              <div className="mobile-cart-content row">
                                <div className="col-xs-3">
                                  <div className="qty-box">
                                    <div className="input-group">
                                      <input
                                        readOnly
                                        type="text"
                                        name="quantity"
                                        className="form-control input-number"
                                        value={item.qty}
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="col-xs-3">
                                  <h2 className="td-color">${item.salePrice || item.listPrice}</h2>
                                </div>
                                <div className="col-xs-3">
                                  <h2 className="td-color">
                                    <button
                                      type="button"
                                      aria-label="close"
                                      title="Remove"
                                      onClick={this.onRemove(item.productId)}
                                      className="icon"
                                    >
                                      <i className="fa fa-times" />
                                    </button>
                                  </h2>
                                </div>
                              </div>
                            </td>
                            <td>
                              <h2>${item.salePrice || item.listPrice}</h2>
                            </td>
                            <td>
                              <div className="qty-box">
                                <div className="input-group">
                                  <span className="input-group-prepend">
                                    <button
                                      aria-label="deduct"
                                      type="button"
                                      className="btn quantity-left-minus"
                                      title="deduct"
                                      data-type="minus"
                                      data-field=""
                                      onClick={this.onDecQuantity(item.productId)}
                                    >
                                      <i className="fa fa-angle-left" />
                                    </button>{' '}
                                  </span>
                                  <input
                                    readOnly
                                    type="text"
                                    name="quantity"
                                    className="form-control input-number"
                                    value={item.qty}
                                  />{' '}
                                  <span className="input-group-prepend">
                                    <button
                                      title="add"
                                      aria-label="add"
                                      type="button"
                                      className="btn quantity-right-plus"
                                      data-type="plus"
                                      data-field=""
                                      onClick={this.onIncQuantity(item.productId)}
                                    >
                                      <i className="fa fa-angle-right" />
                                    </button>
                                  </span>
                                </div>
                              </div>
                            </td>
                            <td>
                              <button
                                onClick={this.onRemove(item.productId)}
                                type="button"
                                title="Remove"
                                aria-label="close"
                                className="icon"
                              >
                                <i className="fa fa-times" />
                              </button>
                            </td>
                            <td>
                              <h2 className="td-color">
                                ${(item.salePrice || item.listPrice) * item.qty}
                              </h2>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>
                <table className="table cart-table table-responsive-md">
                  <tfoot>
                    <tr>
                      <td>total price :</td>
                      <td>
                        <h2>${cartAmount}</h2>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div className="row cart-buttons">
              <div className="col-6">
                <a aria-label="" href="#" className="btn btn-solid">
                  continue shopping
                </a>
              </div>
              <div className="col-6">
                <a aria-label="" href="#" className="btn btn-solid">
                  check out
                </a>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default connect(({ cart }) => ({ cart }))(Cart);
