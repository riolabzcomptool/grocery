import { blogDetailsActions as actions } from './actions'

const initialState = {
  blog: [],
  loading: false,
  fetchError: null,
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE: {
      console.log('blog payload', action.payload)
      return { ...state, ...action.payload }
    }
    default:
      return state
  }
}
