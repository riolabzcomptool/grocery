import { all, put, call, takeEvery } from 'redux-saga/effects'
import { getBlog } from 'services/blogs'
import { blogDetailsActions } from './actions'

export function* GET_DATA(actionObject) {
  const {
    payload: { id },
  } = actionObject
  console.log('in blogs get data')
  const blog = yield call(getBlog, id)
  console.log(blog)
  yield put({
    type: blogDetailsActions.SET_STATE,
    payload: {
      blog,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(blogDetailsActions.GET_DATA, GET_DATA),
    // GET_DATA(), // run once on app load to fetch menu data
  ])
}
