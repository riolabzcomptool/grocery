import { blogActions as actions } from './actions'

const initialState = {
  blogs: [],
  loading: false,
  fetchError: null,
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE: {
      console.log('blogs payload', action.payload)
      return { ...state, ...action.payload }
    }
    default:
      return state
  }
}
