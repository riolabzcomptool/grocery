import React from 'react';
import { useBlogs } from 'hooks/api';
import List from './List';

const Index = props => {
  // const searchStr = '';
  console.log('PRODUCT LIST PROPS', props);
  const { match, location } = props;
  const { params } = match;
  const { type, id } = params;

  const { state } = location;

  let title = null;
  if (state && state.title) title = state.title;

  React.useEffect(() => {
    console.log('title changes', title);
  }, [title]);

  let options = {};
  if (type && id) options = { [type]: id };
  if (type && !id) options = { [type]: true };
  console.log('product fetch options', options);
  const { data, isFetching } = useBlogs(options);
  console.log(data, isFetching);

  return (
    
    <div className="col">
      <div className="flex-wrapper products-wrapper">
        {data && data.data && <List data={data && data.data} title="" />}
      </div>
    </div>
    
  );
};

export default Index;
