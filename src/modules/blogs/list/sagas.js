import { all, put, call, takeLatest } from 'redux-saga/effects'
import { getBlogsList } from 'services/blogs'
import { blogActions } from './actions'

export function* GET_DATA() {
  console.log('in blogs get data')
  const blogs = yield call(getBlogsList)
  console.log(blogs)
  yield put({
    type: blogActions.SET_STATE,
    payload: {
      blogs,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeLatest(blogActions.GET_DATA, GET_DATA),
    // GET_DATA(), // run once on app load to fetch menu data
  ])
}
