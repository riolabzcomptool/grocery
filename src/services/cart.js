import 'toasted-notes/src/styles.css';

import callApi from '_utils/callApi';
import { CATALOG_API } from '_constants/api';
import { getFormData } from '_utils';
import toaster from 'toasted-notes';

// productId, quantity
export async function addMultipleToCart(products) {
  try {
    const jsonData = JSON.stringify({
      products: products.map(i => ({ productId: i.productId, quantity: i.qty })),
    });
    const options = {
      method: 'POST',
      body: jsonData,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const a = await callApi(CATALOG_API.addMultipleToCart, options);
    if (a && a.data) return { products: a.data };
  } catch (error) {
    console.log(error);
    toaster.notify(error.message);
    return { error: error.message };
  }
  return {};
}

export async function getCart() {
  try {
    const a = await callApi(CATALOG_API.getCart);
    if (a && a.cartlistDetails) return { products: a.cartlistDetails };
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
  return {};
}

export async function addToCart(productId, qty) {
  try {
    const formData = getFormData({ productId, quantity: qty });
    const options = {
      method: 'POST',
      body: formData,
    };
    const a = await callApi(CATALOG_API.addToCart, options);
    if (a && a.data) return { products: a.data };
  } catch (error) {
    console.log(error);
    toaster.notify(error.message, {position:'top-right'});
    return { error: error.message };
  }
  return {};
}

export async function removeFromCart(productId) {
  try {
    const formData = getFormData({ productId });
    const options = {
      method: 'POST',
      body: formData,
    };
    const a = await callApi(CATALOG_API.removeFromCart, options);
    if (a && a.data) return { products: a.data };
  } catch (error) {
    console.log(error);
    toaster.notify(error.message);
    return { error: error.message };
  }
  return {};
}
