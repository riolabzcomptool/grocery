import { CATALOG_API } from '_constants/api';

import toaster from 'toasted-notes';
import callApi from '_utils/callApi';

export async function getBrands(options = {}) {
  try {
    let url = `${CATALOG_API.getBrands}?status=active&sort=priorityOrder`;
    Object.entries(options).forEach(([key, value]) => {
      switch (key) {
        case 'fields':
          value.forEach(i => {
            url += `&field[]=${i}`;
          });
          break;
        default:
          url += `&${key}=${value}`;
          break;
      }
    });
    const res = await callApi(url);
    console.log('HII', res);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.error(err);
    toaster.notify(err.message);
    return null;
  }
}
