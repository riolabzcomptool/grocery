import { userActions } from 'redux/actions'

export async function getProfileMenuData() {
  return [
    {
      title: 'My profile',
      key: 'profile',
      icon: 'fa fa-user-circle fa-fw',
      url: '/user/user-profile',
    },
    {
      title: 'Orders',
      key: 'orders',
      url: '/orders',
      icon: 'fa fa-paper-plane fa-fw',
    },
    {
      title: 'Wishlist',
      key: 'wishlist',
      url: '/wishlist',
      icon: 'fa fa-star fa-fw',
    },
    {
      title: 'Notification',
      key: 'notification',
      url: '/notifications',
      icon: 'fa fa-exclamation fa-fw',
    },
    {
      divider: true,
    },
    {
      title: 'Logout',
      key: 'logout',
      action: userActions.LOGOUT,
      icon: 'fa fa-power-off fa-fw',
    },
  ]
}

export async function getTopMenuData() {
  return []
}

export async function getLeftMenuData() {
  return []
}
