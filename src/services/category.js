import { CATALOG_API } from '_constants/api';

import toaster from 'toasted-notes';
import callApi from '_utils/callApi';

export async function getCategories(options = {}) {
  try {
    let queryStr = '?status=active&sort[priorityOrder]=1';
    Object.entries(options).forEach(([key, value]) => {
      switch (key) {
        case 'fields':
          value.forEach(i => {
            queryStr += `&field[]=${i}`;
          });
          break;
        default:
          queryStr += `&${key}=${value}`;
          break;
      }
    });
    const url = `${CATALOG_API.getCategories}${queryStr}`;

    const res = await callApi(url);
    console.log('HII', res);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }
}
