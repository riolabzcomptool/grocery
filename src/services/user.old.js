// import decode from 'jwt-decode';
import { api } from '_constants';
import toaster from 'toasted-notes';
import 'toasted-notes/src/styles.css';
import axios from 'axios';
import { printResponse, printError } from '_utils';

export async function register(request) {
  console.log(request);
  return axios.post(api.signup, request).then(
    response => {
      printResponse(response);

      if (response.status === 200 || response.status === 201) {
        toaster.notify('Registered successfully!');
        return true;
      }
      toaster.notify(`Error registering: ${response.data.message || response.data.error}`);
      return null;
    },
    error => {
      printError(error);
      const { response } = error;
      const { data } = response;
      toaster.notify(`Error registering: ${data.error || data.message}`);
      return null;
    },
  );
}

export async function login(email, password) {
  console.log('login');
  const loginData = {
    email,
    password,
  };

  return fetch(api.login, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(loginData),
  })
    .then(loginResponse => {
      console.log(loginResponse);
      if (loginResponse.status === 200) return loginResponse.json();
      throw new Error(loginResponse.status);
    })
    .then(resJSON => {
      console.log(resJSON);
      setToken(resJSON.token);
      toaster.notify('You are now logged in!');
      return true;
    })
    .catch(error => {
      console.log(error);
      toaster.notify(`Error logging in`);
      return false;
    });
}

const setToken = token => {
  localStorage.setItem('token', token);
};

const getToken = name => {
  return localStorage.getItem(name);
};

const removeToken = name => {
  localStorage.removeItem(name);
};

// const isTokenExpired = token => {
//   console.log('checking token expiry');
//   const now = Date.now().valueOf() / 1000;
//   console.log(token.exp, now);
//   if (token.exp < now) return true;
//   return false;
// };

export async function currentAccount() {
  const token = getToken('token');
  console.log('in currentAccount() service');
  console.log(token);
  try {
    if (token) {
      return fetch(api.getUser, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(json => {
          console.log(json);
          if (json.userId) return json;
          if ((json.error) === "jwt expired") throw new Error("Session expired! Please login again")
          throw new Error();
        })
        .catch(err => {
          console.log(err);
          if (err.message) toaster.notify(err.message);
          removeToken('token'); return null
        })



      // const decodedToken = decode(token);
      // console.log(decodedToken);
      // if (!isTokenExpired(decodedToken)) {
      //   console.log('token not expired');
      //   const { email, userId, role, phone, name } = decodedToken;
      //   return {
      //     email,
      //     id: userId,
      //     role,
      //     phoneNo: phone,
      //     name,
      //   };
      // }
      // console.log('token expired');
      // toaster.notify('Session expired! Please sign in again');
      // logout();
    }
  } catch (err) {
    console.log(err);
    return null;
  }
  return null;
}

export async function logout() {
  toaster.notify('Logged out!');
  removeToken('token');
}

// export async function login(email, password) {
//   console.log(email, password)
//   const requestBody = {
//     email,
//     password
//   }
//   fetch()
//   const promise = new Promise(resolve =>
//     setTimeout(() => {
//       if (email === 'sumixavier@gmail.com' && password === '123abcdQ') {
//         resolve(true)
//       } else resolve(false)
//     }, 2000),
//   )
//   return promise
// }
