import toaster from 'toasted-notes'
import 'toasted-notes/src/styles.css'
// import fetch from '_utils/fetch';
import { BACKEND_API } from '_constants/api';
import callApi from '_utils/callApi';




export async function getCoupen (id) {
  try {
    const url = `${BACKEND_API.getCoupen}/${id}`;

    const res = await callApi(url);
    console.log('HII',res)
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }

}
