import toaster from 'toasted-notes';
import 'toasted-notes/src/styles.css';
import { BACKEND_API } from '_constants/api';
import callApi from '_utils/callApi';
import { getFormDataA } from '_utils';

export async function setOrder(dataOrder) {
  console.log('OOK', dataOrder);
  // const { payment,coupen, shippingId,billingId, prescriptions } = dataOrder;
  // const formData = new FormData();
  // formData.append('payment', payment);
  // formData.append('coupen', coupen);
  // formData.append('shippingId', shippingId);
  // formData.append('billingId', billingId);
  const formData = getFormDataA(dataOrder, [
    'payment',
    'coupen',
    'shippingId',
    'billingId',
    'prescriptions',
  ]);
  const request = {
    method: 'POST',
    body: formData,
  };
  try {
    const url = `${BACKEND_API.getOrder}`;
    const res = await callApi(url, request);
    console.log('HII', res);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message || 'Error adding orders!');
  }
  return null;
}

export async function updateOrder(boltResponse) {
  const formData = new FormData();
  formData.append('paymentResponse', boltResponse);
  const request = {
    method: 'POST',
    body: formData,
  };
  try {
    const url = `${BACKEND_API.updateOrder}`;
    const res = await callApi(url, request);
    console.log('HII', res);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message || 'Error adding orders!');
  }
  return null;
}
