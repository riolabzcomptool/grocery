import callApi from '_utils/callApi';
import { CATALOG_API } from '_constants';
import { getFormData } from '_utils';

export async function createReview(data) {
  try {
    const formData = getFormData(data);
    const options = {
      method: 'POST',
      body: formData,
    };
    const res = await callApi(`${CATALOG_API.createReview}`, options);
    return res;
  } catch (error) {
    console.error(error);
    return { error: error.message };
  }
}

export async function editReview(data, reviewId) {
  try {
    const formData = getFormData(data);
    const options = {
      method: 'PATCH',
      body: formData,
    };
    const res = await callApi(`${CATALOG_API.editReview}/${reviewId}`, options);
    return res;
  } catch (error) {
    console.error(error);
    return { error: error.message };
  }
}

// function getFormData(data) {
//   const formData = new FormData();
//   formData.append('orderId', data.orderId);
//   formData.append('productId', data.productId);
//   formData.append('text', data.text);
//   formData.append('rating', data.rating);
//   formData.append('title', data.title);
//   return formData;
// }
