import callApi from '_utils/callApi';
import { CATALOG_API } from '_constants';

export async function getAddresses() {
  // const { user } = reduxStore.getState();
  try {
    const res = await callApi(`${CATALOG_API.getAddresses}`);
    if (res && res.address && res.address.length > 0) return res.address;
  } catch (error) {
    console.error(error);
  }
  return null;
}

export async function editAddress(data) {
  // const { user } = reduxStore.getState();
  try {
    const formData = getFormData(data);
    const options = {
      method: 'PATCH',
      body: formData,
    };
    const res = await callApi(`${CATALOG_API.getAddresses}/${data.id}`, options);
    if (res && res.address) return { address: res.address };
  } catch (error) {
    console.error(error);
    return { message: error.message };
  }
  return {};
}

export async function addAddress(data) {
  // const { user } = reduxStore.getState();
  try {
    const formData = getFormData(data);
    const options = {
      method: 'POST',
      body: formData,
    };
    const res = await callApi(`${CATALOG_API.getAddresses}/create`, options);
    if (res && res.address) return { address: res.address };
  } catch (error) {
    console.error(error);
    return { message: error.message };
  }
  return {};
}

export async function deleteAddress(id) {
  // const { user } = reduxStore.getState();
  try {
    const options = {
      method: 'DELETE',
    };
    const res = await callApi(`${CATALOG_API.getAddresses}/${id}`, options);
    if (res && res.success) return true;
  } catch (error) {
    return false;
  }
  return false;
}

function getFormData(data) {
  const formData = new FormData();
  formData.append('fullName', data.name);
  formData.append('mobileNo', data.phoneNo);
  formData.append('pincode', data.pincode);
  formData.append('houseNo', data.houseNo);
  formData.append('street', data.street);
  formData.append('landmark', data.landmark);
  formData.append('city', data.city);
  formData.append('state', data.state);
  formData.append('address_type', data.addressType);
  formData.append('active', 'active');
  return formData;
}
