import { BACKEND_API } from '_constants/api';

import toaster from 'toasted-notes';
import callApi from '_utils/callApi';

export async function getPaymentMethod() {
  try {
    const url = `${BACKEND_API.paymentMethod}`;
    const res = await callApi(url);
    console.log('HII', res);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.error(err);
    toaster.notify(err.message);
    return null;
  }
}
