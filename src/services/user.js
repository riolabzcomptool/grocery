// import decode from 'jwt-decode';

import toaster from 'toasted-notes';
import 'toasted-notes/src/styles.css';
import store from 'store';
import callApi from '_utils/callApi';
import { CATALOG_API } from '_constants/api';
import { getFormData } from '_utils';

export async function getWishlist() {
  try {
    const a = await callApi(CATALOG_API.getWishlist);
    if (a && a.wishlist && a.wishlist.products) return { products: a.wishlist.products };
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
  return {};
}
export async function addToWishlist(id) {
  try {
    const options = {
      method: 'POST',
      body: getFormData({ productId: id }),
    };
    const a = await callApi(CATALOG_API.addToWishlist, options);
    if (a && a.wishlist && a.wishlist.products) {
      toaster.notify('Added to wishlist');
      return { products: a.wishlist.products };
    }
  } catch (error) {
    console.log(error);
    toaster.notify('Error adding to wishlist. ', error.message);
    return { error: error.message };
  }
  return {};
}
export async function remveFromWishlist(id) {
  try {
    const options = {
      method: 'DELETE',
      body: getFormData({ productId: id }),
    };
    const a = await callApi(CATALOG_API.removeFromWishlist, options);
    if (a && a.wishlist && a.wishlist.products) {
      toaster.notify('Removed from wishlist');
      return { products: a.wishlist.products };
    }
  } catch (error) {
    console.log(error);
    toaster.notify('Error removing from wishlist');
    return { error: error.message };
  }
  return {};
}

export async function register(data) {
  const {
    email,
    firstName,
    lastName,
    password, // phoneNo: phone
  } = data;
  const formData = new FormData();
  formData.append('email', email);
  formData.append('firstName', firstName);
  formData.append('lastName', lastName);
  formData.append('password', password);
  // formData.append('phone', phone);
  formData.append('roleId', 2);
  const request = {
    method: 'POST',
    body: formData,
  };
  try {
    const a = await callApi(CATALOG_API.signup, request);
    if (a && a.user) return { success: true };
  } catch (err) {
    toaster.notify(err.message || 'Error registering!');
    return { error: err.message, success: false };
  }
  return { success: false };
}

export async function login(email, password) {
  console.log('login', email, password);
  const formData = new FormData();
  formData.append('loginId', email);
  formData.append('password', password);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const response = await callApi(CATALOG_API.login, options);
    if (response && response.token) {
      setToken(response.token);
      toaster.notify('You are now logged in!');
      return { success: true };
    }
  } catch (error) {
    console.log('oye', error);

    toaster.notify(error.message);
    return { error: error.message };
  }
  return {};
}

export async function fblogin(email, id, name) {
  console.log('login', email, id, name);
  const formData = new FormData();
  formData.append('loginId', email);
  formData.append('id', id);
  formData.append('name', name);
  formData.append('roleId', 2);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const response = await callApi(CATALOG_API.fblogin, options);
    if (response && response.token) {
      setToken(response.token);
      return true;
    }
  } catch (error) {
    console.log(error);
    toaster.notify(error.message);
  }
  return false;
}

export async function gblogin(email, id, name) {
  console.log('login', email, id, name);
  const formData = new FormData();
  formData.append('loginId', email);
  formData.append('id', id);
  formData.append('name', name);
  formData.append('roleId', 2);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const response = await callApi(CATALOG_API.gblogin, options);
    if (response && response.token) {
      setToken(response.token);
      return true;
    }
  } catch (error) {
    console.log(error);
    toaster.notify(error.message);
  }
  return false;
}

const setToken = token => {
  store.set('token', token);
};

const getToken = () => {
  store.get('token');
};

const removeToken = () => {
  store.remove('token');
};

// const isTokenExpired = token => {
//   console.log('checking token expiry');
//   const now = Date.now().valueOf() / 1000;
//   console.log(token.exp, now);
//   if (token.exp < now) return true;
//   return false;
// };

export async function currentAccount() {
  const token = getToken();
  console.log('in currentAccount() service');
  console.log(token);
  try {
    const res = await callApi(CATALOG_API.getUser);
    if (res && res.user) return res.user;
  } catch (err) {
    console.error(err);
    toaster.notify(err.message, { duration: null });
    logout();
  }
  return null;
}

export async function forgotPassword({ email }) {
  const formData = new FormData();
  formData.append('email', email);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const res = await callApi(CATALOG_API.forgotPassword, options);
    if (res && res.success) return { success: true };
  } catch (err) {
    console.error(err);
    return { error: err.message, success: false };
    // toaster.notify(err.message, { duration: null });
  }
  return { success: false };
}

export async function resetPassword({ email, password, resetPasswordToken }) {
  const formData = new FormData();
  formData.append('email', email);
  formData.append('password', password);
  formData.append('resetPasswordToken', resetPasswordToken);
  const options = {
    method: 'PATCH',
    body: formData,
  };
  try {
    const res = await callApi(CATALOG_API.resetPassword, options);
    if (res && res.success) return { message: res.message, success: true };
  } catch (err) {
    console.error(err);
    return { message: err.message, success: false };
    // toaster.notify(err.message, { duration: null });
  }
  return { message: '', success: false };
}

/**
 * to verify current password for logged in user
 * @param {object} data
 * @param {string} data.currentPassword
 */
export async function verifyPassword(data) {
  const formData = getFormData(data);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const res = await callApi(CATALOG_API.verifyPassword, options);
    if (res && res.success) return { message: res.message, success: true };
  } catch (error) {
    return { message: error.message, success: false };
  }
  return { success: false, message: 'Error updating password' };
}

/**
 * to reset password for logged in user
 * @param {object} data
 * @param {string} data.newPassword new password
 * @param {string} data.confirmPassword new password
 */
export async function updatePassword(data) {
  const { newPassword, confirmPassword } = data;
  if (newPassword !== confirmPassword) return { success: false, message: `Passwords don't match` };
  const formData = getFormData({ password: newPassword });
  const options = {
    method: 'PATCH',
    body: formData,
  };
  try {
    const res = await callApi(CATALOG_API.updatePassword, options);
    if (res && res.success) return { message: res.message, success: true };
  } catch (error) {
    return { message: error.message, success: false };
  }
  return { success: false, message: 'Error updating password' };
}

export async function logout() {
  removeToken('token');
  toaster.notify('Logged out!');
}

export async function updateProfile(data) {
  const formData = getFormData(data);
  const options = {
    body: formData,
    method: 'PATCH',
  };
  try {
    const updated = await callApi(CATALOG_API.updateProfile, options);
    if (updated && updated.success) return { user: updated.user };
  } catch (error) {
    return { error: error.message };
  }
  return {};
}

export async function sendOtp(data) {
  const formData = getFormData({ phone: data.phone });
  const options = {
    body: formData,
    method: 'POST',
  };
  try {
    const updated = await callApi(CATALOG_API.sendOtp, options);
    if (updated && updated.success) return { success: true };
  } catch (error) {
    return { error: error.message };
  }
  return { error: 'Error sending OTP' };
}

export async function verifyOtp(data) {
  const formData = getFormData({ phone: data.phone, otp: data.otp });
  const options = {
    body: formData,
    method: 'POST',
  };
  try {
    const updated = await callApi(CATALOG_API.verifyOtp, options);
    if (updated && updated.success) return { success: true };
  } catch (error) {
    return { error: error.message };
  }
  return { error: 'Error verifying OTP' };
}

// export async function login(email, password) {
//   console.log(email, password)
//   const requestBody = {
//     email,
//     password
//   }
//   fetch()
//   const promise = new Promise(resolve =>
//     setTimeout(() => {
//       if (email === 'sumixavier@gmail.com' && password === '123abcdQ') {
//         resolve(true)
//       } else resolve(false)
//     }, 2000),
//   )
//   return promise
// }
