import callApi from '_utils/callApi';
import { CATALOG_API } from '_constants';
import { store as reduxStore } from 'storeConfig';
import difference from 'lodash/difference';

export async function uploadPrescriptions({ fileList }) {
  const formData = new FormData();
  fileList.forEach(i => {
    formData.append('prescription', i.file);
  });
  try {
    const res = await callApi(CATALOG_API.uploadPrescription, {
      body: formData,
      method: 'POST',
    });
    if (res.success) return { prescriptions: res.prescriptions };
    return { error: res.message };
  } catch (error) {
    if (error.message === 'Unexpected field') return { error: 'Max upload count: 5' };
    console.error(error);
  }
  return { error: 'Error uploading. Please try again later' };
}

export async function updatePrescriptions({ fileList, origFileList }) {
  const deletedFiles = difference(origFileList, fileList);
  const newFiles = fileList.filter(i => i.file && true);
  const formData = new FormData();
  console.log('deletedFiles', deletedFiles);
  console.log('newFiles', newFiles);
  deletedFiles.forEach(i => formData.append('deleted[]', i.id || i.uid));
  newFiles.forEach(i => formData.append('prescription', i.file));
  try {
    const res = await callApi(CATALOG_API.updatePrescriptions, {
      body: formData,
      method: 'PATCH',
    });
    if (res.success) return { prescriptions: res.prescriptions };
    return { error: res.message };
  } catch (error) {
    if (error.message === 'Unexpected field') return { error: 'Max upload count: 5' };
    console.error(error);
  }
  return { error: 'Error updating. Please try again later' };
}

export async function deletePrescription(id) {
  const formData = new FormData();
  formData.append('deleted[]', id);
  try {
    const res = await callApi(CATALOG_API.updatePrescriptions, {
      body: formData,
      method: 'PATCH',
    });
    if (res.success) return { success: true };
    return { error: res.message };
  } catch (error) {
    if (error.message === 'Unexpected field') return { error: 'Max upload count: 5' };
    console.error(error);
  }
  return { error: 'Error updating. Please try again later' };
}

export async function getPrescriptions() {
  const { user } = reduxStore.getState();
  try {
    if (user.id) {
      const res = await callApi(`${CATALOG_API.getPrescriptions}`);
      if (res && res.prescriptions && res.prescriptions.length > 0) return res.prescriptions;
    }
  } catch (error) {
    console.error(error);
  }
  return null;
}
