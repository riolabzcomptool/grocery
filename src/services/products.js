import { api, CATALOG_API } from '_constants/api';
import axios from 'axios';
import toaster from 'toasted-notes';
import fetch from '_utils/fetch';

// const url = `${api.getProducts}/${id}`;
// const options = {
//     method: 'GET',
// }
// fetch(url, options)
//     .then(res => res.json())
//     .then(resJson => {
//         console.log(resJson)
//     }).catch(err => console.log(err))

export async function getProductsFromIds(ids) {
  try {
    let idStr = '';
    ids.forEach((i, index) => {
      console.log(index, ids.length);
      idStr += `id[]=${i}${index === ids.length - 1 ? '' : '&'}`;
    });
    console.log('idStr', idStr);
    const res = await fetch(`${CATALOG_API.getProducts}?${idStr}`);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }
}

export async function getProducts(options) {
  try {
    let idStr = 'status=active';
    const { category } = options;
    const { featured } = options;
    const { fields } = options
    if (category) idStr += `&category=${category}`;
    if (featured) idStr += `&featured=true`;
    if (fields) fields.forEach(i => { idStr += `&fields[]=${i}` })
    console.log('idStr', idStr);
    const res = await fetch(`${CATALOG_API.getProducts}?${idStr}`);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }
}

export async function getProduct(id) {
  try {
    const res = await fetch(`${CATALOG_API.getProducts}/${id}`);
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }
}

export async function getProductOld(id) {
  const url = `${api.getProducts}/${id}`;
  return axios
    .get(url)
    .then(
      ({ status, data, statusText }) => {
        console.log(data);
        console.log(status);
        console.log(statusText);
        const { product } = data;
        return product;
      },
      ({ response }) => {
        const { data, status } = response;
        console.log(data);
        console.log(status);
        // return null
        toaster.notify(data.message);
      },
    )
    .catch(err => {
      console.log(err);
      toaster.notify(err);
      // return null
    });
}
