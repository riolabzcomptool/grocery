import { CATALOG_API } from '_constants/api';

import toaster from 'toasted-notes';
import fetch from '_utils/fetch';

export async function getInformations() {
  try {
    const url = `${CATALOG_API.getInformations}?status=active`;

    const res = await fetch(url);

    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }
}
