import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ReactQueryConfigProvider } from 'react-query';
import sagas from 'redux/sagas';

// import createHistory from 'history/createBrowserHistory'

import Router from 'navigation/router';
import './global.scss';
import { store, history, sagaMiddleware } from './storeConfig';
import * as serviceWorker from './serviceWorker';

// const history = createHashHistory()
// console.log('NODE_ENV', process.env.NODE_ENV)
if (process.env.NODE_ENV !== 'development' && true) {
console.log = () => { }
console.warn = () => { }
console.error = () => { }
}

sagaMiddleware.run(sagas);

const queryConfig = {
  refetchAllOnWindowFocus: false,
};

ReactDOM.render(
  // <React.StrictMode>
  <ReactQueryConfigProvider config={queryConfig}>
    <Provider store={store}>
      <Router history={history} />
    </Provider>
  </ReactQueryConfigProvider>,
  // </React.StrictMode>,
  document.getElementById('root'),
);

serviceWorker.register();
// export { store, history }
