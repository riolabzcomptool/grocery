import { wishlistActions as actions } from './actions';

const initialState = {
  products: [],
  error: null,
  loading: false,
};

export const getWishlist = state => state.wishlist;

export default function wishlistReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
