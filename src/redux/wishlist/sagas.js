/* eslint-disable camelcase */
import { all, put, call, takeLatest } from 'redux-saga/effects';
import { getWishlist, addToWishlist, remveFromWishlist } from 'services/user';
import { wishlistActions } from 'redux/actions';

export function* LOAD_WISHLIST() {
  console.log('in LOAD_WISHLIST');
  yield put({
    type: wishlistActions.SET_STATE,
    payload: {
      loading: true,
    },
  });
  const data = yield call(getWishlist);
  yield put({
    type: wishlistActions.SET_STATE,
    payload: {
      loading: false,
    },
  });
  if (data && data.products)
    yield put({
      type: wishlistActions.SET_STATE,
      payload: {
        products: data.products,
      },
    });
  if (data && data.error)
    yield put({
      type: wishlistActions.SET_STATE,
      payload: {
        error: data.error,
      },
    });
}

export function* EDIT_WISHLIST({ payload }) {
  const { type, id } = payload;
  const fn = type === 'add' ? addToWishlist : remveFromWishlist;
  yield put({
    type: wishlistActions.SET_STATE,
    payload: {
      loading: true,
    },
  });
  const a = yield call(fn, id);
  if (a && a.products)
    yield put({
      type: wishlistActions.SET_STATE,
      payload: { products: a.products },
    });
  if (a && a.error)
    yield put({
      type: wishlistActions.SET_STATE,
      payload: { error: a.error },
    });
  yield put({
    type: wishlistActions.SET_STATE,
    payload: {
      loading: false,
    },
  });
}

export default function* rootSaga() {
  yield all([
    takeLatest(wishlistActions.LOAD_WISHLIST, LOAD_WISHLIST),
    takeLatest(wishlistActions.EDIT_WISHLIST, EDIT_WISHLIST),
  ]);
}
