/* eslint-disable import/prefer-default-export */
export const wishlistActions = {
  SET_STATE: 'wishlist/SET_STATE',
  LOAD_WISHLIST: 'wishlist/LOAD_WISHLIST',
  EDIT_WISHLIST: 'wishlist/EDIT_WISHLIST',
};
