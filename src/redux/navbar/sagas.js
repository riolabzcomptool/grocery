import { all, put, call } from 'redux-saga/effects';
import { getNavBarMenuData } from 'services/navbar';
import { navBarActions } from './actions';

export function* GET_DATA() {
  console.log('in navbar get data');
  const navBarData = yield call(getNavBarMenuData);
  console.log('navBarData', navBarData);
  yield put({
    type: navBarActions.SET_STATE,
    payload: {
      navBarData,
    },
  });
}

export default function* rootSaga() {
  yield all([
    // takeEvery(navBarActions.GET_DATA, GET_DATA),
    GET_DATA(), // run once on app load to fetch menu data
  ]);
}
