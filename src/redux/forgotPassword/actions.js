
export const forgotPasswordActions = {
  FORGOT_PASSWORD : 'forgotPassword/FORGOT_PASSWORD' ,
  SET_STATE : 'forgotPassword/SET_STATE',
  FORGOT_PASSWORD_SUCCESS : 'forgotPassword/FORGOT_PASSWORD_SUCCESS',
  FORGOT_PASSWORD_ERROR : 'forgotPassword/FORGOT_PASSWORD_ERROR',
}

