import { all, takeEvery, put, call } from 'redux-saga/effects'
import { forgotPassword } from 'services/forgotPassword'
import { forgotPasswordActions as actions } from './actions'

export function* FORGOT_PASSWORD(props) {
  console.log(props)
  const {email} = props
  yield put({
    type: actions.SET_STATE,
    payload :{
      isSubmitting : true
    }
    
  })

  try {
    const response = yield call(forgotPassword, email)
    if (response) {
      yield put({
        type: actions.FORGOT_PASSWORD_SUCCESS,
        payload: response
      })
    }
  } catch (error) {
    yield put({
      type: actions.FORGOT_PASSWORD_ERROR,
      payload: error
    })
  }
  yield put({
    type: actions.SET_STATE,
    isSubmitting: false
  })
  
}

export default function* rootSaga() {
  yield all([takeEvery(actions.FORGOT_PASSWORD, FORGOT_PASSWORD())])
}
