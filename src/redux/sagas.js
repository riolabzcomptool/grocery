import { all } from 'redux-saga/effects';
import product from 'modules/product/details/sagas';
import blogs from 'modules/blogs/list/sagas';
import home from 'modules/home/sagas';
import blog from 'modules/blogs/details/sagas';
import order from './order/sagas';
import user from './user/sagas';
import menu from './menu/sagas';
import navbar from './navbar/sagas';
import cart from './cart/sagas';
import wishlist from './wishlist/sagas';

export default function* rootSaga() {
  yield all([
    user(),
    menu(),
    navbar(),
    product(),
    blogs(),
    blog(),
    home(),
    cart(),
    wishlist(),
    order(),
  ]);
}
