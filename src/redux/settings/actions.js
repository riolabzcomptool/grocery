/* eslint-disable import/prefer-default-export */
export const settingsActions = {
  SET_STATE: 'settings/SET_STATE',
  SET_BACKDROP: 'settings/SET_BACKDROP',
  CHANGE_SETTING: 'settings/CHANGE_SETTING',
}

