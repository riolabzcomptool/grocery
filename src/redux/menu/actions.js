/* eslint-disable import/prefer-default-export */
export const menuActions = {
  SET_STATE: 'menu/SET_STATE',
  GET_DATA: 'menu/GET_DATA',
  TOGGLE_WISHLIST: 'menu/TOGGLE_WISHLIST',
  TOGGLE_ACCOUNT_SIDE: 'menu/TOGGLE_ACCOUNT_SIDE',
  TOGGLE_CART_SIDE: 'menu/TOGGLE_CART_SIDE',
}

