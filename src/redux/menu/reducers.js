import { menuActions as actions } from './actions'

const initialState = {
  menuLeftData: [],
  menuTopData: [],
  profileMenuData: [],
  isWishlist:false,
  isAccountSide:false,
  isCartSide:false,
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_STATE:
      return { ...state, ...action.payload }
    case actions.TOGGLE_WISHLIST:
      return { ...state, isWishlist:!state.isWishlist }
    case actions.TOGGLE_ACCOUNT_SIDE:
      return { ...state, isAccountSide:!state.isAccountSide }
    case actions.TOGGLE_CART_SIDE:
      return { ...state, isCartSide:!state.isCartSide }
    default:
      return state
  }
}
