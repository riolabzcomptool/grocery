import { all, put, call, takeEvery } from 'redux-saga/effects'
import { getProfileMenuData } from 'services/menu'
import { menuActions } from './actions'

export function* GET_DATA() {
  console.log('in menu get data')
  const profileMenuData = yield call(getProfileMenuData)
  console.log(profileMenuData)
  yield put({
    type: 'menu/SET_STATE',
    payload: {
      profileMenuData,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(menuActions.GET_DATA, GET_DATA),
    // GET_DATA(), // run once on app load to fetch menu data
  ])
}
