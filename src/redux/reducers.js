import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import product from 'modules/product/details/reducers'
import home from 'modules/home/reducers'
import blogs from 'modules/blogs/list/reducers'
import blog from 'modules/blogs/details/reducers'
import order from './order/reducers';
import user from './user/reducers'
import menu from './menu/reducers'
import navbar from './navbar/reducers'
import cart from './cart/reducers'
import settings from './settings/reducers'
import wishlist from './wishlist/reducers'

export default history =>
  combineReducers({
    router: connectRouter(history),
    user,
    menu,
    navbar,
    product,
    blogs,
    blog,
    home,
    cart,
    settings,
    wishlist,
    order
  })
