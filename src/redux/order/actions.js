/* eslint-disable import/prefer-default-export */
export const orderDetailsActions = {
    SETCOUPEN_STATE: 'ordercoupen/SETCOUPEN_STATE',
    GETCOUPEN_DATA: 'ordercoupen/GETCOUPEN_DATA',
    SETORDER_STATE: 'order/SETORDER_STATE',
    GETORDER_DATA: 'order/GETORDER_DATA',
  }