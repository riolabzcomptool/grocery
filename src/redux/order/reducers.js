import { orderDetailsActions as actions } from './actions'

const initialState = {
    coupenDetail: [],
    orderDetail: null,
  loading: false,
  fetchError: null,
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SETCOUPEN_STATE: {
      console.log('blog payload', action.payload)
      return { ...state, ...action.payload }
    }
    case actions.SETORDER_STATE: {
      console.log('order payload', action.payload)
      return { ...state, ...action.payload }
    }
    default:
      return state
  }
}
