import { all, put, call, takeEvery } from 'redux-saga/effects'
import { getCoupen } from 'services/coupen'
import { setOrder } from 'services/order'
import { orderDetailsActions } from './actions'

export function* GETCOUPEN_DATA(actionObject) {
  console.log('in GETCOUPEN_DATA');
  const {
    payload: { id },
  } = actionObject;
  const coupenDetail = yield call(getCoupen, id)
  console.log(coupenDetail)
  yield put({
    type: orderDetailsActions.SETCOUPEN_STATE,
    payload: {
        coupenDetail,
    },
  })
}

export function* GETORDER_DATA(actionObject) {
  const {
    payload: { dataOrder },
  } = actionObject
  console.log('in order get data')
  const orderDetail = yield call(setOrder, dataOrder)
  console.log(orderDetail)
  yield put({
    type: orderDetailsActions.SETORDER_STATE,
    payload: {
      orderDetail,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(orderDetailsActions.GETCOUPEN_DATA, GETCOUPEN_DATA),
    takeEvery(orderDetailsActions.GETORDER_DATA, GETORDER_DATA),
    // GET_DATA(), // run once on app load to fetch menu data
  ])
}
