/* eslint-disable import/prefer-default-export */
export const cartActions = {
  GET_CART: 'cart/GET_CART',
  LOAD_CART: 'cart/LOAD_CART',
  SET_CART: 'cart/SET_CART',
  ADD_NEW_ITEM: 'cart/ADD_NEW_ITEM',
  REMOVE_ITEM: 'cart/REMOVE_ITEM',
  ADD_TO_CART: 'cart/ADD_TO_CART',
  REMOVE_FROM_CART: 'cart/REMOVE_FROM_CART',
  INC_QTY: 'cart/INC_QTY',
  DEC_QTY: 'cart/DEC_QTY',
  MOVE_GUEST_LOGGED:'cart/MOVE_GUEST_LOGGED',
  CLEAR_CART:'cart/CLEAR_CART',
};
