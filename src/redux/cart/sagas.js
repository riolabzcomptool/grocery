/* eslint-disable no-underscore-dangle */
import { all, takeEvery, takeLatest, put, select, call } from 'redux-saga/effects';
import toaster from 'toasted-notes';
import { getUser } from 'redux/user/reducers';
import isEmpty from 'lodash/isEmpty';
import { addMultipleToCart, getCart, addToCart, removeFromCart } from 'services/cart';
import store from 'store';
import { STRINGS } from '_constants';
import { cartActions } from './actions';
// deal qith qty

export function* ADD_TO_CART({ payload }) {
  const user = yield select(getUser);
  console.log(user, user.id, isEmpty(user.id));
  console.log('ADD_TO_CART', payload);
  let newProd = { ...payload };
  // let newProd = {
  //   productId: payload.productId,
  //   qty: payload.qty,
  //   listPrice: payload.listPrice,
  //   salePrice: payload.salePrice,
  //   title: payload.title,
  //   description: payload.description,
  //   img: payload.img,
  //   thumbImg: payload.thumbImg,
  //   prescriptionNeeded: payload.prescriptionNeeded,
  // };
  if (!user.isLogged) {
    // GUEST
    let prevProds = [];

    const prevProdsStore = store.get(`cart.products`);
    console.log('newProd', newProd);
    let maxLimitError = false;
    if (typeof prevProdsStore !== 'undefined') {
      prevProds = prevProdsStore;
      const existingProductIndex = prevProds.findIndex(a => a.productId === newProd.productId);
      console.log('existingProductIndex', existingProductIndex);
      if (existingProductIndex !== -1) {
        if (prevProds[existingProductIndex].qty < newProd.maxOrderQty) {
          prevProds[existingProductIndex].qty += newProd.qty;
        } else maxLimitError = true;
        console.log('maxLimitError 1', maxLimitError);
        newProd = null;
      }
    }
    const toUpdate = newProd ? [...prevProds, { ...newProd }] : [...prevProds];
    yield store.set('cart.products', toUpdate);
    yield put({
      type: cartActions.SET_CART,
      payload: { products: toUpdate },
    });
    console.log('maxLimitError 2', maxLimitError);
    if (maxLimitError) toaster.notify(STRINGS.max_limit_reached, { position: 'top-right' });
    else toaster.notify(STRINGS.added_to_cart, { position: 'top-right' });
  } else {
    // LOGGED IN USER
    console.log({ productId: newProd.productId, quantity: newProd.qty });
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: true,
      },
    });
    const { products } = yield call(addToCart, newProd.productId, newProd.qty);
    if (products && products.length > 0) {
      yield put({
        type: cartActions.SET_CART,
        payload: {
          products: products.map(m => {
            const { product: i } = m;
            return transformToProduct(i, m.quantity);
          }),
        },
      });
      toaster.notify(STRINGS.added_to_cart, { position: 'top-right' });
    }
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: false,
      },
    });
  }
}

export function* REMOVE_FROM_CART({ payload }) {
  const user = yield select(getUser);
  const { productId } = payload;

  if (!user.isLogged) {
    console.log(payload);
    const prevProdsStore = store.get(`cart.products`);
    let filteredProds = [];
    if (typeof prevProdsStore !== 'undefined') {
      filteredProds = prevProdsStore.filter(i => i.productId !== productId);
    }
    yield store.set('cart.products', [...filteredProds]);

    yield put({
      type: cartActions.SET_CART,
      payload: { products: [...filteredProds] },
    });
  } else {
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: true,
      },
    });
    const { products } = yield call(removeFromCart, productId);
    if (products)
      yield put({
        type: cartActions.SET_CART,
        payload: {
          products: products.map(m => {
            const { product: i } = m;
            return transformToProduct(i, m.quantity);
          }),
        },
      });
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: false,
      },
    });
  }
}

// maxOrderQty, productId
export function* INC_QTY({ payload }) {
  console.log(payload);
  const { maxOrderQty, productId } = payload;

  const user = yield select(getUser);

  if (!user.isLogged) {
    const prods = store.get(`cart.products`) || [];
    const index = prods.findIndex(a => a.productId === productId);
    if (index !== -1) {
      const { qty } = prods[index];
      if (qty < maxOrderQty) {
        prods[index].qty += 1;
        yield store.set('cart.products', prods);
        yield put({
          type: cartActions.SET_CART,
          payload: { products: prods },
        });
      } else toaster.notify(STRINGS.max_limit_reached);
    } else {
      yield put({
        type: cartActions.ADD_TO_CART,
        payload: {
          productId: payload._id,
          slug: payload.slug,
          name: payload.name,
          qty: 1,
          listPrice: payload.pricing ? payload.pricing.listPrice : '',
          salePrice: payload.pricing ? payload.pricing.salePrice : '',
          img: payload.images ? payload.images[0].url : '',
          thumbImg: payload.images ? payload.images[0].thumbnail : '',
          maxOrderQty: payload.maxOrderQty,
          minOrderQty: payload.minOrderQty,
          prescriptionNeeded: payload.prescriptionNeeded,
          deleted: payload.deleted,
          status: payload.status,
          outOfStockStatus: payload.stock ? payload.stock.outOfStockStatus : '',
        },
      });
    }
  } else {
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: true,
      },
    });
    const { products } = yield call(addToCart, productId, 1);
    if (products)
      yield put({
        type: cartActions.SET_CART,
        payload: {
          products: products.map(m => {
            const { product: i } = m;
            return transformToProduct(i, m.quantity);
          }),
        },
      });
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: false,
      },
    });
  }
}

// minOrderQty, productId
export function* DEC_QTY({ payload }) {
  console.log(payload);
  const { minOrderQty, productId } = payload;

  const user = yield select(getUser);

  if (!user.isLogged) {
    const prods = store.get(`cart.products`) || [];
    const index = prods.findIndex(a => a.productId === productId);
    // let minLimitError = false;
    if (index !== -1) {
      const { qty } = prods[index];
      if (qty > minOrderQty) {
        prods[index].qty -= 1;
        yield store.set('cart.products', prods);
        yield put({
          type: cartActions.SET_CART,
          payload: { products: prods },
        });
      } else toaster.notify(`Minimum quantity of ${minOrderQty} required`);
    } else {
      yield put({
        type: cartActions.REMOVE_FROM_CART,
        payload: {
          productId,
        },
      });
    }
  } else {
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: true,
      },
    });
    const { products } = yield call(addToCart, productId, -1);
    if (products)
      yield put({
        type: cartActions.SET_CART,
        payload: {
          products: products.map(m => {
            const { product: i } = m;
            return transformToProduct(i, m.quantity);
          }),
        },
      });
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: false,
      },
    });
  }
}

function transformToProduct(i, qty) {
  return {
    // product:i.product,
    productId: i._id,
    slug: i.slug,
    name: i.name,
    qty,
    listPrice: i.pricing ? i.pricing.listPrice : '',
    salePrice: i.pricing ? i.pricing.salePrice : '',
    img: i.images ? i.images[0].url : '',
    thumbImg: i.images ? i.images[0].thumbnail : '',
    maxOrderQty: i.maxOrderQty,
    minOrderQty: i.minOrderQty,
    prescriptionNeeded: i.prescriptionNeeded,
    deleted: i.deleted,
    status: i.status,
    outOfStockStatus: i.stock ? i.stock.outOfStockStatus : '',
  };
}

export function* MOVE_GUEST_LOGGED() {
  const user = yield select(getUser);
  const cartStore = store.get(`cart.products`);
  console.log(user.id && cartStore && cartStore.length > 0);

  if (user.id && cartStore && cartStore.length > 0) {
    // call api to add to cart
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: true,
      },
    });
    const { products } = yield call(addMultipleToCart, cartStore);
    console.log(products);
    if (products && products.length) {
      if (cartStore) store.remove('cart.products');
      yield put({
        type: cartActions.SET_CART,
        payload: {
          products: products.map(m => {
            const { product: i } = m;
            return transformToProduct(i, m.quantity);
          }),
        },
      });
    }
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: false,
      },
    });
  }
}

export function* LOAD_CART() {
  const user = yield select(getUser);
  console.log('USERID CART', user.id);
  if (user.id) {
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: true,
      },
    });
    const { products } = yield call(getCart);
    console.log('CART PRODUCTS', products, products && products.length);

    if (products) {
      yield put({
        type: cartActions.SET_CART,
        payload: {
          products:
            products.length > 0
              ? products.map(m => {
                  const { product: i } = m;
                  return { ...m, ...transformToProduct(i, m.quantity) };
                })
              : [],
        },
      });
    }
    yield put({
      type: cartActions.SET_CART,
      payload: {
        loading: false,
      },
    });
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest(cartActions.ADD_TO_CART, ADD_TO_CART),
    takeEvery(cartActions.REMOVE_FROM_CART, REMOVE_FROM_CART),
    takeEvery(cartActions.INC_QTY, INC_QTY),
    takeEvery(cartActions.DEC_QTY, DEC_QTY),
    takeEvery(cartActions.MOVE_GUEST_LOGGED, MOVE_GUEST_LOGGED),
    takeEvery(cartActions.LOAD_CART, LOAD_CART),
    // SETUP(),
  ]);
}

// const sample = [
//   {
//     id: '5e341ebc26d1653b45485daf',
//     qty: 4,
//     listPrice: 350,
//     salePrice: 200,
//     title: 'HealthVit Vitamin B12 2000mcg',
//     description: 'bottle of 30 soft gelatin capsules',
//     img: 'uploads/product/2020-02-13T20:30:50.042Zkickill-product-7.png',
//     thumbImg: 'uploads/product/2020-02-13T20:30:50.042Zkickill-product-7_thumb.png',
//     prescriptionNeeded: false,
//   },
//   {
//     id: '5e35755650ceaa50c99ab1bc',
//     qty: 1,
//     listPrice: 350,
//     salePrice: 200,
//     title: 'HealthVit Vitamin B12 2000mcg 112',
//     description: 'bottle of 30 soft gelatin capsules',
//     img: 'uploads/product/2020-02-13T20:32:18.130Zkickill-product-2.png',
//     thumbImg: 'uploads/product/2020-02-13T20:32:18.130Zkickill-product-2_thumb.png',
//     prescriptionNeeded: false,
//   },
//   {
//     id: '5e35785b50ceaa50c99ab23c',
//     qty: 1,
//     listPrice: 350,
//     salePrice: 200,
//     title: 'HealthVit Vitamin B12 2000mcg ABC',
//     description: 'bottle of 30 soft gelatin capsules',
//     img: 'uploads/product/2020-02-13T20:31:56.772Zkickill-product-1.png',
//     thumbImg: 'uploads/product/2020-02-13T20:31:56.772Zkickill-product-1_thumb.png',
//     prescriptionNeeded: true,
//   },
// ];
