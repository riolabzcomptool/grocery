import store from 'store';
import { cartActions } from './actions';

// productId
// slug
// name
// qty
// listPrice
// salePrice
// img
// thumbImg
// maxOrderQty
// minOrderQty
// prescriptionNeeded
// deleted
// status
// outOfStockStatus

export const getCart = state => state.cart;

const STORED_CART = storedCart => {
  const cart = {};
  Object.keys(storedCart).forEach(key => {
    const item = store.get(`cart.${key}`);
    const cartValue = typeof item !== 'undefined' ? item : storedCart[key];
    cart[key] = cartValue;
  });
  return cart;
};

const initialState = {
  ...STORED_CART({
    id: '',
    loading: false,
    products: [],
  }),
};

export default function cartReducer(state = initialState, action) {
  const { payload } = action;
  let filteredProducts = [];
  let productId = '';
  if (typeof payload !== 'undefined') {
    productId = payload.productId;
    if (productId) filteredProducts = state.products.filter(i => i !== productId);
  }

  const cartStore = store.get(`cart.products`);

  switch (action.type) {
    case cartActions.SET_CART:
      return { ...state, ...action.payload };
    case cartActions.SET_PRODUCTS:
      return { ...state, products: [...action.payload] };
    case cartActions.ADD_NEW_ITEM:
      return { ...state, products: [...state.products, { ...action.payload }] };
    case cartActions.REMOVE_ITEM:
      return { ...state, products: [...filteredProducts] };
    case cartActions.CLEAR_CART:
      if (cartStore) store.remove(`cart.products`);
      return { ...state, products: [], id: '' }
    default:
      return state;
  }
}
