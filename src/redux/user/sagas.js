/* eslint-disable camelcase */
import { all, takeEvery, put, call, select } from 'redux-saga/effects';
import toaster from 'toasted-notes';
import { STRINGS } from '_constants';
import {
  updateProfile,
  logout,
  gblogin,
  currentAccount,
  fblogin,
  register,
  login,
} from 'services/user';
import { menuActions, userActions } from 'redux/actions';
import { wishlistActions } from 'redux/wishlist/actions';
import { cartActions } from 'redux/cart/actions';
import { getCart } from 'redux/cart/reducers';

export function* REGISTER({ payload }) {
  console.log(payload);
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isRegistering: true,
    },
  });
  const {success, error} = yield call(register, payload);

  console.log(success);
  if (success) {
    console.log('registered');
    toaster.notify(STRINGS.register_success);
    yield put({
      type: userActions.SET_STATE,
      payload: {
        registerSuccess: true,
        registerError: false,
      },
    });
  } else {
    console.log('registration fail');
    yield put({
      type: userActions.SET_STATE,
      payload: {
        registerError: error || 'Error registering. Please try again later.',
        registerSuccess: false,
      },
    });
  }
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isRegistering: false,
    },
  });
}

export function* GBLOGIN({ payload }) {
  console.log('in login redux sagatttttttttttt');
  const { email, id, name } = payload;
  console.log(email, id, name);
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isLogging: true,
    },
  });
  const success = yield call(gblogin, email, id, name);
  console.log(success);
  if (success) {
    console.log('logged intttttttttttttt');
    yield put({
      type: userActions.SET_STATE,
      payload: {
        isLogged: true,
        loginError: false,
        isLogging: false,
        loading: false,
      },
    });
    yield put({
      type: userActions.LOAD_CURRENT_ACCOUNT,
      payload: {
        fromLogin: true,
      },
    });
  } else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loginError: true,
        isLogging: false,
      },
    });
  }
}

export function* FBLOGIN({ payload }) {
  console.log('in login redux sagatttttttttttt');
  const { email, id, name } = payload;
  console.log(email, id, name);
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isLogging: true,
    },
  });
  const success = yield call(fblogin, email, id, name);
  console.log(success);
  if (success) {
    console.log('logged intttttttttttttt');
    yield put({
      type: userActions.SET_STATE,
      payload: {
        isLogged: true,
        loginError: false,
        isLogging: false,
        loading: false,
      },
    });
    yield put({
      type: userActions.LOAD_CURRENT_ACCOUNT,
      payload: {
        fromLogin: true,
      },
    });
  } else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loginError: true,
        isLogging: false,
      },
    });
  }
}

export function* LOGIN({ payload }) {
  console.log('in login redux saga');
  const { email, password } = payload;
  console.log(email, password);
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isLogging: true,
    },
  });
  const {success, error} = yield call(login, email, password);
  console.log(success);
  if (success) {
    console.log('logged in');
    yield put({
      type: userActions.SET_STATE,
      payload: {
        isLogged: true,
        loginError: false,
        isLogging: false,
        loading: false,
      },
    });
    yield put({
      type: userActions.LOAD_CURRENT_ACCOUNT,
      payload: {
        fromLogin: true,
      },
    });
  } else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loginError: error || 'Error logging in',
        isLogging: false,
      },
    });
  }
}

export function* UPDATE_PROFILE({ payload }) {
  const updated = yield call(updateProfile, payload);
  if (updated)
    yield put({
      type: userActions.SET_STATE,
      payload: {
        firstName: updated.firstName,
        gender: updated.gender,
        lastName: updated.lastName,
      },
    });
}

export function* LOAD_CURRENT_ACCOUNT(data = {}) {
  const { payload } = data;
  const fromLogin = payload ? payload.fromLogin : false;
  const cart = yield select(getCart);
  const cartHasProducts = cart.products.length > 0;
  console.log('loading current account. fromLogin:', fromLogin);
  yield put({
    type: userActions.SET_STATE,
    payload: {
      loading: true,
    },
  });
  const response = yield call(currentAccount);
  console.log('response LOAD_CURRENT_ACCOUNT', response);
  if (response && response.id) {
    // const { email, id, role, phoneNo, name } = response

    const { email, user_type, firstName, id, lastName, gender, phone, phoneVerified } = response;
    yield put({
      type: userActions.SET_STATE,
      payload: {
        email,
        role: user_type && user_type.name ? user_type.name : '',
        firstName,
        id,
        gender,
        phoneVerified,
        lastName,
        phoneNo: phone,
        authorized: true,
        isLogged: true,
        loading: false,
        loginError: false,
      },
    });
    yield put({
      type: menuActions.GET_DATA,
    });
    yield put({
      type: wishlistActions.LOAD_WISHLIST,
    });
    if (fromLogin && cartHasProducts)
      yield put({
        type: cartActions.MOVE_GUEST_LOGGED,
      });
    else {
      yield put({
        type: cartActions.LOAD_CART,
      });
    }
  } else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loading: false,
        // email:'',
        // role:'',
        // firstName:'',
        // userId:'',
        // lastName:'',
        // phoneNo:'',
        authorized: false,
        isLogged: false,
        // loginError: response.message,
      },
    });
  }
}

export function* LOGOUT() {
  yield call(logout);
  yield put({
    type: userActions.SET_STATE,
    payload: {
      id: '',
      name: '',
      role: '',
      email: '',
      firstName: '',
      gender: '',
      lastName: '',
      loginError: false,
      phoneNo: '',
      phoneVerified: '',
      authorized: false,
      loading: false,
      isLogging: false,
      isLogged: false,
    },
  });
  yield put({
    type: cartActions.CLEAR_CART,
  });
  yield put({
    type: wishlistActions.SET_STATE,
    payload:{
      products:[]
    }
  });
}

export default function* rootSaga() {
  yield all([
    takeEvery(userActions.LOGIN, LOGIN),
    takeEvery(userActions.REGISTER, REGISTER),
    takeEvery(userActions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    takeEvery(userActions.LOGOUT, LOGOUT),
    takeEvery(userActions.FBLOGIN, FBLOGIN),
    takeEvery(userActions.GBLOGIN, GBLOGIN),
    // takeEvery(userActions.LOGOUT, LOGOUT),
    LOAD_CURRENT_ACCOUNT(), // run once on app load to check user auth
  ]);
}
