import React, { useState, useCallback, useMemo } from 'react';
import classNames from 'classnames';

const Tabs = props => {
  const [activeIndex, setActiveIndex] = useState(0);
  const { children } = props;

  // console.log('hahahaha',props)
  const handleTabChange = useCallback(index => {
    return () => setActiveIndex(index);
  }, []);

  const menuItems = useMemo(() => {
    
    return (
      <ul className="nav nav-tabs nav-material" id="top-tab" role="tablist">
        {React.Children.map(children, (item, index) => {
          if (!item) return null;
          console.log('hahaha',item)
          const { label, id } = item? item.props : {};
          // console.log(label, index === activeIndex ? 'active' : 'inactive');
          return (
            <li className="nav-item" key={item.id}>
              <a
                onClick={handleTabChange(index)}
                className={classNames('nav-link', { active: index === activeIndex })}
                id={`${id}-tab`}
                data-toggle="tab"
                href={`#${id}`}
                role="tab"
                aria-selected="true"
              >
                <i className="icofont icofont-ui-home" />
                {label}
              </a>
              <div className="material-border" />
            </li>
          );
        })}
      </ul>
    );
  // }, [activeIndex, children, handleTabChange]);
  }, [activeIndex, children, handleTabChange]);

  // const activeTab = children.length > 0 ? children.find((item, index) => index === activeIndex) : children;
  
  // const activeTab = React.cloneElement(activeT, { className: 'tab-pane fade show active' });

   
  return (
    <>
      <section className="tab-product m-0">
        <div className="row">
          <div className="col-sm-12 col-lg-12">
            {menuItems}
            <div className="tab-content nav-material" id="top-tabContent">
              {children}
            </div>
          </div>
        </div>
      </section>
      {/* <div className="tabs-wrapper">
          {this.getMenuItems()}
          <div className="tab-details">{this.getTabContent()}</div>
        </div> */}
    </>
  );
};

export const TabsPanel = ({ children, id }) => {
  return (
    <div className="tab-pane fade" id={id} role="tabpanel" aria-labelledby={`${id}-tab`}>
      {children}
    </div>
  );
};

export default Tabs;
