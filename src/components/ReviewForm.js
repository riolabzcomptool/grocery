import React from 'react';
import { reviewSchema } from '_utils/Schemas';
import { withFormik } from 'formik';
import RatingStar from 'components/RatingStar';
import FormItem from './FormItem';

const ReviewForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    // setValues,
    setFieldValue,
    // isSubmitting,
    // handleReset
  } = props;

  const onChangeRating = value => {
    console.log('rating changed', value);
    setFieldValue('rating', value, false);
  };
  return (
    <form className="theme-form" onSubmit={handleSubmit}>
      <div className="form-row">
        <div className="col-md-12">
          <div className="media">
            <FormItem
              errors={errors.rating && touched.rating ? errors.rating : ''}
              id="rating"
              label="rating"
            >
              <RatingStar initialValue={+values.rating} onChange={onChangeRating} />
            </FormItem>
          </div>
        </div>
        <div className="col-md-6">
          <FormItem errors={errors.name && touched.name ? errors.name : ''} id="name" label="name">
            <input
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              className="form-control"
              id="name"
              placeholder="Enter Your name"
              // required
            />
          </FormItem>
        </div>
        <div className="col-md-6">
          <FormItem
            errors={errors.email && touched.email ? errors.email : ''}
            id="email"
            label="email"
          >
            <input
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              id="email"
              type="email"
              className="form-control"
              placeholder="Email"
              // required
            />
          </FormItem>
        </div>
        <div className="col-md-12">
          <FormItem
            errors={errors.title && touched.title ? errors.title : ''}
            id="title"
            label="Review title"
          >
            <input
              value={values.title}
              onChange={handleChange}
              onBlur={handleBlur}
              type="text"
              id="title"
              className="form-control"
              placeholder="Review title"
              // required
            />
          </FormItem>
        </div>
        <div className="col-md-12">
          <FormItem
            errors={errors.text && touched.text ? errors.text : ''}
            id="comment"
            label="Comment"
          >
            <textarea
              value={values.text}
              onChange={handleChange}
              onBlur={handleBlur}
              className="form-control"
              placeholder="Write Your Testimonial Here"
              id="text"
              rows="6"
            />
          </FormItem>
        </div>
        <div className="col-md-12 mt-3">
          <button className="btn btn-solid" type="button" onClick={handleSubmit}>
            Submit Your Review
          </button>
        </div>
      </div>
    </form>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: props => {
    const { text = '', title = '', rating = 0, name ='', email='' } = props;
    console.log(props);
    return {
      text,
      title,
      rating,
      name,
      email,
    };
  },

  validationSchema: reviewSchema,

  handleSubmit: async (values, { props }) => {
    props.onSubmit(values);

    // setTimeout(() => {
    //   // alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false);
    // }, 1000);
  },

  displayName: 'ReviewForm',
})(ReviewForm);

export default MyEnhancedForm;
