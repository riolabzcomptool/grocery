/* eslint-disable no-underscore-dangle */
import React, { useEffect } from 'react';
import { useProduct } from 'hooks/api';
import img from 'assets/images/cosmetic/14.jpg';
import { cartActions } from 'redux/actions';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import Portal from './Portal';
import Loader from './Loader';
import Img from './Img';
import QtyInput from './QtyInput';

const ProductDetailModal = props => {
  const { id, dispatch, cart, history } = props;
  const { refetch, data: res, isFetching } = useProduct(id, true);
  const { loading: cartLoading, products: cartProducts } = cart;

  const currentQty = cartProducts.find(i => i.productId === id)?.qty || 1;

  console.log('bbb', document.getElementsByClassName('modal-backdrop'));
  const modalBackdropEls = document.getElementsByClassName('modal-backdrop');
  // if (el.length > 0) el[0].classList.remove('show');

  useEffect(() => {
      console.log('AAAB res', id, res)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[res])

  useEffect(() => {
    if (id) {
      console.log('AAAB refetching prod', id);
      refetch();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  const handleDetailsClick = e => {
    // to={`/product/${res.data.slug}`}
    if (modalBackdropEls?.length > 0) modalBackdropEls[0].classList.remove('show');
    history.push(`/product/${res.data.slug}`);
    e.preventDefault();
  };

  const onIncQty = () => {
    if (res?.data)
      dispatch({
        type: cartActions.INC_QTY,
        payload: {
          productId: res.data._id,
          maxOrderQty: res.data.maxOrderQty,
          ...res.data,
        },
      });
  };

  const onDecQty = () => {
    if (res?.data)
      dispatch({
        type: cartActions.DEC_QTY,
        payload: {
          productId: res.data._id,
          maxOrderQty: res.data.maxOrderQty,
          ...res.data,
        },
      });
  };

  if (id)
    return (
      <Portal>
        <div
          className="modal fade bd-example-modal-lg theme-modal"
          id={`quick-view-${id}`}
          tabIndex={-1}
          role="dialog"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div className="modal-content quick-view-modal">
              <div className="modal-body">
                {isFetching && <Loader fullScreen={false} />}
                {!isFetching && res?.data && (
                  <>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <div className="row">
                      <div className="col-lg-6 col-xs-12">
                        <div className="quick-view-img">
                          <Img
                            lazyImg={img}
                            src={res.data.images?.[0]?.url}
                            alt={res.data.name}
                            className="img-fluid "
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 rtl-text">
                        <div className="product-right">
                          <h2>{res.data.name}</h2>
                          <h3>${res.data.pricing?.salePrice || res.data.pricing?.listPrice}</h3>
                          <div className="border-product">
                            <h6 className="product-title">product details:</h6>
                            <p>
                              this brand is a t-shirt led value brand for men, women and kids. Our
                              range consists of basic and updated basic knit apparel. We offer both
                              singles and packs with the right blend of quality, style and value
                              aimed to delight our customers.
                            </p>
                          </div>
                          <div className="product-description border-product">
                            <h6 className="product-title">color:</h6>
                            <ul className="color-variant">
                              <li className="light-purple active" />
                              <li className="theme-blue" />
                              <li className="theme-color" />
                            </ul>
                            <h6 className="product-title">size:</h6>
                            <div className="size-box">
                              <ul className="size-box">
                                <li className="active">xs</li>
                                <li>s</li>
                                <li>m</li>
                                <li>l</li>
                                <li>xl</li>
                              </ul>
                            </div>
                            <h6 className="product-title">quantity:</h6>
                            <QtyInput
                              loading={cartLoading}
                              onInc={onIncQty}
                              onDec={onDecQty}
                              value={currentQty}
                            />
                          </div>
                          <div className="product-buttons">
                            <button
                              type="button"
                              onClick={onIncQty}
                              className="btn btn-solid bg-gradient"
                            >
                              add to cart
                            </button>
                            {res?.data?.slug && (
                              <a
                                href="#"
                                onClick={handleDetailsClick}
                                className="btn btn-solid bg-gradient"
                              >
                                view detail
                              </a>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </Portal>
    );
  return null;
};

export default withRouter(connect(({ cart }) => ({ cart }))(ProductDetailModal));
