import React from 'react';
import { connect } from 'react-redux';
import { RegisterSchema } from '_utils/Schemas';
import { userActions } from 'redux/actions';
import Input from 'components/Input';
import { withFormik } from 'formik';
import FormItem from './FormItem';
import Button from './Button';
import AnimCheckmark from './AnimCheckmark';

const SignupForm = props => {
  const {
    user,
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    isSubmitting,
    onLoginClick,
    status,
  } = props;
  console.log('koohl', props);

  if (status?.isSubmitted && user.registerSuccess)
    return (
      <div className="register-success-wrapper">
        <AnimCheckmark />
        <h5 className="mt-4">Registration successfull!</h5>
        <button type="button" onClick={onLoginClick} className="btn btn-solid mt-4">
          Login
        </button>{' '}
      </div>
    );

  return (
    <form className="theme-form">
      <FormItem
        label="First Name"
        id="firstName"
        errors={errors.firstName && touched.firstName ? errors.firstName : ''}
      >
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.firstName}
          placeholder="First Name"
          name="firstName"
        />
      </FormItem>
      <FormItem
        label="Last Name"
        id="lastName"
        errors={errors.lastName && touched.lastName ? errors.lastName : ''}
      >
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.lastName}
          placeholder="Last name"
          name="lastName"
        />
      </FormItem>
      <FormItem label="Email" id="email" errors={errors.email && touched.email ? errors.email : ''}>
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          placeholder="Email"
          name="email"
        />
      </FormItem>
      <FormItem
        label="password"
        id="password"
        errors={errors.password && touched.password ? errors.password : ''}
      >
        <Input
          placeholder="Enter password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
          name="password"
          type="password"
        />
      </FormItem>
      <FormItem
        label="Confirm password"
        id="confirmPassword"
        errors={errors.confirmPassword && touched.confirmPassword ? errors.confirmPassword : ''}
      >
        <Input
          placeholder="Confirm password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.confirmPassword}
          type="password"
          name="confirmPassword"
        />
      </FormItem>

      {user.registerError && <p className="error-message capitalize pt-3">{user.registerError}</p>}
      <Button
        onClick={handleSubmit}
        loading={isSubmitting}
        className="btn btn-solid btn-solid-sm btn-block"
      >
        Sign up
      </Button>
      <h5 className="forget-class">
        <button type="button" onClick={onLoginClick} className="d-block link">
          Already a user? Login here
        </button>
      </h5>
    </form>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: () => {
    return {
      lastName: '',
      firstName: '',
      email: '',
      password: '',
      confirmPassword: '',
    };
  },

  validationSchema: RegisterSchema,

  handleSubmit: async (values, { props, setSubmitting, setStatus }) => {
    console.log(props);
    const { dispatch, user } = props;
    const { email, password, firstName, lastName } = values;
    setSubmitting(true);
    dispatch({
      type: userActions.REGISTER,
      payload: {
        email,
        password,
        firstName,
        lastName,
      },
    });
    setStatus({ isSubmitted: true });
    if (!user.isRegistering) setSubmitting(false);
  },

  displayName: 'SignupForm',
})(SignupForm);

export default connect(({ user }) => ({ user }))(MyEnhancedForm);
