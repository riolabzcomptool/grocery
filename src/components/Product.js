/* eslint-disable no-underscore-dangle */
import React, { useState, useCallback } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { connect } from 'react-redux';
import Skeleton from 'react-loading-skeleton';
import PropTypes from 'prop-types';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import { wishlistActions } from 'redux/actions';
// import HeartIcon from './HeartIcon';
// import Authorize from './Authorize';
import lazyImg from 'assets/images/grocery/pro/8.jpg';
import LazyImg from './LazyImg'
import ProductDetailModal from './ProductDetailModal';

const styles = {
  link: { color: '#333' },
  red: { color: '#ce0404' },
};

const Product = props => {
  const {
    hasBg,
    title,
    onClose,
    listPrice,
    salePrice,
    price,
    item,
    slug,
    loading,
    deadLink,
    id,
    isWishlist,
    dispatch,
    bordered,
    noButton,
    user,
    // noFav,
  } = props;
  const [quickViewId, setQuickViewId] = useState(null);
  let { frequent, img = '' } = props;
  if (img && !img.startsWith('/')) img = `/${img}`;
  frequent = Boolean(frequent);

  // const loadingClass = Boolean(title)
  //  <div className={`product-listing ${frequent ? 'frequently-brought-product-list' : ''}`}>

  /**
   *
   * @param {nodes} children
   * @param {boolean} isDeadLink
   * @param {string} toLink
   */
  const getLinked = children => {
    if (deadLink) return <div className="img-block">{children}</div>;
    return (
      <Link
        to={`/product/${slug}`}
        style={styles.link}
        className={classNames({ 'bg-size': hasBg })}
      >
        {children}
      </Link>
    );
  };

  const handleClose = () => onClose(id);

  const handleToggleWishlist = () => {
    if (!isWishlist) {
      dispatch({
        type: wishlistActions.EDIT_WISHLIST,
        payload: {
          type: 'add',
          id,
        },
      });

      return '';
    }
    dispatch({
      type: wishlistActions.EDIT_WISHLIST,
      payload: {
        type: 'remove',
        id,
      },
    });

    return '';
  };

  const handleQuickView = useCallback(selId => {
    return e => {
      e.preventDefault();
      setQuickViewId(selId);
    };
  }, []);

  const LoadingProduct = (
    <div className="product-box product-select-box d-flex flex-column align-center">
      <div className="img-block">
        <Skeleton width={120} height={109} />{' '}
      </div>
      <h5 className="product-name">
        <Skeleton width={150} />
      </h5>
      <h6 className="product-discription">
        <Skeleton width={140} />
      </h6>

      <div>
        <h3>
          <Skeleton width={50} />
        </h3>
      </div>
      {/* <div type="button" style={{ color: 'transparent' }} className="small-btn">
        Buy
      </div> */}
    </div>
  );

  const DefaultProduct = (
    <div
      className={classNames({
        'product-box': true,
        'product-select-box': true,
        bordered,
        'frequently-brought-product-list': frequent,
        // 'loading': loadingClass
      })}
    >
      <ProductDetailModal id={quickViewId} />
      <div className="img-block">
        {getLinked(
          img ? (
            <LazyImg lazyImg={lazyImg} src={img} alt="" className="img-fluid bg-img product-image" />
          ) : (
            <Skeleton />
          ),
        )}
        <div className="cart-details">
          {user.isLogged && (
            <button type="button" title="Add to Wishlist" onClick={handleToggleWishlist}>
              <i className="fa fa-heart" aria-hidden="true" />
            </button>
          )}
          <a
            href="#"
            onClick={handleQuickView(id)}
            data-toggle="modal"
            data-target={`#quick-view-${id}`}
            title="Quick View"
          >
            <i className="fa fa-search" aria-hidden="true" />
          </a>
        </div>

        <div className={classNames('is-wishlist', { show: isWishlist })}>
          <i className="fa fa-heart" aria-hidden={!isWishlist} />
        </div>

        {/* <h6 className="product-discription">{frequent ? '' : description || <Skeleton />}</h6> */}
      </div>

      {/* <Link to={`/product/${slug}`} style={styles.link}>
        {img ? <img src={`/${img}`} alt="" className="product-image lazy" /> : <Skeleton />}
        <h5 className="product-name">{title || <Skeleton />}</h5>
        <h6 className="product-discription">{frequent ? '' : description || <Skeleton />}</h6>
      </Link> */}
      {listPrice && salePrice ? (
        <div className="product-info">
          <h6>{title || <Skeleton />}</h6>

          <h5>
            <i className="fas fa-rupee-sign" /> {listPrice || <Skeleton />}&nbsp;
            <del style={styles.red}>
              <i className="fas fa-rupee-sign" />
              {salePrice || <Skeleton />}
            </del>
          </h5>
          {/* <h5 className="offer-price">
              <i className="fas fa-rupee-sign" /> {salePrice || <Skeleton />}
            </h5> */}
        </div>
      ) : (
        <>
          <div className="product-info">
            <h6>{title || <Skeleton />}</h6>
            <h5>
              {(salePrice || listPrice || price) && <i className="fas fa-rupee-sign" />}
              {salePrice || listPrice || price || <Skeleton />}
            </h5>
          </div>
          <div className="qty-add-box">
            <br />
            {title &&
              !noButton &&
              getLinked(
                <div className="qty-box">
                  <button title="Buy now" type="button" className="small-btn">
                    <i className="fa fa-shopping-cart" />
                    Add to cart
                  </button>
                </div>,
              )}
            {/* // <button type="button" onClick={handleBuyProduct} className="small-btn">
            //   Buy
            // </button> */}

            {/* {title && !deadLink && (
              <Link to={`/product/${slug}`} style={styles.link}>
                <button type="button" className="small-btn">
                  Add to cart
                </button>
              </Link>
            )} */}
          </div>
        </>
      )}
      {/* {!onClose && !noFav && (
        <Authorize noRedirect>
          <HeartIcon active={isWishlist} onClick={handleToggleWishlist} />
        </Authorize>
      )} */}
      {onClose && (
        <div
          className="icon-wrapper"
          role="button"
          tabIndex={0}
          onKeyDown={handleClose}
          onClick={handleClose}
        >
          <i className="fas fa-times-circle" />
        </div>
      )}
    </div>
  );

  // return LoadingProduct;
  if (item) return <div className="item">{DefaultProduct}</div>;
  if (loading) return LoadingProduct;
  return DefaultProduct;
};

Product.propTypes = {
  deadLink: PropTypes.bool,
  bordered: PropTypes.bool,
  noButton: PropTypes.bool,
  noFav: PropTypes.bool,
  hasBg: PropTypes.bool,
  // type: PropTypes.string,
};

Product.defaultProps = {
  deadLink: false,
  // type: 'regular',
  bordered: false,
  noButton: false,
  noFav: false,
  hasBg: true,
};

export default connect(({ user, cart, wishlist }, ownProps) => {
  const { id } = ownProps;

  const isWishlist = !isEmpty(find(wishlist.products, i => i._id === id));
  return { user, cart, isWishlist };
})(Product);
