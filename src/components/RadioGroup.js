import React, { useState, useCallback, useMemo, useEffect } from 'react';

const RadioGroup = props => {
  const { children, name, value: initialValue, onChange: onChangeProp } = props;
  const [value, setValue] = useState(initialValue);


  const onChange = useCallback(
    val => {
      setValue(val);
      if (onChangeProp) onChangeProp(val);
    },
    [onChangeProp],
  );

  const values = useMemo(
    () => ({
      onChange,
      value,
      name,
    }),
    [name, onChange, value],
  );

  return (
    <div className="payment-options">
      <ul>
        {React.Children.map(children, child => {
          if (child.type.name === 'RadioOption') return React.cloneElement(child, { ...values });
          return null;
        })}
      </ul>
    </div>
  );
};

const RadioOption = props => {
  const { id, name, onChange, value, children } = props;

  const handleChange = useCallback(e => {
    e.persist();
    onChange(e.target.value);
  }, [onChange]);

  const getIsChecked = useMemo(() => {
      return id?.toString() === value?.toString()
  },[id, value])

  return (
    <li>
      <div className="radio-option">
        <input
          type="radio"
          name={name}
          id={id}
          onChange={handleChange}
          value={id}
          checked={getIsChecked}
        />
        <label htmlFor={id}>
          {children}
          {/* <span className="image">
            <img src="../assets/images/paypal.png" alt="" />
          </span> */}
        </label>
      </div>
    </li>
  );
};
RadioOption.displayName = 'RadioOption';

export { RadioOption };

export default RadioGroup;
