import React, { useState } from 'react';
import Img from 'components/Img';
import classNames from 'classnames';

const LazyImg = ({ lazyImg, src, ...restProps }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const handleLoad = () => {
    console.log('loaded abdd');
    setIsLoaded(true);
  };
  return (
    <>
      {!isLoaded ? (
        <svg width="100%" height="100%" viewBox="0 0 100 100">
          {/* <rect width="100" height="100" rx="10" ry="10" fill="#CCC" /> */}
          <rect width="100" height="100" rx="10" ry="10" fill="#84ba40" />
        </svg>
      ) : null}
      <Img
        alt=""
        {...restProps}
        className={classNames(restProps.className, 'fade', { show: isLoaded })}
        onLoad={handleLoad}
        src={src}
      />
    </>
  );
};

export default LazyImg;
