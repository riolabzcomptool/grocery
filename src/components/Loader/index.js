import React from 'react';
import classNames from 'classnames';

const Loader = ({ fullScreen }) => (
  <div className={classNames('loader-wrapper', { contained: !fullScreen })}>
    <div className=" bar">
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
);

Loader.defaultProps = {
  fullScreen: true,
};

export default Loader;
