import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router';
import { connect } from 'react-redux';
// import Placeholder from './Placeholder';
// import toaster from 'toasted-notes';
// import toaster from 'toasted-notes'

class Authorize extends Component {
  render() {
    const { children, redirect, user, fallback, noRedirect } = this.props;
    console.log('aaa user.isLogged', user.isLogged)
    if (!user.isLogged) {
      // toaster.notify('You are not logged in!');
      if (noRedirect) return null;
      if (fallback) return fallback;

      // if (!noRedirect) return <Redirect to={redirect || '/'} />;
      return <Redirect to={redirect || '/'} />;
      // return <Placeholder
      //   title="Please login!"
      //   subtitle="Login to view your orders!"
      //   buttonText="Login"
      //   to="/user/login"
      //   // from={pathname}
      // />;
    }
    return children;
  }
}

Authorize.defaultProps = {
  noRedirect: false,
};

export default withRouter(connect(({ user }) => ({ user }))(Authorize));
