/* eslint-disable no-underscore-dangle */
import React from 'react';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import 'rc-banner-anim/assets/index.css';
import 'assets/styles/banner-style.css';

const { BgElement } = Element;
class HomeBanner extends React.Component {
  handleClick = e => {
    e.preventDefault();
  };

  render() {
    const { banner } = this.props;
    return (
      <BannerAnim
        prefixCls="banner-user banner"
        type="across"
        autoPlay
        autoPlaySpeed={5000}
        autoPlayEffect
      >
        {banner.map(item => (
          <Element prefixCls="banner-user-elem" key={item._id}>
            <BgElement
              key="bg"
              className="bg"
              style={{
                background: `url(/${item.images && item.images[0] && item.images[0].url})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center',
              }}
            />
            <TweenOne className="banner-user-text" animation={{ y: 30, opacity: 0, type: 'from' }}>
              <div className="banner-contain">
                <h5>all products</h5>
                <h1>buy grocery</h1>
                <h4>valid till 25 august</h4>
                <a href="#" onClick={this.handleClick} className="btn btn-solid">
                  shop now
                </a>
              </div>
            </TweenOne>
          </Element>
        ))}
      </BannerAnim>
    );
  }
}

export default HomeBanner;
