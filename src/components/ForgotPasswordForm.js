import React from 'react';
import { connect } from 'react-redux';
import { ForgotPasswordSchema } from '_utils/Schemas';
import Input from 'components/Input';
import { forgotPassword } from 'services/user';
import { withFormik } from 'formik';
import FormItem from './FormItem';
import Button from './Button';

const ForgotPasswordForm = ({
  // user,
  values,
  touched,
  errors,
  handleChange,
  handleSubmit,
  handleBlur,
  isSubmitting,
  onLoginClick,
  status,
}) => {
  return (
    <form className="theme-form">
      <FormItem
        label="Email"
        id="email"
        // formControl
        errors={errors.email && touched.email ? errors.email : ''}
      >
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          placeholder="Enter email"
          name="email"
        />
      </FormItem>
      {status?.error && <p className="error-message capitalize pt-3">{status.error}</p>}
      {status?.success && <p className="capitalize pt-3">Password recovery mail sent!</p>}
      <div>
        <Button
          onClick={handleSubmit}
          loading={isSubmitting}
          className="btn btn-solid btn-solid-sm btn-block"
        >
          Reset Password
        </Button>
        <Button
          onClick={onLoginClick}
          className="btn btn-solid btn-solid-sm btn-block"
        >
          Login
        </Button>
      </div>
    </form>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: () => {
    return {
      email: '',
    };
  },

  validationSchema: ForgotPasswordSchema,

  handleSubmit: async (values, { props, setStatus }) => {
    console.log(props);
    const { email } = values;
    const res = await forgotPassword({ email });
    const { success, error } = res;
    console.log('loka', res)
    if (error) {
      setStatus({ error });
      return;
    }
    if (success) {
      setStatus({ success: true });
      return;
    } 
     setStatus({ success: false });
  },

  displayName: 'ForgotPasswordForm',
})(ForgotPasswordForm);

export default connect(({ user }) => ({ user }))(MyEnhancedForm);
