import React from 'react';

const QtyInput = ({ loading, onDec, value, onInc }) => {
  return (
    <div className="qty-box">
      <div className="input-group">
        <span className="input-group-prepend">
          <button
            type="button"
            className="btn quantity-left-minus"
            data-type="minus"
            data-field=""
            disabled={loading}
            onClick={onDec}
          >
            <i className="fa fa-angle-left" />
          </button>
        </span>
        <input
          type="text"
          name="quantity"
          className="form-control input-number"
          value={value}
          readOnly
        />
        <span className="input-group-prepend">
          <button
            type="button"
            className="btn quantity-right-plus"
            data-type="plus"
            data-field=""
            disabled={loading}
            onClick={onInc}
          >
            <i className="fa fa-angle-right" />
          </button>
        </span>
      </div>
    </div>
  );
};

export default QtyInput;
