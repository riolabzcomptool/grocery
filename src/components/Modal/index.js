/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable react/button-has-type */
/* eslint-disable react/self-closing-comp */
import React, { Component, createContext, useContext } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components';
import Button from 'components/Button'
import ModalHead from './ModalHead';
import ModalBody from './ModalBody';
import './styles.scss';

export const ModalContext = createContext({});

const ModalFooter = ({ children }) => {
  const modalContext = useContext(ModalContext);
  console.log('olala',modalContext)
  const {noFooter} = modalContext
  if (noFooter) return null
  return <div className="modal-footer">{children}</div>
}

ModalFooter.propTypes = {
    // eslint-disable-next-line jsx-a11y/control-has-associated-label
    children: PropTypes.oneOf([PropTypes.arrayOf(PropTypes.instanceOf(Button),<button></button>), PropTypes.element])
}

ModalFooter.defaultProps = {
    children: "button"
}

const StyledIconDiv = styled.div`
  padding: 1em;
  border-bottom: 1px solid #dee2e6;
  position: sticky;
  background-color: inherit;
  z-index: 1;
  top: 0;
`;

const StyledModal = styled.div`
  ${props => {
    console.log('modal', props);
    return (
      props.fullHeight &&
      css`
        height: 100%;
      `
    );
  }};
`;

const StyledModalContent = styled.div`
  height: 100%;
  overflow-y: auto;
`;
class Modal extends Component {
  static Head = ModalHead;

  static Body = ModalBody;

  static Footer = ModalFooter;

  render() {
    const { show, onClose, children, style, fullHeight, noFooter } = this.props;
    console.log('in Modal', show);
    const initialContext = {
      noFooter,
      fullHeight,
    };
    // if (!show) return null;
    if (!show) return null;
    return (
      <ModalContext.Provider value={initialContext}>
        <div
          className={classNames({
            'modal modal-show d-block': show,
            'modal modal-none': !show,
            // 'modal fade show': show,
            // 'modal fade': !show,
          })}
          // ROLE WAS dialog
          role="presentation"
          tabIndex={-42}
          style={style || {}}
        >
          <StyledModal
            fullHeight={fullHeight}
            className={classNames({
              'modal-dialog': true,
            })}
            role="dialog"
          >
            <StyledModalContent className="modal-content" style={style}>
              {onClose && (
                <StyledIconDiv role="button" tabIndex={0} onKeyDown={onClose} onClick={onClose}>
                  <i className="fas fa-close fa-pull-right" />
                </StyledIconDiv>
              )}
              {/* {show && children}
            {!show && null} */}
              {children}
            </StyledModalContent>
          </StyledModal>
        </div>
      </ModalContext.Provider>
    );
  }
}

Modal.defaultProps = {
  fullHeight: true,
  style: {},
  noFooter: false,
};

export default Modal;
