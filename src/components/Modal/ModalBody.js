import React from 'react'
// import PropTypes from 'prop-types'

const ModalBody = ({ children, className }) => {
  return <div className={`modal-body ${className}`}>{children}</div>
}

// ModalBody.propTypes = {
//   children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.any), PropTypes.element]).isRequired,
// }

ModalBody.defaultProps  = {
  className:''
}

export default ModalBody
