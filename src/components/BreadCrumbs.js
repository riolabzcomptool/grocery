/* eslint-disable react/require-default-props */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

// import { Spring } from 'react-spring/renderprops'

// const springFrom = { marginTop: '20px', opacity: 0 };
// const springTo = { marginTop: '0px', opacity: 1 }
// const springConfig = { delay: 500 };

// headerState: {
//   title: 'Login',
//   bread: [
//     {
//       url: '/home',
//       text: 'Home',
//     },
//     {
//       text: 'Login',
//     },
//   ],
// },



class BreadCrumbs extends Component {
  state = {
    // breadcrumbs: [],
  };

  render() {
    const { bread } = this.props;
    const breadFiltered = bread.filter((item, index, array) => index < array.length - 1);
    const breads = breadFiltered.map(item => {
      // console.log(item.url)
      return (
        <li key={item.url} className="breadcrumb-item"><Link to={`${item.url}`}>{item.text}</Link></li>
      );
    });
    // console.log(bread)
    if (breads.length > 0)
      return (
        <section className="breadcrumb-section section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="page-title">
                  <h2>{bread[bread.length - 1].text}</h2>
                </div>
              </div>
              <div className="col-12">
                <nav aria-label="breadcrumb" className="theme-breadcrumb">
                  <ol className="breadcrumb">
                    {breads}
                    <li className="breadcrumb-item active" aria-current="page">{bread[bread.length - 1].text}</li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </section>

      );
    return null;
  }
}


BreadCrumbs.propTypes = {
  bread: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      url: PropTypes.string,
    }),
  ),
};



export default BreadCrumbs