import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { menuActions } from 'redux/actions';
import Button from 'components/Button'
import classNames from 'classnames';
import LoginForm from './LoginForm';
import SignupForm from './SignupForm';
import ForgotPasswordForm from './ForgotPasswordForm';

const MyAccountSide = ({ isAccountSide, dispatch, user }) => {
  const [formType, setFormType] = useState('login');
  const closeAccount = useCallback(
    e => {
      e.preventDefault();
      dispatch({
        type: menuActions.TOGGLE_ACCOUNT_SIDE,
      });
    },
    [dispatch],
  );

  const handleSignupClick = useCallback((e) => {
    if (e) e.preventDefault();
    setFormType('signup')
  }, []);

  const handleLoginClick = useCallback((e) => {
    if (e) e.preventDefault();
    setFormType('login')
  }, []);

  const handleForgotPwdClick = useCallback((e) => {
    if (e) e.preventDefault();
    setFormType('forgotPassword')
  }, []);

  if (!user.isLogged)
    return (
      <div
        id="myAccount"
        className={classNames('add_to_cart', 'right', { 'open-side': isAccountSide })}
      >
        <a href="#" aria-label="Close Account" className="overlay" onClick={closeAccount} />
        <div className="cart-inner">
          <div className="cart_top">
            <h3>my account</h3>
            <div className="close-cart">
              <Button plain title="Close" type="button" onClick={closeAccount}>
                <i className="fa fa-times" aria-hidden={!isAccountSide} />
              </Button>
            </div>
          </div>
          {formType === 'login' &&  <LoginForm onSignupClick={handleSignupClick} onForgotPasswordClick={handleForgotPwdClick} />}
          {formType === 'signup' && <SignupForm onLoginClick={handleLoginClick} />}
          {formType === 'forgotPassword' && <ForgotPasswordForm onLoginClick={handleLoginClick} /> }
        </div>
      </div>
    );
  return null;
};

export default connect(({ menu, user }) => ({ isAccountSide: menu.isAccountSide, user }))(
  MyAccountSide,
);
