import React, { Suspense, useMemo, useCallback } from 'react';
import Loader from 'components/Loader';
import Slider from 'react-slick';

import img0 from 'assets/images/category/grocery/1.png';
import img1 from 'assets/images/category/grocery/2.png';
import img2 from 'assets/images/category/grocery/3.png';
import img3 from 'assets/images/category/grocery/4.png';
import img4 from 'assets/images/category/grocery/5.png';
import { useCategories } from 'hooks/api';
import { generateCategoryTree } from '_utils';

const Categories = () => {
  const sliderSettings = useMemo(() => {
    return {
      className: 'slide-6 no-arrow',
      dots: false,
      arrows: false,
      // infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
      ],
    };
  }, []);

  const { data } = useCategories();

  let categoryTree = [];
  if (data?.data) categoryTree = generateCategoryTree(data.data);

  console.log('pooh', categoryTree);

  const handleCategoryClick = useCallback(e => {
    e.preventDefault();
  }, []);

  if (data?.data) {
    console.log('pooh returning', categoryTree.length);
    return (
      <Suspense fallback={<Loader />}>
        <section className="category category-classic bg-none absolute-banner">
          <div className="container absolute-bg">
            <Slider {...sliderSettings}>
              {categoryTree.map((item, index) => {
                let img = img1;
                switch (index % 5) {
                  case 0:
                    img = img0;
                    break;
                  case 1:
                    img = img1;
                    break;
                  case 2:
                    img = img2;
                    break;
                  case 3:
                    img = img3;
                    break;
                  case 4:
                    img = img4;
                    break;
                  default:
                    img = img0;
                }
                return (
                  <div>
                    <div className="category-wrapper">
                      <div className="img-block">
                        <button className="btn p-0" type="button" onClick={handleCategoryClick}>
                          <img src={img} alt="" className=" img-fluid" />
                        </button>
                      </div>
                      <div className="category-title">
                        <button className="btn p-0" type="button" onClick={handleCategoryClick}>
                          <h5>{item.title}</h5>
                        </button>
                      </div>
                    </div>
                  </div>
                );
              })}
            </Slider>
          </div>
        </section>
      </Suspense>
    );
  }
  return null;
};

export default Categories;
