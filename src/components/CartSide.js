import React, { useCallback } from 'react';
import classNames from 'classnames';
import Button from 'components/Button';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { cartActions, menuActions } from 'redux/actions';

const CartSide = ({ isCartSide, cart, dispatch }) => {
  // const [isOpen, setIsOpen] = useState(isCartSide);

  // useEffect(() => {
  //   setIsOpen(isCartSide);
  // }, [isCartSide]);

  const onClose = useCallback(
    e => {
      e.preventDefault();
      dispatch({
        type: menuActions.TOGGLE_CART_SIDE,
      });
    },
    [dispatch],
  );

  const onRemove = useCallback(
    id => {
      return e => {
        e.preventDefault();
        dispatch({
          type: cartActions.REMOVE_FROM_CART,
          payload: { productId: id },
        });
      };
    },
    [dispatch],
  );

  const cartAmount = cart.reduce((acc, item) => acc + (+item.pricing?.salePrice || 0), 0);
  return (
    <div id="cart_side" className={classNames('add_to_cart right', { 'open-side': isCartSide })}>
      <a href="#" aria-label="Close cart" className="overlay" onClick={onClose} />
      <div className="cart-inner">
        <div className="cart_top">
          <h3>my cart</h3>
          <div className="close-cart">
            <Button plain onClick={onClose}>
              <i className="fa fa-times" aria-hidden={!isCartSide} />
            </Button>
          </div>
        </div>
        <div className="cart_media">
          <ul className="cart_product">
            {cart.map(i => (
              <li key={i.productId}>
                <div className="media">
                  <Link to={`/product/${i.slug}`}>
                    <img alt="" className="mr-3" src={i.thumbImg} />
                  </Link>
                  <div className="media-body">
                    <Link to={`/product/${i.slug}`}>
                      <h4>{i.name}</h4>
                    </Link>
                    <h4>
                      <span>{`${i.qty} x $ ${i.salePrice || i.listPrice}`}</span>
                    </h4>
                  </div>
                </div>
                <div className="close-circle">
                  <button type="button" onClick={onRemove(i.productId)}>
                    <i className="fa fa-trash" aria-hidden="true" />
                  </button>
                </div>
              </li>
            ))}
          </ul>
          <ul className="cart_total">
            <li>
              <div className="total">
                <h5>
                  subtotal : <span>${cartAmount}</span>
                </h5>
              </div>
            </li>
            <li>
              <div className="buttons">
                <Link to="/cart" className="btn btn-solid btn-block btn-solid-sm view-cart">
                  view cart
                </Link>
                <Link to="/checkout" className="btn btn-solid btn-solid-sm btn-block checkout">
                  checkout
                </Link>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default connect(({ menu, cart }) => ({ isCartSide: menu.isCartSide, cart: cart.products }))(
  CartSide,
);
