import React from 'react';
import classNames from 'classnames';

const ToastedNotes = ({ error, text, onClose }) => {
  return (
    <div className={classNames('Toaster__alert', { error })}>
      <div className="Toaster__alert_text">{text}</div>
      <button onClick={onClose} className="Toaster__alert_close" type="button" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
  );
};

ToastedNotes.defaultProps = {
  error: false,
};

export default ToastedNotes;
