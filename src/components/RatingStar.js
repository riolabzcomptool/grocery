/* eslint-disable react/destructuring-assignment */
import React, { useCallback, useState } from 'react';

const RatingStar = ({ initialValue, onChange }) => {
  const [rating, setRating] = useState(initialValue);

  const resetRating = () => {
    setRating(0);
  };

  const handleRatingChange = useCallback(newRating => {
    return () => {
      if (newRating === rating) resetRating();
      setRating(newRating);
      if (onChange) onChange(newRating)
    };
  }, [onChange, rating]);

  return (
    <div className="media-body ml-3">
      <div className={`rating rating-${rating}`}>
        <i
          onClick={handleRatingChange(1)}
          onKeyPress={handleRatingChange(1)}
          tabIndex={0}
          label="rating-1"
          aria-selected={rating === 1}
          role="option"
          className="fa fa-star"
        />
        <i
          onClick={handleRatingChange(2)}
          onKeyPress={handleRatingChange(2)}
          tabIndex={0}
          label="rating-2"
          aria-selected={rating === 2}
          role="option"
          className="fa fa-star"
        />
        <i
          onClick={handleRatingChange(3)}
          onKeyPress={handleRatingChange(3)}
          tabIndex={0}
          label="rating-3"
          aria-selected={rating === 3}
          role="option"
          className="fa fa-star"
        />
        <i
          onClick={handleRatingChange(4)}
          onKeyPress={handleRatingChange(4)}
          tabIndex={0}
          label="rating-4"
          aria-selected={rating === 4}
          role="option"
          className="fa fa-star"
        />
        <i
          onClick={handleRatingChange(5)}
          onKeyPress={handleRatingChange(5)}
          tabIndex={0}
          label="rating-5"
          aria-selected={rating === 5}
          role="option"
          className="fa fa-star"
        />
      </div>
    </div>
  );
};

RatingStar.defaultProps = {
  rating: 5,
};

export default RatingStar;
