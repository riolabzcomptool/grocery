import React from 'react';
import classNames from 'classnames';

const FormItem = props => {
  const { children, label, errors, errorPosition, id, formControl, name } = props;
  return (
    <div className={classNames("form-group", {"mb-4":errors})}>
      {errorPosition === 'above' && errors && <div className="error-message">{errors}</div>}
      {!formControl && (
        <>
          <label className="capitalize" htmlFor={id || name}>
            {label}
          </label>
          {children}
        </>
      )}
      {formControl && (
        <>
          <label className="capitalize" htmlFor={id || name}>
            {label}
            {children}
          </label>
        </>
      )}
      {errorPosition === 'below' && errors && <div className="error-message">{errors}</div>}
    </div>
  );
};

FormItem.defaultProps = {
  errorPosition: 'below',
  formControl: false,
};

export default FormItem;
