import React from 'react';
import Input from 'components/Input';

import FormItem from 'components/FormItem';
import Button from 'components/Button';

import { withFormik } from 'formik';

const AddPhoneVerify = props => {
  console.log('AddPhoneVerify', props);
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    inputProps,
    isSubmitting,
    // setValues,
    // handleReset
  } = props;

  return (
    <>
      {inputProps.map(i => (
        <FormItem errors={errors[i] && touched[i] ? errors[i] : ''} key={i}>
          <Input
            initialValue={values[i]}
            onChange={handleChange}
            name={i}
            value={values[i]}
            
          />
        </FormItem>
      ))}
      <Button loading={isSubmitting} onClick={handleSubmit}>Next</Button>
    </>
  );
};

AddPhoneVerify.defaultProps = {
  noWrapper: false,
};

const MyEnhancedForm = withFormik({
  // mapPropsToValues: props => {
  //   const { inputProps } = props;
  //   console.log(props);
  //   return {
  //     phone,
  //   };
  // },

  validationSchema: props => props.schema,

  handleSubmit: async (
    values,
    {
      props,
      // setSubmitting
    },
  ) => {
    // setSubmitting(true);
    await props.onSubmit(values);
    // setSubmitting(false);
  },

  displayName: 'PhoneVerifyForm',
})(AddPhoneVerify);

export default MyEnhancedForm;
