import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Modal from 'components/Modal';
import Steps from 'components/Steps';
import { phoneVerifySchema, otpSchema } from '_utils/Schemas';
import { sendOtp, verifyOtp } from 'services/user';
import { userActions } from '_constants';
// import Portal from 'components/Portal';
import Form from './Form';

const PhoneVerify = ({ phoneNotVerified, delay, showOnMount, openCount, dispatch, isLogging }) => {
  console.log(delay, openCount);

  let toRender = phoneNotVerified;
  if (openCount && openCount > 0) toRender = true;
  // const [isShow, setShow] = useState(toRender);
  // let initShow = toRender;
  // if (typeof isOpen !== 'undefined') initShow = toRender && isOpen;
  const [isShow, setShow] = useState(phoneNotVerified && showOnMount);

  useEffect(() => {
    console.log('showw', isShow);
    console.log('showw phoneNotVerified && showOnMount', phoneNotVerified && showOnMount);
    console.log('showw openCount', openCount);
    if (phoneNotVerified && showOnMount && !isShow && typeof openCount === 'undefined')
      setShow(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [phoneNotVerified, showOnMount]);

  useEffect(() => {
    if (!isLogging && toRender && showOnMount && !isShow && typeof openCount === 'undefined')
      setShow(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLogging]);
  console.log('aaap', toRender && showOnMount, 'isShow', isShow);
  const [phone, setPhone] = useState();
  const [stepErrMsg, setStepErrMsg] = useState({ phone: null, otp: null });
  const [otpMsg, setOtpMsg] = useState(null);
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    console.log('aab', openCount, !isShow, openCount > 0 && !isShow);
    if (openCount && openCount > 0 && !isShow) {
      console.log('will open');

      setActiveIndex(0);
      setPhone('');
      setShow(true);
    }
    // return () => setShow(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [openCount]);

  // useEffect(() => {
  //   if (toRender && delay > 0 && activeIndex === 0) {
  //     setTimeout(() => {
  //       if (!isShow) setShow(true);
  //     }, delay);
  //   }
  // }, [toRender, delay, activeIndex, isShow]);

  const handleClose = () => {
    setShow(false);
  };

  const handlePhoneSubmit = async values => {
    if (otpMsg) setOtpMsg(null);
    if (stepErrMsg.phone) setStepErrMsg(prev => ({ ...prev, phone: null }));
    const res = await sendOtp({ phone: values.phone });
    console.log('ABCD a', values);
    if (res && res.success) {
      setPhone(values.phone);
      setActiveIndex(1);
      setOtpMsg(`An OTP has been sent to your phone at ${values.phone}`);
    }
    if (res && res.error) {
      setStepErrMsg(prev => ({
        ...prev,
        phone: res.error,
      }));
    }
  };

  const handleResendOtp = async (e) => {
    e.preventDefault();
    if (otpMsg) setOtpMsg(null);
    if (stepErrMsg.otp) setStepErrMsg(prev => ({ ...prev, otp: null }));
    console.log('ABCD b', phone);
    const res = await sendOtp({ phone });
    console.log('ABCD b resent', res);
    if (res && res.success) {
      setOtpMsg(`An OTP has been resent to your phone at ${phone}`);
    }
    if (res && res.error) {
      setStepErrMsg(prev => ({
        ...prev,
        otp: res.error,
      }));
    }
  };

  const handlOtpSubmit = async values => {
    if (stepErrMsg.otp) setStepErrMsg(prev => ({ ...prev, otp: null }));
    const res = await verifyOtp({ phone, otp: values.otp });
    if (res && res.success) {
      setActiveIndex(2);
      dispatch({
        type: userActions.SET_STATE,
        payload: {
          phoneNo: phone,
          phoneVerified: 1,
        },
      });
      // setTimeout(() => {
      //   if (isShow) setShow(false);
      // }, 1500);
    }
    if (res && res.error) {
      setStepErrMsg(prev => ({
        ...prev,
        otp: res.error,
      }));
    }
  };

  if (!phoneNotVerified && !isShow) return null;

  return (
    <Modal show={isShow} onClose={handleClose} noFooter fullHeight={false}>
      <Modal.Body className="pb-5">
        <Steps activeIndex={activeIndex} noMenu>
          <Steps.Panel key={0} noWrapper>
            <div>
              <h5>Verify your phone number</h5>
              <Form
                text="Verify your phone number"
                inputProps={['phone']}
                onSubmit={handlePhoneSubmit}
                schema={phoneVerifySchema}
              />
              {stepErrMsg.phone && <div className="error-message mt-2">{stepErrMsg.phone}</div>}
            </div>
          </Steps.Panel>
          <Steps.Panel key={1} noWrapper>
            <div>
              <h5>Enter OTP</h5>
              <p className="font-weight-normal">{otpMsg}</p>
              <Form onSubmit={handlOtpSubmit} inputProps={['otp']} schema={otpSchema} />
              <a href="#" onClick={handleResendOtp} className="mt-2 d-block font-weight-normal">
                Didn&apos;t receive OTP? Click here to send again
              </a>
              {stepErrMsg.otp && <div className="error-message mt-2">{stepErrMsg.otp}</div>}
            </div>
          </Steps.Panel>
          <Steps.Panel key={2} noWrapper>
            <h5>Your phone number has been verified!</h5>
          </Steps.Panel>
        </Steps>
      </Modal.Body>
    </Modal>
  );
};

export default connect(({ user }) => {
  return { phoneNotVerified: user.phoneVerified === 0, loading: user.isLogging };
})(PhoneVerify);

PhoneVerify.defaultProps = {
  delay: 1500,
  showOnMount: true,
  openCount: undefined,
};
