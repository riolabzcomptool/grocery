/* eslint-disable react/require-default-props */
import React, { Component } from 'react';


import { withRouter } from 'react-router-dom'
import { getHeaderState } from 'navigation/routes'
import BreadCrumbs from './BreadCrumbs';
// import { Spring } from 'react-spring/renderprops'

// const springFrom = { marginTop: '20px', opacity: 0 };
// const springTo = { marginTop: '0px', opacity: 1 }
// const springConfig = { delay: 500 };

// headerState: {
//   title: 'Login',
//   bread: [
//     {
//       url: '/home',
//       text: 'Home',
//     },
//     {
//       text: 'Login',
//     },
//   ],
// },
class BreadcrumbWrapper extends Component {
    state = {
        headerState: null,
    }

    componentDidMount() {
        const { location, headerState: headerStateFromProps } = this.props
        const { pathname } = location
        console.log(this.props)
        const headerState = headerStateFromProps || getHeaderState(pathname)
        if (headerState) {
            this.setState({
                headerState,
            })
        }
        console.log(headerState)
    }

    // shouldComponentUpdate(nextProps) {
    //   const { location } = nextProps
    //   const { pathname } = location
    //   return isRouteWithHeader(pathname)
    // }

    static getDerivedStateFromProps(props, state) {
        console.log(props, state)
        const { location, headerState: headerStateFromProps } = props
        const { pathname } = location
        return { headerState: headerStateFromProps || getHeaderState(pathname) }
    }

    render() {
        const { headerState } = this.state;

        console.log(headerState)
        if (headerState) {
            return <BreadCrumbs bread={headerState.bread} />
        }
        return null
    }
}

export default withRouter(BreadcrumbWrapper)