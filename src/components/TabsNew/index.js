import React, { useState, useMemo, useCallback } from 'react';
import classNames from 'classnames';
import leftImg from 'assets/images/left.png';
import rightImg from 'assets/images/right.png';

const TabsNew = ({ children, className }) => {
  const [activeIndex, setActiveIndex] = useState(0);

  const handleTabClick = useCallback(index => {
    return e => {
      e.preventDefault();
      setActiveIndex(+index);
    };
  }, []);

  const menuItems = useMemo(() => {
    console.log('kooh children', children);
    return React.Children.map(children, (child, index) => {
      const { props } = child;
      return (
        <li className={classNames({ current: activeIndex === index })}>
          <a href="#" onClick={handleTabClick(index)}>
            {props.label}
          </a>
        </li>
      );
    });
  }, [activeIndex, children, handleTabClick]);
  return (
    <section className=" tab-layout1 section-b-space">
      <div className="theme-tab">
        <ul className="tabs border-top-0">
          {menuItems}
          {/* <li className="current">
            <a href="tab-1">Vegetables</a>
          </li>
          <li className="">
            <a href="tab-2">Snacks</a>
          </li>
          <li className="">
            <a href="tab-3">Food grains</a>
          </li>
          <li className="">
            <a href="tab-4">bakery</a>
          </li> */}
        </ul>
        <div className={`tab-content-cls ${className}`}>
          {React.Children.map(children, (child, index) => {
            return React.cloneElement(child, {
              isActive: index === activeIndex,
              isDefault: index === 0,
            });
          })}
        </div>
        {/* <div className="tab-content-cls ratio_square">
          <div id="tab-1" className="tab-content active default">
            <div className="container shadow-cls">
              <div className="drop-shadow">
                <div className="left-shadow">
                  <img src={leftImg} alt="" className=" img-fluid" />
                </div>
                <div className="right-shadow">
                  <img src={rightImg} alt="" className=" img-fluid" />
                </div>
              </div>
              <div className="row border-row1 m-0">
                {featProdsLoading && (
                  <>
                    <Product key="1" loading />
                    <Product key="2" loading />
                    <Product key="3" loading />
                    <Product key="4" loading />
                    <Product key="5" loading />
                    <Product key="6" loading />
                    <Product key="7" loading />
                  </>
                )}
                {!featProdsLoading &&
                  featuredProducts &&
                  featuredProducts.map(product => (
                    <div className="col-lg-2 col-sm-4 col-6 p-0">
                      <Product
                        slug={product.slug}
                        key={product._id}
                        id={product._id}
                        title={product.name}
                        description={product.textDescription}
                        price={product.pricing.salePrice}
                        img={product.images[0].thumbnail}
                      />
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </div> */}
        {/* <div className="tab-content-cls ratio_square">
          <div id="tab-2" className="tab-content active default">
            <div className="container shadow-cls">
              <div className="drop-shadow">
                <div className="left-shadow">
                  <img src={leftImg} alt="" className=" img-fluid" />
                </div>
                <div className="right-shadow">
                  <img src={rightImg} alt="" className=" img-fluid" />
                </div>
              </div>
              <div className="row border-row1 m-0">
                {cat2Loading && (
                  <div className="col-lg-2 col-sm-4 col-6 p-0">
                    <Product key="1" loading />
                    <Product key="2" loading />
                    <Product key="3" loading />
                    <Product key="4" loading />
                    <Product key="5" loading />
                    <Product key="6" loading />
                    <Product key="7" loading />
                  </div>
                )}
                {!cat2Loading &&
                  cat2Prods &&
                  cat2Prods.map(product => (
                    <Product
                      item
                      key={product.key}
                      slug={product.slug}
                      title={product.name}
                      description={product.textDescription}
                      price={product.pricing.salePrice}
                      img={product.images[0].thumbnail}
                    />
                  ))}
              </div>
            </div>
          </div>
        </div> */}

        {/* <div className="tab-content-cls ratio_square">
          <div id="tab-3" className="tab-content active default">
            <div className="container shadow-cls">
              <div className="drop-shadow">
                <div className="left-shadow">
                  <img src={leftImg} alt="" className=" img-fluid" />
                </div>
                <div className="right-shadow">
                  <img src={rightImg} alt="" className=" img-fluid" />
                </div>
              </div>
              <div className="row border-row1 m-0">
                {cat1Loading && (
                  <div className="col-lg-2 col-sm-4 col-6 p-0">
                    <Product key="1" loading />
                    <Product key="2" loading />
                    <Product key="3" loading />
                    <Product key="4" loading />
                    <Product key="5" loading />
                    <Product key="6" loading />
                    <Product key="7" loading />
                  </div>
                )}

                {!cat1Loading &&
                  cat1Prods &&
                  cat1Prods.map(product => (
                    <Product
                      item
                      key={product.key}
                      slug={product.slug}
                      title={product.name}
                      description={product.textDescription}
                      price={product.pricing.salePrice}
                      img={product.images[0].thumbnail}
                    />
                  ))}
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </section>
  );
};

export const TabsNewPanel = props => {
  console.log('kooh panel', props);
  const { children, isActive, id, isDefault } = props;
  const style = useMemo(() => {
    let styleA = { display: 'none' };
    if (isActive && id)
      styleA = {
        display: 'block',
      };
    console.log('kooh panel id & isActive', id, isActive, styleA);
    return styleA;
  }, [isActive, id]);
  return (
    <div className={classNames('tab-content', { 'active default': isDefault })} style={style}>
      <div className="container shadow-cls">
        <div className="drop-shadow">
          <div className="left-shadow">
            <img src={leftImg} alt="" className=" img-fluid" />
          </div>
          <div className="right-shadow">
            <img src={rightImg} alt="" className=" img-fluid" />
          </div>
        </div>
        <div className="row border-row1 m-0">{children}</div>
      </div>
    </div>
  );
};

TabsNew.defaultProps = {
  className:''
}

export default TabsNew;
