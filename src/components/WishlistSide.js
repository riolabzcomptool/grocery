/* eslint-disable no-underscore-dangle */
import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import Button from 'components/Button'
import { wishlistActions, menuActions } from 'redux/actions';
import { Link } from 'react-router-dom';

const WishlistSide = ({ isWishlist, wishlist, dispatch }) => {
  // const [isOpen, setIsOpen] = useState(isWishlist);

  // useEffect(() => {
  //   setIsOpen(isWishlist);
  // }, [isWishlist]);

  const closeWishlist = useCallback(e => {
    e.preventDefault();
    dispatch({
      type: menuActions.TOGGLE_WISHLIST,
    });
  }, [dispatch]);

  const deleteFromWishlist = useCallback(
    id => {
      return e => {
        e.preventDefault();
        dispatch({
          type: wishlistActions.EDIT_WISHLIST,
          payload: {
            type: 'remove',
            id,
          },
        });
      };
    },
    [dispatch],
  );

  const totalAmt = wishlist.reduce((acc, item) => acc + (+item.pricing?.salePrice || 0), 0)

  return (
    <div id="wishlist_side" className={classNames('add_to_cart right', { 'open-side': isWishlist })}>
      <a aria-label="Close wishlist" href="#" className="overlay" onClick={closeWishlist} />
      <div className="cart-inner">
        <div className="cart_top">
          <h3>my wishlist</h3>
          <div className="close-cart">
            <Button plain title="Close wishlist" aria-label="Close wishlist" onClick={closeWishlist}>
              <i className="fa fa-times" aria-hidden={!isWishlist} />
            </Button>
          </div>
        </div>
        <div className="cart_media">
          <ul className="cart_product">
            {wishlist.map(i => (
              <li key={i._id}>
                <div className="media">
                  <Link to={`/product/${i.slug}`}>
                    <img alt="" className="mr-3" src={i.images[0].thumbnail} />
                  </Link>
                  <div className="media-body">
                    <Link to={`/product/${i.slug}`}>
                      <h4>{i.name}</h4>
                    </Link>
                    {/* <h4>
                      <span>sm</span>
                      <span>, blue</span>
                    </h4> */}
                    <h4>
                      <span>${i.pricing?.salePrice}</span>
                    </h4>
                  </div>
                </div>
                <div className="close-circle">
                  <Button plain title="Delete" onClick={deleteFromWishlist(i._id)}>
                    <i className="fa fa-trash" aria-hidden={!isWishlist} />
                  </Button>
                </div>
              </li>
            ))}
          </ul>
          <ul className="cart_total">
            <li>
              <div className="total">
                <h5>
                  subtotal : <span>${totalAmt}</span>
                </h5>
              </div>
            </li>
            <li>
              <div className="buttons">
                <Link to="/wishlist" className="btn btn-solid btn-block btn-solid-sm view-cart">
                  view wislist
                </Link>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default connect(({ menu, wishlist }) => ({
  isWishlist: menu.isWishlist,
  wishlist: wishlist.products,
  loading: wishlist.loading,
}))(WishlistSide);
