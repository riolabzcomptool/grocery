import React from 'react';
import { connect } from 'react-redux';
import { LoginSchema } from '_utils/Schemas';
import { userActions } from 'redux/actions';
import Input from 'components/Input';
import { withFormik } from 'formik';
import FormItem from './FormItem';
import Button from './Button';

const LoginForm = ({
  user,
  values,
  touched,
  errors,
  handleChange,
  handleSubmit,
  handleBlur,
  isSubmitting,
  onSignupClick,
  onForgotPasswordClick,
}) => {
  return (
    <form className="theme-form">
      <FormItem
        label="Email"
        id="email"
        // formControl
        errors={errors.email && touched.email ? errors.email : ''}
      >
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          placeholder="Enter email"
          name="email"
        />
      </FormItem>
      <FormItem
        label="password"
        id="password"
        // formControl
        errors={errors.password && touched.password ? errors.password : ''}
      >
        <Input
          placeholder="Enter password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
          name="password"
          type="password"
        />
      </FormItem>

      {user.loginError && <p className="error-message capitalize pt-3">{user.loginError}</p>}
      <Button
        onClick={handleSubmit}
        loading={isSubmitting}
        className="btn btn-solid btn-solid-sm btn-block"
      >
        Login
      </Button>
      <h5 className="forget-class">
        <button type="button" onClick={onForgotPasswordClick} className="d-block link">
          Forgot password?
        </button>
      </h5>
      <h5 className="forget-class">
        <button type="button" onClick={onSignupClick} className="d-block link">
          New to store? Signup now
        </button>
      </h5>
    </form>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: () => {
    return {
      email: '',
      password: '',
    };
  },

  validationSchema: LoginSchema,

  handleSubmit: async (values, { props, setSubmitting }) => {
    console.log(props);
    const { dispatch, user } = props;
    const { email, password } = values;
    setSubmitting(true);
    dispatch({
      type: userActions.LOGIN,
      payload: {
        email,
        password,
      },
    });
    if (!user.isLogging) setSubmitting(false);
  },

  displayName: 'LoginForm',
})(LoginForm);

export default connect(({ user }) => ({ user }))(MyEnhancedForm);
