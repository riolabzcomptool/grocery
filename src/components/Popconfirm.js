import React, { useState, useCallback } from 'react';
import classNames from 'classnames';

const Popconfirm = ({ children, text, onOk }) => {
  const [isActive, setActive] = useState(false);

  const onCancel = useCallback(() => setActive(false), []);
  return (
    <div className="popconfirm-wrapper">
      <div className="popconfirm">
        {children}
        <div className={classNames('popconfirm-text', { active: isActive })}>
          {text}
          <button type="button" onClick={onOk} className="btn">
            Yes
          </button>
          <button type="button" onClick={onCancel} className="btn">
            No
          </button>
        </div>
      </div>
    </div>
  );
};

Popconfirm.defaultProps = {
  text: 'Are you sure?',
};

export default Popconfirm;
